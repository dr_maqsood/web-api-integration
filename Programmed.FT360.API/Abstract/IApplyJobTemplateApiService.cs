﻿using System.Threading.Tasks;
using Programmed.FT360.API.Model;

namespace Programmed.FT360.API.Abstract
{
    public interface IApplyJobTemplateApiService
    {
        Task<JobOrderJobTemplate.JobOrderJobTemplates> ApplyJobTemplate(long jobOrderId, long jobTemplateId);
    }
}