﻿using System.Threading.Tasks;
using Programmed.HRLink.Integration.Model.Api;

namespace Programmed.FT360.API.Abstract
{
    public interface ICandidateApiService
    {
        /// <summary>
        /// Get candidates from fast track
        /// </summary>
        /// <returns></returns>
        Task<Candidates> GetCandidates();
        /// <summary>
        /// Get candidate by id from fast track
        /// </summary>
        /// <param name="candidateId"></param>
        /// <returns></returns>
        Task<CandidateOne> GetCandidate(long candidateId);
        /// <summary>
        /// Update candidate in fast track
        /// </summary>
        /// <param name="candidateId"></param>
        /// <param name="candidate"></param>
        /// <returns></returns>
        Task<UpdateResponse> UpdateCandidate(long? candidateId, CandidateAddEdit candidate);
    }
}
