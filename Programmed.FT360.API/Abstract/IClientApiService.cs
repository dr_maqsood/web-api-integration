﻿using System.Threading.Tasks;
using Programmed.FT360.API.Model.Client;
using Programmed.HRLink.Integration.Model.Api;

namespace Programmed.FT360.API.Abstract
{
    public interface IClientApiService
    {
        /// <summary>
        /// Get clients from fast track
        /// </summary>
        /// <returns></returns>
        Task<Clients> GetClients();
        /// <summary>
        /// Get client by id from fast track
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        Task<Client> GetClient(long clientId);
        /// <summary>
        /// Update client in fast track
        /// </summary>
        /// <param name="fTClientId"></param>
        /// <param name="clientAddEdit"></param>
        /// <returns></returns>
        Task<UpdateResponse> UpdateClient(long? fTClientId, ClientAddEdit clientAddEdit);
    }
}
