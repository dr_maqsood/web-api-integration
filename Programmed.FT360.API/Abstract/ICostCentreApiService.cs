﻿using Programmed.HRLink.Integration.Model.Api;
using Programmed.HRLink.Integration.Model.Api.CostCentre;
using System.Threading.Tasks;

namespace Programmed.FT360.API.Abstract
{
    public interface ICostCentreApiService
    {
        /// <summary>
        /// Get cost centres from fast track
        /// </summary>
        /// <returns></returns>
        Task<CostCentres> GetCostCentres();
        /// <summary>
        /// Get cost centre from fast track for a given cost centre id
        /// </summary>
        /// <param name="costCentreId"></param>
        /// <returns></returns>
        Task<CostCentre> GetCostCentre(long costCentreId);
        /// <summary>
        /// Update const centre in fast track
        /// </summary>
        /// <param name="fTCostCentreId"></param>
        /// <param name="costCentreAddEdit"></param>
        /// <returns></returns>
        Task<UpdateResponse> UpdateCostCentre(long? fTCostCentreId, CostCentreAddEdit costCentreAddEdit);
    }
}
