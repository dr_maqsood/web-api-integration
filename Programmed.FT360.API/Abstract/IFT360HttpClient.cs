﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Programmed.FT360.API.Abstract
{
    public interface IFT360HttpClient
    {

        /// <summary>
        /// Refresh token if required
        /// </summary>
        Task RefreshTokenIfRequired();

        /// <summary>
        /// Get objects from fast track
        /// </summary>
        /// <returns></returns>
        Task<HttpResponseMessage> GetManyAsync();

        Task<HttpResponseMessage> GetAsyncById(string identifier);
        /// <summary>
        /// Post an object to fast track
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<HttpResponseMessage> PostAsync<T>(T model);
        /// <summary>
        /// Put an object to fast track
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="identifier"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<HttpResponseMessage> PutAsync<T>(string identifier, T model);
        /// <summary>
        /// Delete an object from fast track
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        Task DeleteAsync(string identifier);

        void Dispose();
    }
}