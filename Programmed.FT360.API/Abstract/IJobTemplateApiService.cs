﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Programmed.FT360.API.Abstract
{
    public interface IJobTemplateApiService
    {
        Task<List<Model.JobTemplateMethod>> GetJobTemplates();
    }
}