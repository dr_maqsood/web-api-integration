﻿using Programmed.FT360.API.Model.ParentCompany;
using System.Threading.Tasks;
using Programmed.HRLink.Integration.Model.Api;

namespace Programmed.FT360.API.Abstract
{
    public interface IParentCompanyApiService
    {
        /// <summary>
        /// Get all parent companyes from fast track
        /// </summary>
        /// <returns></returns>
        Task<ParentCompanies> GetParentCompanies();
        /// <summary>
        /// Get parent company by id from fast track
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        Task<ParentCompany> GetParentCompany(long parentId);
        /// <summary>
        /// Update parent company in fast track
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="parentCompany"></param>
        /// <returns></returns>
        Task<UpdateResponse> UpdateParentCompany(long? parentId, ParentCompanyAddEdit parentCompany);
    }
}
