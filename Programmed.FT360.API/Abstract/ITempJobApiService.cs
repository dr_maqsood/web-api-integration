﻿using System.Threading.Tasks;
using Programmed.FT360.API.Model.TempJob;
using Programmed.HRLink.Integration.Model.Api;

namespace Programmed.FT360.API.Abstract
{
    public interface ITempJobApiService
    {
        /// <summary>
        /// Get temp job by id from fast track
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        Task<TempJob> GetTempJob(long jobId);
        /// <summary>
        /// Update parent company in fast track
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="tempJobAddEdit"></param>
        /// <returns></returns>
        Task<UpdateResponse> UpdateTempJob(long? jobId, TempJobAddEdit tempJobAddEdit);
    }
}
