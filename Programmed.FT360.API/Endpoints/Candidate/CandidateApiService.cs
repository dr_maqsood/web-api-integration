﻿using log4net;
using Newtonsoft.Json;
using Programmed.FT360.API.Abstract;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Programmed.HRLink.Integration.Model.Api;


namespace Programmed.FT360.API.Endpoints.Candidate
{
    public class CandidateApiService : ICandidateApiService, IDisposable
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CandidateApiService));
        private readonly FT360HttpClient _candidateHttpClient;        
        public CandidateApiService(string consumerUserName, string userName, string password, string baseUrl, string loginEndpoint, int tokenExpiryInterval, string candidateEndpoint)
        {
            _candidateHttpClient = new FT360HttpClient(consumerUserName, loginEndpoint, tokenExpiryInterval, userName, password, baseUrl, candidateEndpoint);
        }        
        /// <summary>
        /// Get active candidates from fast track
        /// </summary>
        /// <returns></returns>
        public async Task<Candidates> GetCandidates()
        {
            var response = await _candidateHttpClient.GetManyAsync();
            if(response.IsSuccessStatusCode)
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();

                    var candidates = JsonConvert.DeserializeObject<Candidates>(responseString);
                    candidates.data = candidates.data.Where(x => x.status?.id == HRLink.Integration.Common.Constants.CandidateStatus.Active).ToList();
                    return candidates;
                }
            }
            else
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();
                    Logger.Info(responseString);
                    return new Candidates();
                }
            }           
        }
        /// <summary>
        /// Get Candidate from Fast Track by Id
        /// </summary>
        /// <param name="candidateId"></param>
        /// <returns></returns>
        public async Task<CandidateOne> GetCandidate(long candidateId)
        {
            var response = await _candidateHttpClient.GetAsyncById(candidateId.ToString());

            if (response.IsSuccessStatusCode)
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();
                    var candidate = JsonConvert.DeserializeObject<CandidateOne>(responseString);
                    return candidate;
                }
            }
            else
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();
                    Logger.Info(responseString);
                    return new CandidateOne() { data = new HRLink.Integration.Model.Api.Candidate()};
                }
            }          
        }
        /// <summary>
        /// Update/Create Candidate in Fast Track
        /// </summary>
        /// <param name="candidateId"></param>
        /// <param name="candidateAddEdit"></param>
        /// <returns></returns>
        public async Task<UpdateResponse> UpdateCandidate(long? candidateId, CandidateAddEdit candidateAddEdit)
        {
            var updateResponse = new UpdateResponse();
            try
            {
                CandidateOne candidate;
                if (candidateId.HasValue)
                {
                    candidate = await GetCandidate(candidateId.Value);
                }
                else
                {
                    candidate = new CandidateOne() { data = new HRLink.Integration.Model.Api.Candidate() };
                }

                if (candidate.data.id == 0)//Candidate doesn't exist in fast track so create new one
                {
                    var response = await _candidateHttpClient.PostAsync(candidateAddEdit);

                    if (response.IsSuccessStatusCode)
                    {
                        var c = await GetCandidateObject<HRLink.Integration.Model.Api.Candidate>(response);
                        updateResponse.Id = c.id;
                        //Console.WriteLine($"Candidate Created=> FT CandidateId: {updateResponse.Id } HRLink MemberId: {candidateAddEdit.alternateNumberOne} Surname: {candidateAddEdit.surname}");
                        Logger.Info($"Candidate Created=> FT CandidateId: {updateResponse.Id } HRLink MemberId: {candidateAddEdit.alternateNumberOne} Surname: {candidateAddEdit.surname} FirstName: {candidateAddEdit.firstName}");
                    }
                    else
                    {
                        updateResponse.ErrorMessage = await GetErrorMessage(response, candidateId, candidateAddEdit);
                    }
                }
                else//Candidate does exist in fast track so update existing one
                {
                    //Console.WriteLine(JsonConvert.SerializeObject(candidateAddEdit));
                    //Console.WriteLine(JsonConvert.SerializeObject(candidate));
                    var response = await _candidateHttpClient.PutAsync(candidate.data.id.ToString(), candidateAddEdit);

                    if (response.IsSuccessStatusCode)
                    {
                        var c = await GetCandidateObject<CandidateOne>(response);
                        updateResponse.Id = c.data.id;
                        //Console.WriteLine($"Candidate Updated=> FT CandidateId: {updateResponse.Id } HRLink MemberId: {candidateAddEdit.alternateNumberOne} Surname: {candidateAddEdit.surname}");
                        Logger.Info($"Candidate Updated=> FT CandidateId: {updateResponse.Id } HRLink MemberId: {candidateAddEdit.alternateNumberOne} Surname: {candidateAddEdit.surname} FirstName: {candidateAddEdit.firstName}");
                    }
                    else
                    {
                        updateResponse.ErrorMessage = await GetErrorMessage(response, candidateId, candidateAddEdit);
                    }
                }
            }
            catch (Exception ex)
            {
               Logger.Error(ex);
                updateResponse.ErrorMessage = ex.Message;
            }
            
            return updateResponse;
        }

        private async Task<T> GetCandidateObject<T>(HttpResponseMessage response)
        {
            var cont = await response.Content.ReadAsStringAsync();
            var c = JsonConvert.DeserializeObject<T>(cont);
            return c;
        }
        private async Task<string> GetErrorMessage(HttpResponseMessage response, long? candidateId, CandidateAddEdit candidate)
        {
            var res = await response.Content.ReadAsStringAsync();
            var errObj = JsonConvert.DeserializeObject<Error>(res);            

            Console.WriteLine($"FT CandidateId: {candidateId} HRLink MemberId: {candidate.alternateNumberOne} Surname: {candidate.surname} {res}");
            Logger.Error($"FT CandidateId: {candidateId} HRLink MemberId: {candidate.alternateNumberOne} Surname: {candidate.surname} {res}");

            return $"{errObj.error?.errorUserMessage?.FirstOrDefault()} {errObj.error?.errorDeveloperMessage}";
        }
        /// <summary>
        /// Dispose all the resources
        /// </summary>
        public void Dispose()
        {
            _candidateHttpClient?.Dispose();
        }
    }
}