﻿using log4net;
using Newtonsoft.Json;
using Programmed.FT360.API.Abstract;
using Programmed.FT360.API.Model.Client;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Programmed.HRLink.Integration.Model.Api;

namespace Programmed.FT360.API.Endpoints.Client
{
    public class ClientApiService : IClientApiService, IDisposable
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ClientApiService));
        private readonly FT360HttpClient _clientHttpClient;

        public ClientApiService(string consumerUserName, string userName, string password, string baseUrl, string loginEndpoint, int tokenExpiryInterval, string clientEndpoint)
        {
            _clientHttpClient = new FT360HttpClient(consumerUserName, loginEndpoint,  tokenExpiryInterval, userName, password, baseUrl, clientEndpoint);
        }

        /// <summary>
        /// Get active clients from fast track
        /// </summary>
        /// <returns></returns>
        public async Task<Clients> GetClients()
        {
            var response = await _clientHttpClient.GetManyAsync();
            if (response.IsSuccessStatusCode)
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();

                    var clients = JsonConvert.DeserializeObject<Clients>(responseString);
                    clients.data = clients.data.Where(x => x.status?.id == HRLink.Integration.Common.Constants.ClientStatus.Active).ToList();
                    return clients;
                }
            }
            else
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();
                    Logger.Info(responseString);
                    return new Clients();
                }
            }
        }
        /// <summary>
        /// Get Client by Id from fast track
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<Model.Client.Client> GetClient(long clientId)
        {
            var response = await _clientHttpClient.GetAsyncById(clientId.ToString());

            if (response.IsSuccessStatusCode)
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();
                    var client = JsonConvert.DeserializeObject<Clients>(responseString);
                    return client.data.FirstOrDefault();
                }
            }
            else
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();
                    Logger.Info(responseString);
                    return new Model.Client.Client();
                }
            }           
        }
        /// <summary>
        /// Update client in Fast Track
        /// </summary>
        /// <param name="fTClientId"></param>
        /// <param name="clientAddEdit"></param>
        public async Task<UpdateResponse> UpdateClient(long? fTClientId, ClientAddEdit clientAddEdit)
        {
            var updateResponse = new UpdateResponse();
            try
            {
                Model.Client.Client client;
                if (fTClientId.HasValue)
                {
                    client = await GetClient(fTClientId.Value);
                }
                else
                {
                    client = new Model.Client.Client();
                }

                if (client.id == 0)//Client doesn't exist in fast track so create new one
                {
                    //Console.WriteLine(JsonConvert.SerializeObject(clientAddEdit));

                    var response = await _clientHttpClient.PostAsync(clientAddEdit);
                    if (response.IsSuccessStatusCode)
                    {
                        var c = await GetClientObject<Model.Client.Client>(response);
                        updateResponse.Id = c.id;
                        //Console.WriteLine($"Client Created=> FT ClientId: {updateResponse.Id} HRLink ClientId: {clientAddEdit.alternateNumber} Name: {clientAddEdit.name})");
                        Logger.Info($"Client Created=> FT ClientId: {updateResponse.Id} HRLink ClientId: {clientAddEdit.alternateNumber} Name: {clientAddEdit.name}");
                    }
                    else
                    {
                        updateResponse.ErrorMessage = await GetErrorMessage(response, fTClientId, clientAddEdit);
                    }
                }
                else//Client does exist in fast track so update existing one
                {
                    //Console.WriteLine(JsonConvert.SerializeObject(parentCompany));
                    var response = await _clientHttpClient.PutAsync(client.id.ToString(), clientAddEdit);
                    if (response.IsSuccessStatusCode)
                    {
                        var c = await GetClientObject<Clients>(response);
                        if (c.data != null && c.data.Any())
                        {
                            updateResponse.Id = c.data.First().id;
                            //Console.WriteLine($"Client Updated=> FT ClientId: {updateResponse.Id} HRLink ClientId: {clientAddEdit.alternateNumber} Name: {clientAddEdit.name})");
                            Logger.Info($"Client Updated=> FT ClientId: {updateResponse.Id} HRLink ClientId: {clientAddEdit.alternateNumber} Name: {clientAddEdit.name}");
                        }
                    }
                    else
                    {
                        updateResponse.ErrorMessage = await GetErrorMessage(response, fTClientId, clientAddEdit);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                updateResponse.ErrorMessage = ex.Message;
            }
            
            return updateResponse;
        }

        private async Task<T> GetClientObject<T>(HttpResponseMessage response)
        {
            var cont = await response.Content.ReadAsStringAsync();
            var c = JsonConvert.DeserializeObject<T>(cont);
            return c;
        }
        private async Task<string> GetErrorMessage(HttpResponseMessage response, long? clientId, ClientAddEdit client)
        {
            var res = await response.Content.ReadAsStringAsync();
            var errObj = JsonConvert.DeserializeObject<Error>(res);

            Console.WriteLine($"FT ClientId: {clientId} HRLink ClientId: {client.alternateNumber} Name: {client.name} {res}");
            Logger.Error($"FT ClientId: {clientId} HRLink ClientId: {client.alternateNumber} Name: {client.name} {res}");

            return $"{errObj.error?.errorUserMessage?.FirstOrDefault()} {errObj.error?.errorDeveloperMessage}";
        }
        /// <summary>
        /// Dispose all the resources
        /// </summary>
        public void Dispose()
        {
            _clientHttpClient?.Dispose();
        }
    }
}