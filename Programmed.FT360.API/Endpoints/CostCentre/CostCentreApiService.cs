﻿using log4net;
using Newtonsoft.Json;
using Programmed.FT360.API.Abstract;
using Programmed.HRLink.Integration.Model.Api;
using Programmed.HRLink.Integration.Model.Api.CostCentre;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Programmed.FT360.API.Endpoints.CostCentre
{
    public class CostCentreApiService : ICostCentreApiService, IDisposable
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CostCentreApiService));
        private readonly FT360HttpClient _costCentreHttpClient;
        public CostCentreApiService(string consumerUserName, string userName, string password, string baseUrl, string loginEndpoint, int tokenExpiryInterval, string candidateEndpoint)
        {
            _costCentreHttpClient = new FT360HttpClient(consumerUserName, loginEndpoint, tokenExpiryInterval, userName, password, baseUrl, candidateEndpoint);
        }
        /// <summary>
        /// Get active cost centres from fast track
        /// </summary>
        /// <returns></returns>
        public async Task<CostCentres> GetCostCentres()
        {
            var response = await _costCentreHttpClient.GetManyAsync();
            if (response.IsSuccessStatusCode)
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();

                    var costCentres = JsonConvert.DeserializeObject<CostCentres>(responseString);
                    costCentres.data = costCentres.data.Where(x => x.status?.id == HRLink.Integration.Common.Constants.CostCentreStatus.Active).ToList();
                    return costCentres;
                }
            }
            else
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();
                    Logger.Info(responseString);
                    return new CostCentres();
                }
            }
        }
        /// <summary>
        /// Get cost centre by id from fast track
        /// </summary>
        /// <param name="costCentreId"></param>
        /// <returns></returns>
        public async Task<HRLink.Integration.Model.Api.CostCentre.CostCentre> GetCostCentre(long costCentreId)
        {
            var response = await _costCentreHttpClient.GetAsyncById(costCentreId.ToString());

            if (response.IsSuccessStatusCode)
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();
                    var costCentres = JsonConvert.DeserializeObject<CostCentres>(responseString);
                    return costCentres.data.FirstOrDefault();
                }
            }
            else
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();
                    Logger.Info(responseString);
                    return new HRLink.Integration.Model.Api.CostCentre.CostCentre();
                }
            }
        }
        /// <summary>
        /// Update cost centre in Fast Track
        /// </summary>
        /// <param name="fTCostCentreId"></param>
        /// <param name="costCentreAddEdit"></param>
        /// <returns></returns>
        public async Task<UpdateResponse> UpdateCostCentre(long? fTCostCentreId, CostCentreAddEdit costCentreAddEdit)
        {
            var updateResponse = new UpdateResponse();

            try
            {
                HRLink.Integration.Model.Api.CostCentre.CostCentre costCentre;
                if (fTCostCentreId.HasValue)
                {
                    costCentre = await GetCostCentre(fTCostCentreId.Value);
                }
                else
                {
                    costCentre = new HRLink.Integration.Model.Api.CostCentre.CostCentre();
                }

                if (costCentre.id == 0)//Cost Centre doesn't exist in fast track so create new one
                {
                    var response = await _costCentreHttpClient.PostAsync(costCentreAddEdit);

                    if (response.IsSuccessStatusCode)
                    {
                        var c = await GetCostCentreObject<HRLink.Integration.Model.Api.CostCentre.CostCentre>(response);
                        updateResponse.Id = c.id;
                        //Console.WriteLine($"Cost Centre Created=> FT CostCentreId: {updateResponse.Id} HRLink CostCentreId: {costCentreAddEdit.alternateNumber} Name: {costCentreAddEdit.name} Code: {costCentreAddEdit.code}");
                        Logger.Info($"Cost Centre Created=> FT CostCentreId: {updateResponse.Id} HRLink CostCentreId: {costCentreAddEdit.alternateNumber} Name: {costCentreAddEdit.name} Code: {costCentreAddEdit.code} HRLink ClientId: {costCentreAddEdit.clientid}");
                    }
                    else
                    {
                        updateResponse.ErrorMessage = await GetErrorMessage(response, fTCostCentreId, costCentreAddEdit);
                    }
                }
                else //Cost Centre does exist in fast track so update existing one
                {
                    var response = await _costCentreHttpClient.PutAsync(costCentre.id.ToString(), costCentreAddEdit);

                    if (response.IsSuccessStatusCode)
                    {
                        var c = await GetCostCentreObject<CostCentres>(response);
                        if (c.data != null && c.data.Any())
                        {
                            updateResponse.Id = c.data.First().id;
                            //Console.WriteLine($"Cost Centre Updated=> FT CostCentreId: {updateResponse.Id} HRLink CostCentreId: {costCentreAddEdit.alternateNumber} Name: {costCentreAddEdit.name} Code: {costCentreAddEdit.code}");
                            Logger.Info($"Cost Centre Updated=> FT CostCentreId: {updateResponse.Id} HRLink CostCentreId: {costCentreAddEdit.alternateNumber} Name: {costCentreAddEdit.name} Code: {costCentreAddEdit.code} HRLink ClientId: {costCentreAddEdit.clientid}");
                        }
                    }
                    else
                    {
                        updateResponse.ErrorMessage = await GetErrorMessage(response, fTCostCentreId, costCentreAddEdit);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                updateResponse.ErrorMessage = ex.Message;
            }
            
            return updateResponse;
        }

        private async Task<T> GetCostCentreObject<T>(HttpResponseMessage response)
        {
            var cont = await response.Content.ReadAsStringAsync();
            var c = JsonConvert.DeserializeObject<T>(cont);
            return c;
        }
        private async Task<string> GetErrorMessage(HttpResponseMessage response, long? fTCostCentreId, CostCentreAddEdit costCentreAddEdit)
        {
            var res = await response.Content.ReadAsStringAsync();
            var errObj = JsonConvert.DeserializeObject<Error>(res);

            Console.WriteLine($"FT CostCentreId: {fTCostCentreId} HRLink CostCentreId: {costCentreAddEdit.alternateNumber} Name: {costCentreAddEdit.name} Code: {costCentreAddEdit.code} {res}");
            Logger.Error($"FT CostCentreId: {fTCostCentreId} HRLink CostCentreId: {costCentreAddEdit.alternateNumber} Name: {costCentreAddEdit.name} Code: {costCentreAddEdit.code} {res}");

            return $"{errObj.error?.errorUserMessage?.FirstOrDefault()} {errObj.error?.errorDeveloperMessage}";
        }
        /// <summary>
        /// Dispose all the resources
        /// </summary>
        public void Dispose()
        {
            _costCentreHttpClient?.Dispose();
        }
    }
}
