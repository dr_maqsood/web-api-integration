﻿using System;
using System.ComponentModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;
using Programmed.FT360.API.Abstract;

namespace Programmed.FT360.API.Endpoints
{
    public class FT360HttpClient : HttpClientHandler, IDisposable, IFT360HttpClient// where T : class
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(FT360HttpClient));

        private string _authorizationToken;
        private DateTime _authroizationTime;       
        private HttpClient _httpClient;
        private bool _disposed = false;        
        private readonly string _addressSuffix;
        private readonly string _jsonMediaType = "application/json";
        private string _loginEndpoint { get; }
        private string _userName { get; }
        private string _password { get; }
        private string _consumerUserName { get; }
        private string _baseUrl { get; }        
        private int _tokenExpiryInterval { get; }

        /// <summary>
        /// Create new Fast Track 360 HttpClient
        /// </summary>
        /// <param name="consumerUserName"></param>
        /// <param name="tokenExpiryInterval"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="baseUrl"></param>
        /// <param name="addressSuffix"></param>
        /// <param name="loginEndpoint"></param>
        public FT360HttpClient(string consumerUserName, string loginEndpoint, int tokenExpiryInterval, string userName, string password, string baseUrl, string addressSuffix)
        {
            _userName = userName;
            _loginEndpoint = loginEndpoint;
            _tokenExpiryInterval = tokenExpiryInterval;
            _consumerUserName = consumerUserName;
            _password = password;
            _baseUrl = baseUrl;
            _addressSuffix = addressSuffix;
            CreateHttpClient();
        }

        /// <summary>
        /// Create an HttpClient
        /// </summary>
        /// <returns></returns>
        protected HttpClient CreateHttpClient()
        {
            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(_baseUrl)
            };
            _httpClient.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse(_jsonMediaType));
            _httpClient.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(new ProductHeaderValue("Programmed.HRLink", "1.0")));
            _httpClient.DefaultRequestHeaders.Add("X-Consumer-Username", _consumerUserName);
            _authorizationToken = "Token";
            _httpClient.DefaultRequestHeaders.Add("Authorization", _authorizationToken);
            return _httpClient;
        }
        /// <summary>
        /// Refresh authorization token from fast track
        /// </summary>
        /// <returns></returns>
        private async Task RefreshAuthorizationToken()
        {
            var response = await _httpClient.PostAsJsonAsync($"{_baseUrl}/{_loginEndpoint}", new FTCredentials { userName = _userName, password = _password });

            if (response.IsSuccessStatusCode)
            {
                var responseString = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<Authorization>(responseString);
                _authroizationTime = DateTime.Now;
                _authorizationToken = result.authorization;
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(_authorizationToken);
            }
            else
            {
                var responseString = await response.Content.ReadAsStringAsync();
                Logger.Error($"Fast Track 360 Login Error: {responseString}");
            }
        }
        /// <summary>
        /// Check if authorization token has expired or not
        /// </summary>
        /// <returns></returns>
        private bool HasAuthorizationExpired()
        {
            if (_authorizationToken == null || _authroizationTime.AddHours(_tokenExpiryInterval) < DateTime.Now)
                return true;
            else
                return false;
        }
        /// <summary>
        /// Refresh token if required
        /// </summary>
        public async Task RefreshTokenIfRequired()
        {
            if (HasAuthorizationExpired())
            {
                await RefreshAuthorizationToken();
            }
        }
        /// <summary>
        /// FT credentials
        /// </summary>
        private class FTCredentials
        {
            public string userName { get; set; }
            public string password { get; set; }
        }
        /// <summary>
        /// authorization token
        /// </summary>
        private class Authorization
        {
            public string authorization { get; set; }
        }
        /// <summary>
        /// Get response message from fast track
        /// </summary>
        /// <returns></returns>
        public async Task<HttpResponseMessage> GetManyAsync()
        {
            await RefreshTokenIfRequired();
            var responseMessage = await _httpClient.GetAsync(_addressSuffix);
            return responseMessage;
        }
        /// <summary>
        /// Get response message from fast track by Id
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> GetAsyncById(string identifier)
        {
            await RefreshTokenIfRequired();
            var responseMessage = await _httpClient.GetAsync(_addressSuffix + "/" + identifier);
            return responseMessage;
        }
        /// <summary>
        /// Post an object to fast track
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> PostAsync<T>(T model)
        {
            await RefreshTokenIfRequired();
            var responseMessage = await _httpClient.PostAsJsonAsync(_addressSuffix, model);
            return responseMessage;
        }
        /// <summary>
        /// Put an object to fast track
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="identifier"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> PutAsync<T>(string identifier, T model)
        {
            await RefreshTokenIfRequired();
            var responseMessage = await _httpClient.PutAsJsonAsync(_addressSuffix + "/" + identifier, model);
            return responseMessage;
        }
        /// <summary>
        /// Delete an object from fast track
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public async Task DeleteAsync(string identifier)
        {
            await RefreshTokenIfRequired();
            await _httpClient.DeleteAsync(_addressSuffix + "/" + identifier);
        }

        #region IDisposable Members

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            private void Dispose(bool disposing)
            {
                if (!_disposed && disposing)
                {
                    if (_httpClient != null)
                    {
                        var hc = _httpClient;
                        _httpClient = null;
                        hc.Dispose();
                    }
                    _disposed = true;
                }
            }

            #endregion IDisposable Members        
    }
}