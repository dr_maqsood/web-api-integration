﻿using log4net;
using Newtonsoft.Json;
using Programmed.FT360.API.Abstract;
using Programmed.FT360.API.Model.ParentCompany;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Programmed.HRLink.Integration.Model.Api;

namespace Programmed.FT360.API.Endpoints.ParentCompany
{
    public class ParentCompanyApiService : IParentCompanyApiService, IDisposable
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ParentCompanyApiService));
        private readonly FT360HttpClient _parentCompanyHttpClient;

        public ParentCompanyApiService(string consumerUserName, string userName, string password, string baseUrl, string loginEndpoint, int tokenExpiryInterval, string parentCompanyEndpoint)
        {
            _parentCompanyHttpClient = new FT360HttpClient(consumerUserName, loginEndpoint, tokenExpiryInterval, userName, password, baseUrl, parentCompanyEndpoint);
        }

        /// <summary>
        /// Get parent companies from fast track
        /// </summary>
        /// <returns></returns>
        public async Task<ParentCompanies> GetParentCompanies()
        {
            var response = await _parentCompanyHttpClient.GetManyAsync();
            if (response.IsSuccessStatusCode)
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();

                    var parentCompanies = JsonConvert.DeserializeObject<ParentCompanies>(responseString);
                    return parentCompanies;
                }
            }
            else
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();
                    Logger.Info(responseString);
                    return new ParentCompanies();
                }
            }
        }
        /// <summary>
        /// Get parent company by Id from fast track
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public async Task<Model.ParentCompany.ParentCompany> GetParentCompany(long parentId)
        {
            var response = await _parentCompanyHttpClient.GetAsyncById(parentId.ToString());

            if (response.IsSuccessStatusCode)
            {
                using (HttpContent content = response.Content)
                {
                    string parentCompanyResponseString = await content.ReadAsStringAsync();
                    var parentCompany = JsonConvert.DeserializeObject<ParentCompanies>(parentCompanyResponseString);
                    return parentCompany.data.FirstOrDefault();
                }
            }
            else
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();
                    Logger.Info(responseString);
                    return new Model.ParentCompany.ParentCompany();
                }
            }           
        }
        /// <summary>
        /// Update/Create Candidate in Fast Track
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="parentCompanyAddEdit"></param>
        /// <returns></returns>
        public async Task<UpdateResponse> UpdateParentCompany(long? parentId, ParentCompanyAddEdit parentCompanyAddEdit)
        {
            var updateResponse = new UpdateResponse();
            try
            {
                Model.ParentCompany.ParentCompany parentCompany;
                if (parentId.HasValue)
                {
                    parentCompany = await GetParentCompany(parentId.Value);
                }
                else
                {
                    parentCompany = new Model.ParentCompany.ParentCompany();
                }

                if (parentCompany.id == 0)//Parent Company doesn't exist in fast track so create new one
                {
                    //Console.WriteLine(JsonConvert.SerializeObject(candidate));

                    var response = await _parentCompanyHttpClient.PostAsync(parentCompanyAddEdit);
                    if (response.IsSuccessStatusCode)
                    {
                        var c = await GetParentCompanyObject<Model.ParentCompany.ParentCompany>(response);
                        updateResponse.Id = c.id;
                        //Console.WriteLine($"Parent Company Created=> FT ParentCompanyId: {updateResponse.Id} HRLink NetworkId: {parentCompanyAddEdit.alternateNumber} Name: {parentCompanyAddEdit.name}");
                        Logger.Info($"Parent Company Created=> FT ParentCompanyId: {updateResponse.Id} HRLink NetworkId: {parentCompanyAddEdit.alternateNumber} Name: {parentCompanyAddEdit.name}");
                    }
                    else
                    {
                        updateResponse.ErrorMessage = await GetErrorMessage(response, parentId, parentCompanyAddEdit);
                    }
                }
                else //Parent Company does exist in fast track so update existing one
                {
                    //Console.WriteLine(JsonConvert.SerializeObject(parentCompany));
                    var response = await _parentCompanyHttpClient.PutAsync(parentId.ToString(), parentCompanyAddEdit);
                    if (response.IsSuccessStatusCode)
                    {
                        var c = await GetParentCompanyObject<ParentCompanies>(response);
                        if (c.data != null && c.data.Any())
                        {
                            updateResponse.Id = c.data.First().id;
                            //Console.WriteLine($"Parent Company Updated=> FT ParentCompanyId: {updateResponse.Id} HRLink NetworkId: {parentCompanyAddEdit.alternateNumber} Name: {parentCompanyAddEdit.name}");
                            Logger.Info($"Parent Company Updated=> FT ParentCompanyId: {updateResponse.Id} HRLink NetworkId: {parentCompanyAddEdit.alternateNumber} Name: {parentCompanyAddEdit.name}");
                        }
                    }
                    else
                    {
                        updateResponse.ErrorMessage = await GetErrorMessage(response, parentId, parentCompanyAddEdit);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                updateResponse.ErrorMessage = ex.Message;
            }
            
            return updateResponse;
        }

        private async Task<T> GetParentCompanyObject<T>(HttpResponseMessage response)
        {
            var cont = await response.Content.ReadAsStringAsync();
            var c = JsonConvert.DeserializeObject<T>(cont);
            return c;
        }
        private async Task<string> GetErrorMessage(HttpResponseMessage response, long? parentCompanyId, ParentCompanyAddEdit parentCompany)
        {
            var res = await response.Content.ReadAsStringAsync();
            var errObj = JsonConvert.DeserializeObject<Error>(res);

            Console.WriteLine($"FT ParentCompanyId: {parentCompanyId} HRLink NetworkId: {parentCompany.alternateNumber} Name: {parentCompany.name} {res}");
            Logger.Error($"FT ParentCompanyId: {parentCompanyId} HRLink NetworkId: {parentCompany.alternateNumber} Name: {parentCompany.name} {res}");

            return $"{errObj.error?.errorUserMessage?.FirstOrDefault()} {errObj.error?.errorDeveloperMessage}";
        }
        /// <summary>
        /// Dispose all the resources
        /// </summary>
        public void Dispose()
        {
            _parentCompanyHttpClient?.Dispose();
        }
    }
}