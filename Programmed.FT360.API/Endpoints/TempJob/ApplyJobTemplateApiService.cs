﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;
using Programmed.FT360.API.Abstract;
using Programmed.FT360.API.Model;

namespace Programmed.FT360.API.Endpoints.TempJob
{
    public class ApplyJobTemplateApiService : IApplyJobTemplateApiService, IDisposable
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ApplyJobTemplateApiService));
        private readonly FT360HttpClient _applyJobTemplateHttpClient;

        public ApplyJobTemplateApiService(string consumerUserName, string userName, string password, string baseUrl, string loginEndpoint, int tokenExpiryInterval, string applyJobTemplateEndpoint)
        {
            _applyJobTemplateHttpClient = new FT360HttpClient(consumerUserName, loginEndpoint, tokenExpiryInterval, userName, password, baseUrl, applyJobTemplateEndpoint);
        }
        /// <summary>
        /// Apply job template to job order in fast track
        /// </summary>
        /// <returns></returns>
        public async Task<JobOrderJobTemplate.JobOrderJobTemplates> ApplyJobTemplate(long jobOrderId, long jobTemplateId)
        {
            var response = await _applyJobTemplateHttpClient.PutAsync($"{jobOrderId}/jobTemplate", new JobTemplateId {jobTemplateId = jobTemplateId});
            try
            {
                if (response.IsSuccessStatusCode)
                {
                    using (HttpContent content = response.Content)
                    {
                        string jobTemplateResponseString = await content.ReadAsStringAsync();
                        var jobTemplates = JsonConvert.DeserializeObject<JobOrderJobTemplate.JobOrderJobTemplates>(jobTemplateResponseString);
                        
                        return jobTemplates;

                    }
                }
                else
                {
                    using (HttpContent content = response.Content)
                    {
                        string responseString = await content.ReadAsStringAsync();
                        Logger.Info(responseString);
                        return new JobOrderJobTemplate.JobOrderJobTemplates();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error(ex);
                return new JobOrderJobTemplate.JobOrderJobTemplates();
            }
            
        }
        /// <summary>
        /// Dispose all the resources
        /// </summary>
        public void Dispose()
        {
            _applyJobTemplateHttpClient?.Dispose();
        }
    }
}
