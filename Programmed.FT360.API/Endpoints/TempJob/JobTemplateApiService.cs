﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;
using Programmed.FT360.API.Abstract;
using Programmed.FT360.API.Model;

namespace Programmed.FT360.API.Endpoints.TempJob
{
    public class JobTemplateApiService : IJobTemplateApiService, IDisposable
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(JobTemplateApiService));
        private readonly FT360HttpClient _jobTemplateHttpClient;

        public JobTemplateApiService(string consumerUserName, string userName, string password, string baseUrl, string loginEndpoint, int tokenExpiryInterval, string jobTemplateEndpoint)
        {
            _jobTemplateHttpClient = new FT360HttpClient(consumerUserName, loginEndpoint, tokenExpiryInterval, userName, password, baseUrl, jobTemplateEndpoint);
        }
        /// <summary>
        /// Get job templates from fast track
        /// </summary>
        /// <returns></returns>
        public async Task<List<Model.JobTemplateMethod>> GetJobTemplates()
        {
            var response = await _jobTemplateHttpClient.GetManyAsync();

            if (response.IsSuccessStatusCode)
            {
                using (HttpContent content = response.Content)
                {
                    string jobTemplateResponseString = await content.ReadAsStringAsync();
                    var jobTemplates = JsonConvert.DeserializeObject<List<Model.JobTemplateMethod>>(jobTemplateResponseString);
                    return jobTemplates;
                        
                }
            }
            else
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();
                    Logger.Info(responseString);
                    return new List<JobTemplateMethod>();
                }
            }
        }
        /// <summary>
        /// Dispose all the resources
        /// </summary>
        public void Dispose()
        {
            _jobTemplateHttpClient?.Dispose();
        }
    }
}
