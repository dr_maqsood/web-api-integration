﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;
using Programmed.FT360.API.Abstract;
using Programmed.FT360.API.Model.TempJob;
using Programmed.HRLink.Integration.Model.Api;

namespace Programmed.FT360.API.Endpoints.TempJob
{
    public class TempJobApiService : ITempJobApiService, IDisposable
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TempJobApiService));
        private readonly FT360HttpClient _tempJobHttpClient;

        public TempJobApiService(string consumerUserName, string userName, string password, string baseUrl, string loginEndpoint, int tokenExpiryInterval, string tempJobEndpoint)
        {
            _tempJobHttpClient = new FT360HttpClient(consumerUserName, loginEndpoint, tokenExpiryInterval, userName, password, baseUrl, tempJobEndpoint);
        }
        /// <summary>
        /// Get temp job by Id from fast track
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        public async Task<Model.TempJob.TempJob> GetTempJob(long jobId)
        {
            var response = await _tempJobHttpClient.GetAsyncById(jobId.ToString());

            if (response.IsSuccessStatusCode)
            {
                using (HttpContent content = response.Content)
                {
                    string tempJobResponseString = await content.ReadAsStringAsync();
                    var jobTempJobs = JsonConvert.DeserializeObject<TempJobs>(tempJobResponseString);
                    return jobTempJobs.data.FirstOrDefault();
                }
            }
            else
            {
                using (HttpContent content = response.Content)
                {
                    string responseString = await content.ReadAsStringAsync();
                    Logger.Info(responseString);
                    return new Model.TempJob.TempJob();
                }
            }
        }
        /// <summary>
        /// Update/Create Temp Job in Fast Track
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="tempJobAddEdit"></param>
        /// <returns></returns>
        public async Task<UpdateResponse> UpdateTempJob(long? jobId, TempJobAddEdit tempJobAddEdit)
        {
            var updateResponse = new UpdateResponse();

            Model.TempJob.TempJob tempJob;
            if (jobId.HasValue)
            {
                tempJob = await GetTempJob(jobId.Value);
            }
            else
            {
                tempJob = new Model.TempJob.TempJob();
            }

            if (tempJob.id == 0)//Temp Job doesn't exist in fast track so create new one
            {
                //var a = _tempJobHttpClient.GetManyAsync().Result;
                //var s = await a.Content.ReadAsStringAsync();
                //Console.WriteLine(s);
                var response = await _tempJobHttpClient.PostAsync(tempJobAddEdit);
                if (response.IsSuccessStatusCode)
                {
                    var c = await GetTempJobObject<Model.TempJob.TempJob>(response);
                    updateResponse.Id = c.id;
                    Logger.Info($"Temp Job Created=> FT TempJobId: {updateResponse.Id} HRLink ShiftId: {tempJobAddEdit.alternateNumber} ClientId: {tempJobAddEdit.clientId}");
                }
                else
                {
                    updateResponse.ErrorMessage = await GetErrorMessage(response, jobId, tempJobAddEdit);
                }
            }
            else //Temp Job does exist in fast track so update existing one
            {
                var response = await _tempJobHttpClient.PutAsync(jobId.ToString(), tempJobAddEdit);
                if (response.IsSuccessStatusCode)
                {
                    var c = await GetTempJobObject<TempJobs>(response);
                    if (c.data != null && c.data.Any())
                    {
                        updateResponse.Id = c.data.First().id;
                        Logger.Info($"Temp Job Updated=> FT TempJobId: {updateResponse.Id} HRLink ShiftId: {tempJobAddEdit.alternateNumber} ClientId: {tempJobAddEdit.clientId}");
                    }
                }
                else
                {
                    updateResponse.ErrorMessage = await GetErrorMessage(response, jobId, tempJobAddEdit);
                }
            }
            return updateResponse;
        }

        private async Task<T> GetTempJobObject<T>(HttpResponseMessage response)
        {
            var cont = await response.Content.ReadAsStringAsync();
            var c = JsonConvert.DeserializeObject<T>(cont);
            return c;
        }
        private async Task<string> GetErrorMessage(HttpResponseMessage response, long? tempJobId, TempJobAddEdit tempJobAddEdit)
        {
            var res = await response.Content.ReadAsStringAsync();
            var errObj = JsonConvert.DeserializeObject<Error>(res);

            Console.WriteLine($"FT TempJobId: {tempJobId} HRLink ShiftId: {tempJobAddEdit.alternateNumber} ClientId: {tempJobAddEdit.clientId} {res}");
            Logger.Error($"FT TempJobId: {tempJobId} HRLink ShiftId: {tempJobAddEdit.alternateNumber} ClientId: {tempJobAddEdit.clientId} {res}");

            return $"{errObj.error.errorUserMessage?.FirstOrDefault()} {errObj.error.errorDeveloperMessage}";
        }
        /// <summary>
        /// Dispose all the resources
        /// </summary>
        public void Dispose()
        {
            _tempJobHttpClient?.Dispose();
        }
    }
}
