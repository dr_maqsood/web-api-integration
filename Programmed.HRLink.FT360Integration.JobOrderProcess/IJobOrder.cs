﻿using System.Collections.Generic;
using Programmed.FT360.API.Endpoints.TempJob;
using System.Threading.Tasks;
using Programmed.HRLink.Integration.Model;

namespace Programmed.HRLink.FT360Integration.JobOrderProcess
{
    public interface IJobOrder
    {
        /// <summary>
        /// Transfer HRLink shifts to FastTrack
        /// </summary>
        /// <param name="locationId"></param>
        /// <param name="tempJobApiService"></param>
        /// <param name="jobTemplateApiService"></param>
        /// <param name="applyJobTemplateApiService"></param>
        /// <returns></returns>
        Task<List<JobOrderProcessResponse>> ProcessJobOrders(int locationId, TempJobApiService tempJobApiService, JobTemplateApiService jobTemplateApiService, ApplyJobTemplateApiService applyJobTemplateApiService);
    }
}
