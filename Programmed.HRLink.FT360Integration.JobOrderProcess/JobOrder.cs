﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Programmed.FT360.API.Endpoints.TempJob;
using Programmed.FT360.API.Model.TempJob;
using Programmed.HRLink.Integration.Database.Services;
using Programmed.HRLink.Integration.Model;
using Programmed.HRLink.Integration.Model.Api;

namespace Programmed.HRLink.FT360Integration.JobOrderProcess
{
    public class JobOrder : IJobOrder
    {
        /// <summary>
        /// Transfer HRLink shifts to FastTrack and update JobId for each Shift
        /// </summary>
        /// <param name="locationId"></param>
        /// <param name="tempJobApiService"></param>
        /// <param name="jobTemplateApiService"></param>
        /// <param name="applyJobTemplateApiService"></param>
        /// <returns></returns>
        public async Task<List<JobOrderProcessResponse>> ProcessJobOrders(int locationId, TempJobApiService tempJobApiService, JobTemplateApiService jobTemplateApiService, ApplyJobTemplateApiService applyJobTemplateApiService)
        {
            var updateResponsList = new List<JobOrderProcessResponse>();
            try
            {
                var shiftService = new ShiftService();
                var shifts = shiftService.GetUnpaidShifts(locationId);
                var jobTemplates = await jobTemplateApiService.GetJobTemplates();
                foreach (var shift in shifts)
                {
                    var updateResponse = new JobOrderProcessResponse {ShiftId = shift.ShiftID};
                    try
                    {
                        var tempJobAddEdit = new TempJobAddEdit
                        {
                            alternateNumber = shift.ShiftID.ToString(),
                            clientId = shift.FTClientId,
                            candidateId = shift.FTCandidateId,
                            startDate = shift.StartDate,
                            endDate = shift.FinishDate,
                            costCentreId = shift.FTCostCentreId,
                            skillGroupId = 1,
                            statusId = 461,
                            officeId = 1,
                            filledByUserId = 900000001,
                            sourceId = 1,
                            creatorUserId = 900000018,
                            positionId = 2,
                            ownerUserId = 900000015,
                            probabilityFill = 2.0,
                            needReasonId = 6,
                            typeId = 7,
                            workTypeId = 1,
                            jobOrderServiceTypeId = 467,
                            exclusiveDate = DateTime.Now,
                            timesheetFrequencyDetails = new TempJobAddEdit.TimesheetFrequencyDetails(),
                            otherAgency = new TempJobAddEdit.OtherAgency(),
                            workplace = new Workplace(),
                            orderDate = DateTime.Now
                        };
                        var response = await tempJobApiService.UpdateTempJob(null, tempJobAddEdit);

                        if (response.Id > 0)
                        {
                            //update FTJobId in HRLink for this shift
                            shiftService.UpdateJobId(shift.ShiftID, response.Id);

                            //construct the template name
                            string jobTemplateName;
                            if (shift.InvoiceSplitType == InvoiceSplitType.CostCentre)
                            {
                                //ClientId-CostCentreId-Grade
                                jobTemplateName = $"{shift.FTClientId}-{shift.FTCostCentreId}-{shift.PayClassName}";
                            }
                            else
                            {
                                //ClientId-Grade
                                jobTemplateName = $"{shift.FTClientId}-{shift.PayClassName}";
                            }

                            //find the job template
                            var jobTempalte = jobTemplates.FirstOrDefault(x => x.jobTemplateName == jobTemplateName);
                            if (jobTempalte != null)
                            {
                                //apply jop template to this job
                                var a = await applyJobTemplateApiService.ApplyJobTemplate(response.Id, jobTempalte.jobTemplateId);

                                //submit job order to timesheet
                                tempJobAddEdit.submitToTimeSheet = true;
                                response = await tempJobApiService.UpdateTempJob(response.Id, tempJobAddEdit);
                            }
                            else
                            {
                                updateResponse.ErrorMessage += $" Can't find job template for {jobTemplateName}";
                            }
                        }

                        updateResponse.JobId = response.Id;
                        updateResponse.ErrorMessage = response.ErrorMessage;
                        
                    }
                    catch (Exception ex)
                    {
                        updateResponse.ErrorMessage = ex.Message;
                    }
                    updateResponsList.Add(updateResponse);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return updateResponsList;
        }
    }

    public static class InvoiceSplitType
    {
        public static int CostCentre = 3;
    }
}