﻿using Programmed.HRLink.Integration.Database.Model;
using System.Collections.Generic;

namespace Programmed.HRLink.Integration.Database.Abstract
{
    public interface IClientNetworkService
    {
        List<ClientNetwork> GetLinkedClientNetworks();
        List<ClientNetwork> GetClientNetworksToBeSent();
        //void UpdateClietnNetworkFTParentId(ClientNetwork clientNetwork);
        void UpdateClientNetworkFTStatus(ClientNetwork clientNetwork);
        long? GetClientNetworkParentId(int clientNetworkId);
    }
}
