﻿using System.Collections.Generic;

namespace Programmed.HRLink.Integration.Database.Abstract
{
    public interface IClientService
    {
        List<Model.Client> GetLinkedActiveClients();
        List<Model.Client> GetClientsToBeSent();
        //void UpdateFTClientId(Model.Client client);
        void UpdateClientFTStatus(Model.Client client);
    }
}