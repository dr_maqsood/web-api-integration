﻿using System.Collections.Generic;
using Programmed.FT360.API.Model.ParentCompany;
using Programmed.HRLink.Integration.Database.Model;
using Programmed.HRLink.Integration.Model.Api;
using Programmed.HRLink.Integration.Model.Api.CostCentre;
using Programmed.HRLink.Integration.Model.Database;


namespace Programmed.HRLink.Integration.Database.Abstract
{
    public interface ICompareService
    {
        List<string> CompareMembers(IList<Candidate> ftCandidates, List<Member> hrMembers);
        List<string> CompareClients(IList<FT360.API.Model.Client.Client> ftClient, List<Client> hrClients);
        List<string> CompareClientNetworks(IList<ParentCompany> ftParentCompanies, List<ClientNetwork> hrClientNetworks);
        List<string> CompareCostCenters(IList<CostCentre> ftCostCentres, List<WardCostCentreDate> hrCostCentres);
        bool SendEmail(string smtpServer, string from, string to, string subject, string body);
    }
}