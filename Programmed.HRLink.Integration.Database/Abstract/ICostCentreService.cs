﻿using Programmed.HRLink.Integration.Model.Database;
using System.Collections.Generic;

namespace Programmed.HRLink.Integration.Database.Abstract
{
    public interface ICostCentreService
    {
        List<WardCostCentreDate> GetLinkedActiveCostCentres();
        List<WardCostCentreDate> GetCostCentresToBeSent();
        //void UpdateFTCostCentreId(WardCostCentreDate costCentre);
        void UpdateCostCentreFTStatus(WardCostCentreDate costCentre);
    }
}
