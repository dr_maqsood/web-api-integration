﻿using Programmed.HRLink.Integration.Database.Model;
using System.Collections.Generic;
using Programmed.HRLink.Integration.Model.Api;

namespace Programmed.HRLink.Integration.Database.Abstract
{
    public interface IMemberService
    {
        List<Member> GetLinkedActiveMembers();
        bool UpdateMembers(Candidates candidates);
        void UpdateMemberFTStatus(Member member);
        //void UpdateMemberCandidateId(Member member);
        List<Member> GetMembersToBeSent();
    }
}