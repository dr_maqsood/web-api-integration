﻿using System.Collections.Generic;
using Programmed.HRLink.Integration.Model.Database;

namespace Programmed.HRLink.Integration.Database.Abstract
{
    public interface IShiftService
    {
        List<LockedShift> GetUnpaidShifts(int? locationId);
        List<Shift> GetLockedShifts(int locationId);
        bool UpdateJobId(int shiftId, long jobId);
    }
}