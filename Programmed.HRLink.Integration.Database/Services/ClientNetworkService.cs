﻿using log4net;
using Programmed.HRLink.Integration.Database.Abstract;
using Programmed.HRLink.Integration.Database.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Programmed.HRLink.Integration.Database.Services
{
    public class ClientNetworkService : IClientNetworkService
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ClientNetworkService));

        /// <summary>
        /// Get linked client networks
        /// </summary>
        /// <returns></returns>
        public List<ClientNetwork> GetLinkedClientNetworks()
        {
            using (var hrLink = new HRLinkDBContext())
            {
                try
                {
                    var hrClientNetworks = hrLink.ClientNetworks.Where(x => x.FTParentId != null);

                    return hrClientNetworks.ToList();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Logger.Error(ex);
                }
            }
            return new List<ClientNetwork>();
        }
        /// <summary>
        /// Get client networks which has changed
        /// </summary>
        /// <returns></returns>
        public List<ClientNetwork> GetClientNetworksToBeSent()
        {
            using (var hrLink = new HRLinkDBContext())
            {
                try
                {
                    var hrClientNetworks = hrLink.ClientNetworks.Where(x => x.FTStatus == Common.Constants.FTStatus.ToBeSent);

                    return hrClientNetworks.ToList();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Logger.Error(ex);
                }
            }
            return new List<ClientNetwork>();
        }
        ///// <summary>
        ///// Update parent id
        ///// </summary>
        ///// <param name="clientNetwork"></param>
        //public void UpdateClietnNetworkFTParentId(ClientNetwork clientNetwork)
        //{
        //    using (var hrLink = new HRLinkDBContext())
        //    {
        //        try
        //        {
        //            var hrClientNetwork = hrLink.ClientNetworks.FirstOrDefault(x => x.ClientNetworkID == clientNetwork.ClientNetworkID);

        //            if(hrClientNetwork != null)
        //            {
        //                hrClientNetwork.FTParentId = clientNetwork.FTParentId;
        //                hrLink.SaveChanges();
        //            }                    
        //        }
        //        catch (Exception ex)
        //        {
        //            Console.WriteLine(ex.ToString());
        //            Logger.Error(ex);
        //        }
        //    }
        //}
        /// <summary>
        /// update ft status
        /// </summary>
        /// <param name="clientNetwork"></param>
        public void UpdateClientNetworkFTStatus(ClientNetwork clientNetwork)
        {
            using (var hrLink = new HRLinkDBContext())
            {
                try
                {
                    var hrClientNetwork = hrLink.ClientNetworks.FirstOrDefault(x => x.ClientNetworkID == clientNetwork.ClientNetworkID);

                    if (hrClientNetwork != null)
                    {
                        hrClientNetwork.FTParentId = clientNetwork.FTParentId;
                        hrClientNetwork.FTStatus = clientNetwork.FTStatus;
                        hrClientNetwork.FTErrorDescription = clientNetwork.FTErrorDescription;
                        hrLink.SaveChanges();
                    }                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Logger.Error($"ClientNetworkId: {clientNetwork.ClientNetworkID}", ex);
                }
            }
        }
        /// <summary>
        /// Get client network parent id
        /// </summary>
        /// <param name="clientNetworkId"></param>
        /// <returns></returns>
        public long? GetClientNetworkParentId(int clientNetworkId)
        {
            using (var hrLink = new HRLinkDBContext())
            {
                try
                {
                    var hrClientNetwork = hrLink.ClientNetworks.FirstOrDefault(x => x.ClientNetworkID == clientNetworkId);
                    if (hrClientNetwork != null)
                        return hrClientNetwork.FTParentId;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Logger.Error($"ClientNetworkId: {clientNetworkId}", ex);
                }
            }
            return null;
        }
    }
}
