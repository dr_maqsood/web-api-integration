﻿using log4net;
using Programmed.HRLink.Integration.Database.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Programmed.HRLink.Integration.Database.Services
{
    public class ClientService : IClientService
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ClientService));
        /// <summary>
        /// Get linked active clients
        /// </summary>
        /// <returns></returns>
        public List<Model.Client> GetLinkedActiveClients()
        {
            using (var hrLink = new HRLinkDBContext())
            {
                try
                {
                    var hrClients = hrLink.Clients.Include("ClientFinance").
                        Where(x => x.FTClientId != null && x.EnabledStatus != null && x.EnabledStatus.Value);

                    return hrClients.ToList();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Logger.Error(ex);
                }
            }
            return new List<Model.Client>();
        }
        /// <summary>
        /// Get clients which are to be sent to Fast Track
        /// </summary>
        /// <returns></returns>
        public List<Model.Client> GetClientsToBeSent()
        {
            using (var hrLink = new HRLinkDBContext())
            {
                try
                {                    
                    var hrClients = hrLink.Clients.Include("ClientFinance").
                        Where(x => x.FTStatus == Common.Constants.FTStatus.ToBeSent && 
                                   //new record with Active Status only
                                   (x.FTClientId == null && x.EnabledStatus != null && x.EnabledStatus.Value ||
                                    //existing record with any Status
                                   x.FTClientId != null));
                    
                    return hrClients.ToList();                    
                }                
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Logger.Error(ex);
                }
            }
            return new List<Model.Client>();
        }
        /// <summary>
        /// Get client by id
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public Model.Client GetClient(int clientId)
        {
            using (var hrLink = new HRLinkDBContext())
            {
                try
                {
                    var hrClients = hrLink.Clients.Include("ClientFinance").Where(x => x.ClientId == clientId);

                    return hrClients.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Logger.Error($"ClientId: {clientId}", ex);
                }
            }
            return new Model.Client();
        }
        ///// <summary>
        ///// Update ft client id
        ///// </summary>
        ///// <param name="client"></param>
        //public void UpdateFTClientId(Model.Client client)
        //{
        //    using (var hrLink = new HRLinkDBContext())
        //    {
        //        try
        //        {
        //            var hrClient = hrLink.Clients.FirstOrDefault(x => x.ClientId == client.ClientId);

        //            if (hrClient != null)
        //            {
        //                hrClient.FTClientId = client.FTClientId;
        //                hrLink.SaveChanges();
        //            }                    
        //        }
        //        catch (Exception ex)
        //        {
        //            Console.WriteLine(ex.ToString());
        //            Logger.Error(ex);
        //        }
        //    }
        //}
        /// <summary>
        /// update ft status
        /// </summary>
        /// <param name="client"></param>
        public void UpdateClientFTStatus(Model.Client client)
        {
            using (var hrLink = new HRLinkDBContext())
            {
                try
                {
                    var hrClient = hrLink.Clients.FirstOrDefault(x => x.ClientId == client.ClientId);

                    if (hrClient != null)
                    {
                        hrClient.FTClientId = client.FTClientId;
                        hrClient.FTStatus = client.FTStatus;
                        hrClient.FTErrorDescription = client.FTErrorDescription;
                        hrLink.SaveChanges();
                    }                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Logger.Error($"ClientId: {client.ClientId}", ex);
                }
            }
        }
    }
}