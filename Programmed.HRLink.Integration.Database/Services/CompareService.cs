﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using log4net;
using Programmed.FT360.API.Model.ParentCompany;
using Programmed.HRLink.Integration.Database.Abstract;
using Programmed.HRLink.Integration.Database.Model;
using Programmed.HRLink.Integration.Model.Api;
using Programmed.HRLink.Integration.Model.Api.CostCentre;
using Programmed.HRLink.Integration.Model.Database;
using Client = Programmed.FT360.API.Model.Client.Client;

namespace Programmed.HRLink.Integration.Database.Services
{
    public class CompareService : ICompareService
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CompareService));
        /// <summary>
        /// Compare hrlink member & fast track candidate data for linked records
        /// </summary>
        /// <param name="ftCandidates"></param>
        /// <param name="hrMembers"></param>
        /// <returns></returns>
        public List<string> CompareMembers(IList<Candidate> ftCandidates, List<Member> hrMembers)
        {
            List<string> differences = new List<string>();
            foreach (var hrMember in hrMembers)
            {
                var ftCandidate = ftCandidates.FirstOrDefault(x => x.id == hrMember.FTCandidateId);
                try
                {
                    if (ftCandidate == null)
                        differences.Add($"HRLink MemberId: {hrMember.MemberID} Candidate Id {hrMember.FTCandidateId} does not exist in Fast Track 360");
                    else
                    {
                        //if (ftCandidate.office?.id != hrMember.FTOfficeId)
                        //    differences.Add($"HRLink MemberId: {hrMember.MemberID} Office Code: {hrMember.FTOfficeId} <> Fast Track CandidateId {ftCandidate.id}, Office Code: {ftCandidate.office?.id}");

                        if (ftCandidate.salutation?.name != hrMember.Title)
                            differences.Add($"HRLink MemberId: {hrMember.MemberID} Salutation: {hrMember.Title} <> Fast Track CandidateId {ftCandidate.id}, Salutation: {ftCandidate.salutation?.name}");

                        //if (ftCandidate.ownerUserId != hrMember.FTOwnerId)
                        //    differences.Add($"HRLink MemberId: {hrMember.MemberID}, Owner: {hrMember.FTOwnerId} <> Fast Track CandidateId {ftCandidate.id}, Owner: {ftCandidate.ownerUserId}");

                        string ftSaluation = string.Join(string.Empty, ftCandidate.gender?.name?.ToCharArray().First().ToString() ?? "U");

                        if (ftSaluation != hrMember.Sex)
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, Gender: {hrMember.Sex} <> Fast Track CandidateId {ftCandidate.id}, Sex: {ftCandidate.gender?.name}");

                        if (ftCandidate.firstName != hrMember.GivenName.Trim().Split(' ').FirstOrDefault())
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, First Name: {hrMember.GivenName.Trim().Split(' ').FirstOrDefault()} <> Fast Track CandidateId {ftCandidate.id}, First Name: {ftCandidate.firstName}");

                        if (ftCandidate.surname != hrMember.Surname)
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, Surname: {hrMember.Surname} <> Fast Track CandidateId {ftCandidate.id}, Surname: {ftCandidate.surname}");

                        if (ftCandidate.contactDetails?.emailOne != hrMember.MemberInteractionMethods.FirstOrDefault(x => x.InteractionMethodID == Common.Constants.MemberInteractionMethod.Email)?.SendTo)
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, Email: {hrMember.MemberInteractionMethods.FirstOrDefault(x => x.InteractionMethodID == Common.Constants.MemberInteractionMethod.Email)?.SendTo} <> Fast Track CandidateId {ftCandidate.id}, Email: {ftCandidate.contactDetails?.emailOne}");

                        if (ftCandidate.contactDetails?.mobilePhone != hrMember.MemberInteractionMethods.FirstOrDefault(x => x.InteractionMethodID == Common.Constants.MemberInteractionMethod.Mobile && x.SendTo.StartsWith("(04"))?.SendTo)
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, Mobile Phone: {hrMember.MemberInteractionMethods.FirstOrDefault(x => x.InteractionMethodID == Common.Constants.MemberInteractionMethod.Mobile && x.SendTo.StartsWith("(04"))?.SendTo} <> Fast Track CandidateId {ftCandidate.id}, Mobile Phone: {ftCandidate.contactDetails?.mobilePhone}");

                        if (ftCandidate.contactDetails?.phoneAfterHours != hrMember.MemberInteractionMethods.FirstOrDefault(x => x.InteractionMethodID == Common.Constants.MemberInteractionMethod.Mobile && !x.SendTo.StartsWith("(04"))?.SendTo)
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, Home Phone: {hrMember.MemberInteractionMethods.FirstOrDefault(x => x.InteractionMethodID == Common.Constants.MemberInteractionMethod.Mobile && !x.SendTo.StartsWith("(04"))?.SendTo} <> Fast Track CandidateId {ftCandidate.id}, AH Phone: {ftCandidate.contactDetails?.phoneAfterHours}");

                        if (ftCandidate.dob?.Date != hrMember.BirthDate.Date)
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, Date of Birth: {hrMember.BirthDate.Date} <> Fast Track CandidateId {ftCandidate.id}, Date of Birth: {ftCandidate.dob?.Date}");
                        //if (ftCandidate.skillGroupId != hrMember.FTSkillGroupId)
                        //    differences.Add($"HRLink MemberId: {hrMember.MemberID}, Skill Group: {hrMember.FTSkillGroupId} <> Fast Track CandidateId {ftCandidate.id}, Skill Group: {ftCandidate.skillGroupId}");

                        bool? hrStatus = hrMember.MemberAgencies.FirstOrDefault(x => x.AgencyID == Common.Constants.Agency.Php)?.EnabledStatus;

                        if (ftCandidate.status?.id != Common.GetCandidateStatus(hrStatus))
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, Status: {(hrStatus == true ? "Active" : "InActive")} <> Fast Track CandidateId {ftCandidate.id}, Status: {ftCandidate.status?.name}");

                        if (ftCandidate.mainAddress?.addressLineOne != $"{hrMember.MemberContact?.Street1No} {hrMember.MemberContact?.Street1} {hrMember.MemberContact?.Abbrev1}".Trim())
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, Main Address Street 1: {$"{hrMember.MemberContact?.Street1No} {hrMember.MemberContact?.Street1} {hrMember.MemberContact?.Abbrev1}".Trim()} <> Fast Track CandidateId {ftCandidate.id}, Actual Address Line 1: {ftCandidate.mainAddress?.addressLineOne}");

                        if (ftCandidate.mainAddress?.addressLineTwo != $"{hrMember.MemberContact?.Street2No} {hrMember.MemberContact?.Street2} {hrMember.MemberContact?.Abbrev2}".Trim())
                        {
                            //check if comparing null with empty space
                            if(string.IsNullOrWhiteSpace(ftCandidate.mainAddress?.addressLineTwo) != string.IsNullOrEmpty($"{hrMember.MemberContact?.Street2No} {hrMember.MemberContact?.Street2} {hrMember.MemberContact?.Abbrev2}".Trim()))
                                differences.Add($"HRLink MemberId: {hrMember.MemberID}, Main Address Street 2: {$"{hrMember.MemberContact?.Street2No} {hrMember.MemberContact?.Street2} {hrMember.MemberContact?.Abbrev2}".Trim()} <> Fast Track CandidateId {ftCandidate.id}, Actual Address Line 2: {ftCandidate.mainAddress?.addressLineTwo}");
                        }

                        if (ftCandidate.mainAddress?.postCode != hrMember.MemberContact?.Postcode)
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, Main Address Post Code: {hrMember.MemberContact?.Postcode} <> Fast Track CandidateId {ftCandidate.id}, Actual Address Post Code: {ftCandidate.mainAddress?.postCode}");

                        if (ftCandidate.mainAddress?.suburb != hrMember.MemberContact?.Suburb)
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, Main Address Suburb: {hrMember.MemberContact?.Suburb} <> Fast Track CandidateId {ftCandidate.id}, Actual Address Suburb: {ftCandidate.mainAddress?.suburb}");

                        if (ftCandidate.mainAddress?.state?.name != hrMember.MemberContact?.State.Trim())
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, Main Address State: {hrMember.MemberContact?.State.Trim()} <> Fast Track CandidateId {ftCandidate.id}, Actual Address State: {ftCandidate.mainAddress?.state?.name}");

                        if (ftCandidate.mailingAddress?.addressLineOne != hrMember.MemberContact?.PostalStreet1)
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, Mailing Address Street 1: {hrMember.MemberContact?.PostalStreet1} <> Fast Track CandidateId {ftCandidate.id}, Mailing Address Line 1: {ftCandidate.mailingAddress?.addressLineOne}");

                        if (ftCandidate.mailingAddress?.addressLineTwo != hrMember.MemberContact?.PostalStreet2)
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, Mailing Address Street 2: {hrMember.MemberContact?.PostalStreet2} <> Fast Track CandidateId {ftCandidate.id}, Mailing Address Line 2: {ftCandidate.mailingAddress?.addressLineTwo}");

                        if (ftCandidate.mailingAddress?.postCode != hrMember.MemberContact?.PostalPostcode)
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, Mailing Address Post Code: {hrMember.MemberContact?.PostalPostcode} <> Fast Track CandidateId {ftCandidate.id}, Mailing Address Post Code: {ftCandidate.mailingAddress?.postCode}");

                        if (ftCandidate.mailingAddress?.suburb != hrMember.MemberContact?.PostalSuburb)
                            differences.Add($"HRLink MemberId: {hrMember.MemberID}, Mailing Address Suburb: {hrMember.MemberContact?.PostalSuburb} <> Fast Track CandidateId {ftCandidate.id}, Mailing Address Suburb: {ftCandidate.mailingAddress?.suburb}");

                        if (ftCandidate.mailingAddress?.state?.name != hrMember.MemberContact?.PostalState)
                        {
                            if(string.IsNullOrEmpty(ftCandidate.mailingAddress?.state?.name) != string.IsNullOrEmpty(hrMember.MemberContact?.PostalState.Trim()))//to check null and empty spaces
                                differences.Add($"HRLink MemberId: {hrMember.MemberID}, Mailing Address State: {hrMember.MemberContact?.PostalState} <> Fast Track CandidateId {ftCandidate.id}, Mailing Address State: {ftCandidate.mailingAddress?.state?.name}");
                        }

                        //if (differences.Count > 1)
                        //    break;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                }
            }

            return differences;
        }
       /// <summary>
       /// Compare hrlink client and fast track client data
       /// </summary>
       /// <param name="ftClients"></param>
       /// <param name="hrClients"></param>
       /// <returns></returns>
        public List<string> CompareClients(IList<Client> ftClients, List<Model.Client> hrClients)
        {
            List<string> differences = new List<string>();
            foreach (var hrClient in hrClients)
            {
                var ftClient = ftClients.FirstOrDefault(x => x.id == hrClient.FTClientId);
                try
                {
                    if (ftClient == null)
                        differences.Add($"HRLink ClientId: {hrClient.ClientId} FTClientId {hrClient.FTClientId} does not exist in Fast Track 360");
                    else
                    {
                        if (ftClient.name != hrClient.Name)
                            differences.Add($"HRLink ClientId: {hrClient.ClientId} Name: {hrClient.Name} <> Fast Track ClientId {ftClient.id}, Name: {ftClient.name}");

                        var hrStatusId = Common.GetFTClientStatus(hrClient.EnabledStatus);
                        if(ftClient.status?.id != hrStatusId)
                            differences.Add($"HRLink ClientId: {hrClient.ClientId} Status: {(hrStatusId == Common.Constants.ClientStatus.Active ? "Active" : "InActive")} <> Fast Track ClientId {ftClient.id}, Status: {ftClient.status?.name}");

                        if(ftClient.mainAddress.addressLineOne != hrClient.ClientFinance?.PostalStreet1)
                            differences.Add($"HRLink ClientId: {hrClient.ClientId} Main Address Street 1: {hrClient.ClientFinance?.PostalStreet1} <> Fast Track ClientId {ftClient.id}, Main Address Street 1: {ftClient.mainAddress.addressLineOne}");

                        if (ftClient.mainAddress.addressLineTwo != hrClient.ClientFinance?.PostalStreet2)
                            differences.Add($"HRLink ClientId: {hrClient.ClientId} Main Address Street 2: {hrClient.ClientFinance?.PostalStreet2} <> Fast Track ClientId {ftClient.id}, Main Address Street 2: {ftClient.mainAddress.addressLineTwo}");

                        if (ftClient.mainAddress.suburb != hrClient.ClientFinance?.PostalSuburb)
                            differences.Add($"HRLink ClientId: {hrClient.ClientId} Main Address Suburb: {hrClient.ClientFinance?.PostalSuburb} <> Fast Track ClientId {ftClient.id}, Main Address Street 2: {ftClient.mainAddress.suburb}");

                        if (ftClient.mainAddress.state?.id != Common.GetFTStateId(hrClient.ClientFinance?.PostalState))
                            differences.Add($"HRLink ClientId: {hrClient.ClientId} Main Address State: {hrClient.ClientFinance?.PostalState} <> Fast Track ClientId {ftClient.id}, Main Address State: {ftClient.mainAddress.state?.name}");

                        if (ftClient.mailingAddress.addressLineOne != hrClient.ClientFinance?.PostalStreet1)
                            differences.Add($"HRLink ClientId: {hrClient.ClientId} Mailing Address Street 1: {hrClient.ClientFinance?.PostalStreet1} <> Fast Track ClientId {ftClient.id}, Mailing Address Street 1: {ftClient.mailingAddress.addressLineOne}");

                        if (ftClient.mailingAddress.addressLineTwo != hrClient.ClientFinance?.PostalStreet2)
                            differences.Add($"HRLink ClientId: {hrClient.ClientId} Mailing Address Street 2: {hrClient.ClientFinance?.PostalStreet2} <> Fast Track ClientId {ftClient.id}, Mailing Address Street 2: {ftClient.mailingAddress.addressLineTwo}");

                        if (ftClient.mailingAddress.suburb != hrClient.ClientFinance?.PostalSuburb)
                            differences.Add($"HRLink ClientId: {hrClient.ClientId} Mailing Address Suburb: {hrClient.ClientFinance?.PostalSuburb} <> Fast Track ClientId {ftClient.id}, Mailing Address Street 2: {ftClient.mailingAddress.suburb}");

                        if (ftClient.mailingAddress.state?.id != Common.GetFTStateId(hrClient.ClientFinance?.PostalState))
                            differences.Add($"HRLink ClientId: {hrClient.ClientId} Mailing Address State: {hrClient.ClientFinance?.PostalState} <> Fast Track ClientId {ftClient.id}, Mailing Address State: {ftClient.mailingAddress.state?.name}");
                        //if (ftParentCompany.office?.id != hrClientNetwork.FTOfficeId)
                        //    differences.Add($"HRLink ClientNetworkId: {hrClientNetwork.ClientNetworkID} Office Code: {hrClientNetwork.FTOfficeId} <> Fast Track ParentCompanyId {ftParentCompany.id}, Office Code: {ftParentCompany.office?.id}");

                        //if (ftParentCompany.ownerUserId != hrClientNetwork.FTOwnerId)
                        //    differences.Add($"HRLink ClientNetworkId: {hrClientNetwork.ClientNetworkID}, Owner: {hrClientNetwork.FTOwnerId} <> Fast Track ParentCompanyId {ftParentCompany.id}, Owner: {ftParentCompany.ownerUserId}");

                        //if (ftParentCompany.status?.id != hrClientNetwork.FTStatusId)
                        //    differences.Add($"HRLink ClientNetworkId: {hrClientNetwork.ClientNetworkID}, Status: {(hrClientNetwork.FTStatusId == Common.Constants.FTParentCompanyStatus.Active ? "Active" : "Inactive")} <> Fast Track ParentCompanyId {ftParentCompany.id}, Status: {ftParentCompany.status?.name}");
                        //if (differences.Count > 1)
                        //    break;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                }
            }
            return differences;
        }
        /// <summary>
        /// Compare hrlink network and fast track parent company data
        /// </summary>
        /// <param name="ftParentCompanies"></param>
        /// <param name="hrClientNetworks"></param>
        /// <returns></returns>
        public List<string> CompareClientNetworks(IList<ParentCompany> ftParentCompanies, List<ClientNetwork> hrClientNetworks)
        {
            List<string> differences = new List<string>();
            foreach (var hrClientNetwork in hrClientNetworks)
            {
                var ftParentCompany = ftParentCompanies.FirstOrDefault(x => x.id == hrClientNetwork.FTParentId);
                try
                {
                    if (ftParentCompany == null)
                        differences.Add($"HRLink ClientNetworkId: {hrClientNetwork.ClientNetworkID} FTParentId {hrClientNetwork.FTParentId} does not exist in Fast Track 360");
                    else
                    {
                        if (ftParentCompany.name != hrClientNetwork.Name)
                            differences.Add($"HRLink ClientNetworkId: {hrClientNetwork.ClientNetworkID} Name: {hrClientNetwork.Name} <> Fast Track ParentCompanyId {ftParentCompany.id}, Name: {ftParentCompany.name}");

                        //if (ftParentCompany.office?.id != hrClientNetwork.FTOfficeId)
                        //    differences.Add($"HRLink ClientNetworkId: {hrClientNetwork.ClientNetworkID} Office Code: {hrClientNetwork.FTOfficeId} <> Fast Track ParentCompanyId {ftParentCompany.id}, Office Code: {ftParentCompany.office?.id}");

                        //if (ftParentCompany.ownerUserId != hrClientNetwork.FTOwnerId)
                        //    differences.Add($"HRLink ClientNetworkId: {hrClientNetwork.ClientNetworkID}, Owner: {hrClientNetwork.FTOwnerId} <> Fast Track ParentCompanyId {ftParentCompany.id}, Owner: {ftParentCompany.ownerUserId}");

                        //if (ftParentCompany.status?.id != hrClientNetwork.FTStatusId)
                        //    differences.Add($"HRLink ClientNetworkId: {hrClientNetwork.ClientNetworkID}, Status: {(hrClientNetwork.FTStatusId == Common.Constants.FTParentCompanyStatus.Active ? "Active" : "Inactive")} <> Fast Track ParentCompanyId {ftParentCompany.id}, Status: {ftParentCompany.status?.name}");

                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                }
            }
            return differences;
        }
        /// <summary>
        /// Compares hrlink cost centre data and fast track cost centre data
        /// </summary>
        /// <param name="ftCostCentres"></param>
        /// <param name="hrCostCentres"></param>
        /// <returns></returns>
        public List<string> CompareCostCenters(IList<CostCentre> ftCostCentres, List<WardCostCentreDate> hrCostCentres)
        {
            List<string> differences = new List<string>();
            foreach (var hrCostCentre in hrCostCentres)
            {
                var ftCostCentre = ftCostCentres.FirstOrDefault(x => x.id == hrCostCentre.FTCostCentreId);
                try
                {
                    if (ftCostCentre == null)
                        differences.Add($"HRLink CostCentreId: {hrCostCentre.CostCentreId} FTCostCentreId {hrCostCentre.FTCostCentreId} does not exist in Fast Track 360");
                    else
                    {
                        if (ftCostCentre.name != hrCostCentre.ClientCampusWard?.Name)
                            differences.Add($"HRLink CostCentreId: {hrCostCentre.CostCentreId} Name: {hrCostCentre.ClientCampusWard?.Name} <> Fast Track CostCentreId {ftCostCentre.id}, Name: {ftCostCentre.name}");

                        if (ftCostCentre.code != hrCostCentre.ClientCostCentre.Name)
                            differences.Add($"HRLink CostCentreId: {hrCostCentre.CostCentreId} Code: {hrCostCentre.ClientCostCentre.Name} <> Fast Track CostCentreId {ftCostCentre.id}, Code: {ftCostCentre.code}");

                        if (ftCostCentre.mainAddress.addressLineOne != hrCostCentre.ClientCampusWard?.ClientCampus?.Street1)
                            differences.Add($"HRLink CostCentreId: {hrCostCentre.CostCentreId} Main Address Street 1: {hrCostCentre.ClientCampusWard?.ClientCampus?.Street1} <> Fast Track CostCentreId {ftCostCentre.id}, Main Address Street 1: {ftCostCentre.mainAddress.addressLineOne}");

                        if (ftCostCentre.mainAddress.addressLineTwo != hrCostCentre.ClientCampusWard?.ClientCampus?.Street2)
                            differences.Add($"HRLink CostCentreId: {hrCostCentre.CostCentreId} Main Address Street 2: {hrCostCentre.ClientCampusWard?.ClientCampus?.Street2} <> Fast Track CostCentreId {ftCostCentre.id}, Main Address Street 2: {ftCostCentre.mainAddress.addressLineTwo}");

                        if (ftCostCentre.mainAddress.suburb != hrCostCentre.ClientCampusWard?.ClientCampus?.Suburb)
                            differences.Add($"HRLink CostCentreId: {hrCostCentre.CostCentreId} Main Address Suburb: {hrCostCentre.ClientCampusWard?.ClientCampus?.Suburb} <> Fast Track CostCentreId {ftCostCentre.id}, Main Address Suburb: {ftCostCentre.mainAddress.suburb}");

                        if (ftCostCentre.mainAddress.state?.id != Common.GetFTStateId(hrCostCentre.ClientCampusWard?.ClientCampus?.State))
                            differences.Add($"HRLink CostCentreId: {hrCostCentre.CostCentreId} Main Address State: {hrCostCentre.ClientCampusWard?.ClientCampus?.State} <> Fast Track CostCentreId {ftCostCentre.id}, Main Address State: {ftCostCentre.mainAddress.state?.name}");

                        if (ftCostCentre.mailingAddress.addressLineOne != hrCostCentre.ClientCostCentre.Client.ClientFinance?.PostalStreet1)
                            differences.Add($"HRLink CostCentreId: {hrCostCentre.CostCentreId} Mailing Address Street 1: {hrCostCentre.ClientCostCentre.Client.ClientFinance ?.PostalStreet1} <> Fast Track CostCentreId {ftCostCentre.id}, Mailing Address Street 1: {ftCostCentre.mailingAddress.addressLineOne}");

                        if (ftCostCentre.mailingAddress.addressLineTwo != hrCostCentre.ClientCostCentre.Client.ClientFinance?.PostalStreet2)
                            differences.Add($"HRLink CostCentreId: {hrCostCentre.CostCentreId} Mailing Address Street 2: {hrCostCentre.ClientCostCentre.Client.ClientFinance?.PostalStreet2} <> Fast Track CostCentreId {ftCostCentre.id}, Mailing Address Street 2: {ftCostCentre.mailingAddress.addressLineTwo}");

                        if (ftCostCentre.mailingAddress.suburb != hrCostCentre.ClientCostCentre.Client.ClientFinance?.PostalSuburb)
                            differences.Add($"HRLink CostCentreId: {hrCostCentre.CostCentreId} Mailing Address Suburb: {hrCostCentre.ClientCostCentre.Client.ClientFinance?.PostalSuburb} <> Fast Track CostCentreId {ftCostCentre.id}, Mailing Address Suburb: {ftCostCentre.mailingAddress.suburb}");

                        if (ftCostCentre.mailingAddress.state?.id != Common.GetFTStateId(hrCostCentre.ClientCostCentre.Client.ClientFinance?.PostalState))
                            differences.Add($"HRLink CostCentreId: {hrCostCentre.CostCentreId} Mailing Address State: {hrCostCentre.ClientCostCentre.Client.ClientFinance?.PostalState} <> Fast Track CostCentreId {ftCostCentre.id}, Mailing Address State: {ftCostCentre.mailingAddress.state?.name}");
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                }
            }
            return differences;
        }

        /// <summary>
        /// Send email
        /// </summary>
        /// <param name="smtpServer"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public bool SendEmail(string smtpServer, string from, string to, string subject, string body)
        {
            try
            {
                using (SmtpClient client = new SmtpClient(smtpServer))
                {

                    MailAddress fromAddress = new MailAddress(from);

                    MailAddress toAddress = new MailAddress(to);

                    MailMessage message = new MailMessage(fromAddress, toAddress)
                    {
                        Body = body,
                        BodyEncoding = Encoding.UTF8,
                        Subject = subject,
                        SubjectEncoding = Encoding.UTF8
                    };

                    client.Send(message);
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }
    }
}
