﻿using log4net;
using Programmed.HRLink.Integration.Database.Abstract;
using Programmed.HRLink.Integration.Model.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Programmed.HRLink.Integration.Database.Services
{
    public class CostCentreService : ICostCentreService
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CostCentreService));
        /// <summary>
        /// Get linked cost centres
        /// </summary>
        /// <returns></returns>
        public List<WardCostCentreDate> GetLinkedActiveCostCentres()
        {
            using (var hrLink = new HRLinkDBContext())
            {
                try
                {
                    var hrClients = hrLink.WardCostCentreDates.Include("ClientCostCentre.Client.ClientFinance").Include("ClientCostCentre").Include("ClientCampusWard").Include("ClientCampusWard.ClientCampus").
                        Where(x => x.FTCostCentreId != null && x.ClientCampusWard.EnabledStatus);

                    return hrClients.ToList();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Logger.Error(ex);
                }
            }
            return new List<WardCostCentreDate>();
        }
        /// <summary>
        /// Get cost centres which have changed in hr link
        /// </summary>
        /// <returns></returns>
        public List<WardCostCentreDate> GetCostCentresToBeSent()
        {
            using (var hrLink = new HRLinkDBContext())
            {
                try
                {
                    var hrClients = hrLink.WardCostCentreDates.Include("ClientCostCentre.Client.ClientFinance").Include("ClientCostCentre").Include("ClientCampusWard").Include("ClientCampusWard.ClientCampus").
                        Where(x => x.FTStatus == Common.Constants.FTStatus.ToBeSent && 
                                   //new record with Active Stauts only
                                   (x.FTCostCentreId == null && x.ClientCampusWard.EnabledStatus ||
                                    //existing record with any Status
                                    x.FTCostCentreId != null));

                    return hrClients.ToList();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Logger.Error(ex);
                }
            }
            return new List<WardCostCentreDate>();
        }
        ///// <summary>
        ///// Update ft cost centre id
        ///// </summary>
        ///// <param name="costCentre"></param>
        //public void UpdateFTCostCentreId(WardCostCentreDate costCentre)
        //{
        //    using (var hrLink = new HRLinkDBContext())
        //    {
        //        try
        //        {
        //            var hrCostCentre = hrLink.WardCostCentreDates.FirstOrDefault(x => x.WardId == costCentre.WardId && x.CostCentreId == costCentre.CostCentreId && x.EffectiveStartDate == costCentre.EffectiveStartDate);

        //            if (hrCostCentre != null)
        //            {
        //                hrCostCentre.FTCostCentreId = costCentre.FTCostCentreId;
        //                hrLink.SaveChanges();
        //            }                    
        //        }
        //        catch (Exception ex)
        //        {
        //            Console.WriteLine(ex.ToString());
        //            Logger.Error(ex);
        //        }
        //    }
        //}
        /// <summary>
        /// update ftStatus
        /// </summary>
        /// <param name="costCentre"></param>
        public void UpdateCostCentreFTStatus(WardCostCentreDate costCentre)
        {
            using (var hrLink = new HRLinkDBContext())
            {
                try
                {
                    var hrCostCentre = hrLink.WardCostCentreDates.FirstOrDefault(x => x.WardId == costCentre.WardId && x.CostCentreId == costCentre.CostCentreId && x.EffectiveStartDate == costCentre.EffectiveStartDate);

                    if (hrCostCentre != null)
                    {
                        hrCostCentre.FTCostCentreId = costCentre.FTCostCentreId;
                        hrCostCentre.FTStatus = costCentre.FTStatus;
                        hrCostCentre.FTErrorDescription = costCentre.FTErrorDescription;
                        hrLink.SaveChanges();
                    }                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Logger.Error($"CostCentreId: {costCentre.CostCentreId} EffectiveStartDate: {costCentre.EffectiveStartDate}", ex);
                }
            }
        }
    }
}
