﻿using log4net;
using Programmed.HRLink.Integration.Database.Abstract;
using Programmed.HRLink.Integration.Database.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using Programmed.HRLink.Integration.Model.Api;

namespace Programmed.HRLink.Integration.Database.Services
{
    public class MemberService : IMemberService
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MemberService));
        /// <summary>
        /// Get linked active memebers
        /// </summary>
        /// <returns></returns>
        public List<Member> GetLinkedActiveMembers()
        {
            using (var hrLink = new HRLinkDBContext())
            {
                try
                {
                    int phpAgency = Common.Constants.Agency.Php;
                    var hrMemember = hrLink.Members.Include("MemberContact").Include("MemberInteractionMethods").Include("MemberAgencies").Where(x => x.FTCandidateId != null
                                     && x.MemberAgencies.Any(a => a.AgencyID == phpAgency) && x.MemberAgencies.FirstOrDefault(s => s.AgencyID == phpAgency).EnabledStatus);

                    return hrMemember.ToList();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Logger.Error(ex);
                }
            }
            return new List<Member>();
        }
        /// <summary>
        /// Update mmebers
        /// </summary>
        /// <param name="candidates"></param>
        /// <returns></returns>
        public bool UpdateMembers(Candidates candidates)
        {
            long candidateId = 0;
            string alternateNumberOne = "";

            using (var hrLink = new HRLinkDBContext())
            {
                foreach (var candidate in candidates.data)
                {
                    try
                    {
                        candidateId = candidate.id;
                        alternateNumberOne = candidate.alternateNumberOne;
                        var hrMemember = hrLink.Members.FirstOrDefault(x => x.FTCandidateId == candidate.id);

                        if (hrMemember == null)
                        {
                            #region Add New Member
                            #region Add Member
                            Member newMember = new Member
                            {
                                FTCandidateId = candidate.id,
                                Dear = string.Join(string.Empty, candidate.firstName.Take(20)),
                                FTOfficeId = candidate.office?.id,
                                Sex = string.Join(string.Empty, candidate.gender?.name?.ToCharArray().First().ToString() ?? "U"),
                                GivenName = string.Join(string.Empty, candidate.firstName.Take(30)),
                                Surname = string.Join(string.Empty, candidate.surname.Take(30)),
                                Title = string.Join(string.Empty, (candidate.salutation?.name ?? "U").Take(6)),
                                FTSkillGroupId = candidate.skillGroupId,// skillGroupId,//Common: 41
                                FTOwnerId = candidate.ownerUserId,// ownerUserId,//Generic Owner (Agency User)
                                                    //m.FTStatus = 
                                LocationID = Common.GetHRLinkStateCode(candidate.mainAddress.state.id),
                                BirthDate = candidate.dob ?? new DateTime(1900, 1, 1),
                                FTSourceId = candidate.source?.id,
                                FTStatus = Common.Constants.FTStatus.ToBeSent,//so that we can update alternateNumberOne back in Fast Track
                            };
                            if (newMember.BirthDate.Year < 1900)
                                newMember.BirthDate = new DateTime(1900, 1, 1);
                            hrLink.Members.Add(newMember);
                            #endregion

                            #region Add Email
                            if (!string.IsNullOrEmpty(candidate.contactDetails.emailOne))
                            {
                                MemberInteractionMethod mim = new MemberInteractionMethod
                                {
                                    MemberID = newMember.MemberID,
                                    SendTo = candidate.contactDetails.emailOne,
                                    InteractionMethodID = Common.Constants.MemberInteractionMethod.Email,
                                    ContactOrder = 2
                                };
                                hrLink.MemberInteractionMethods.Add(mim);
                            }
                            #endregion

                            #region Add Mobile
                            if (!string.IsNullOrEmpty(candidate.contactDetails.mobilePhone))
                            {
                                MemberInteractionMethod newMim = new MemberInteractionMethod
                                {
                                    MemberID = newMember.MemberID,
                                    SendTo = candidate.contactDetails.mobilePhone,
                                    ContactOrder = 1,
                                    Description = "Mobile",
                                    InteractionMethodID = Common.Constants.MemberInteractionMethod.Mobile
                                };
                                hrLink.MemberInteractionMethods.Add(newMim);
                            }
                            #endregion

                            #region Add Home Phone
                            if (!string.IsNullOrEmpty(candidate.contactDetails.phoneAfterHours))
                            {
                                MemberInteractionMethod newMim = new MemberInteractionMethod
                                {
                                    MemberID = newMember.MemberID,
                                    SendTo = candidate.contactDetails.phoneAfterHours,
                                    ContactOrder = 1,
                                    Description = "Home",
                                    InteractionMethodID = Common.Constants.MemberInteractionMethod.Mobile
                                };
                                hrLink.MemberInteractionMethods.Add(newMim);
                            }
                            #endregion

                            #region Add Agency
                            MemberAgency newMa = new MemberAgency
                            {
                                MemberID = newMember.MemberID,
                                AgencyID = Common.Constants.Agency.Php,//php
                                EnabledStatus = Common.GeMemberStatus(candidate.status.id),//active-inactive
                                MailingListStatus = true,
                                PrefillStatus = 0,
                                QuarantineStatus = Common.IsFTCandidateQuarantine(candidate.type?.id, candidate.status.id),//quarntine
                                Timestamp = Encoding.Default.GetBytes(DateTime.Now.ToString("dd/MM/yyy HH:mm:ss:fff")).Take(8).ToArray()
                            };
                            hrLink.MemberAgencies.Add(newMa);
                            #endregion

                            #region Add Member Contact
                            MemberContact newMc = new MemberContact
                            {
                                Street1 = string.Join(string.Empty, (candidate.mainAddress.addressLineOne ?? "NA").Take(30)),
                                Street2 = string.Join(string.Empty, (candidate.mainAddress.addressLineTwo ?? "NA").Take(30)),
                                Suburb = string.Join(string.Empty, (candidate.mainAddress.suburb ?? "NA").Take(30)),
                                State = string.Join(string.Empty, (candidate.mainAddress.state?.name ?? "NA").Take(10)),
                                Postcode = string.Join(string.Empty, (candidate.mainAddress.postCode ?? "3000").Take(6)),
                                PostalSuburb = string.Join(string.Empty, (candidate.mailingAddress.suburb ?? "NA").Take(30)),
                                PostalPostcode = string.Join(string.Empty, (candidate.mailingAddress.postCode ?? "3000").Take(6)),
                                PostalStreet1 = string.Join(string.Empty, (candidate.mailingAddress.addressLineOne ?? "NA").Take(30)),
                                PostalStreet2 = string.Join(string.Empty, (candidate.mailingAddress.addressLineTwo ?? "NA").Take(30)),
                                PostalState = string.Join(string.Empty, (candidate.mailingAddress.state?.name ?? "NA").Take(6)),
                                PostalCountry = string.Join(string.Empty, (candidate.mailingAddress.country?.name ?? "NA").Take(30)),
                                DoNotEmailPayslips = false,
                                SendSMSShiftReminder = false,
                                ModifyDate = DateTime.Now,
                                Timestamp = Encoding.Default.GetBytes(DateTime.Now.ToString("dd/MM/yyy HH:mm:ss:fff")).Take(8).ToArray()
                            };
                            newMc.MemberID = newMember.MemberID;
                            hrLink.MemberContacts.Add(newMc);
                            #endregion
                            hrLink.SaveChanges();
                            Console.WriteLine($"Created Member {candidate.surname} in HRLink");
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        var validationErrors = "";
                        if (ex.GetType() == typeof(DbEntityValidationException))
                        {
                            var errorMessages = ((DbEntityValidationException)ex).EntityValidationErrors
                                .SelectMany(x => x.ValidationErrors)
                                .Select(x => x.ErrorMessage);

                            validationErrors = string.Join("; ", errorMessages);
                        }
                        Console.WriteLine(ex.ToString());
                        Logger.Error($"MemberId: {alternateNumberOne} FTCandidateId: {candidateId} {validationErrors}", ex);
                    }
                }
            }
            return true;
        }        

        /// <summary>
        /// Update member Fast Track Status
        /// </summary>
        /// <param name="member"></param>
        public void UpdateMemberFTStatus(Member member)
        {
            using (var hrLink = new HRLinkDBContext())
            {
                try
                {
                    var hrMemember = hrLink.Members.FirstOrDefault(x => x.MemberID == member.MemberID);

                    if (hrMemember != null)
                    {
                        hrMemember.FTCandidateId = member.FTCandidateId;
                        hrMemember.FTStatus = member.FTStatus;
                        hrMemember.FTErrorDescription = member.FTErrorDescription;
                        hrLink.SaveChanges();
                    }                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Logger.Error($"MemberId: {member.MemberID}", ex);
                }
            }            
        }
        ///// <summary>
        ///// Update member candidate id
        ///// </summary>
        ///// <param name="member"></param>
        //public void UpdateMemberCandidateId(Member member)
        //{
        //    using (var hrLink = new HRLinkDBContext())
        //    {
        //        try
        //        {
        //            var hrMemember = hrLink.Members.FirstOrDefault(x => x.MemberID == member.MemberID);

        //            if (hrMemember != null)
        //            {
        //                hrMemember.FTCandidateId = member.FTCandidateId;
        //                hrLink.SaveChanges();
        //            }                    
        //        }
        //        catch (Exception ex)
        //        {
        //            Console.WriteLine(ex.ToString());
        //            Logger.Error(ex);
        //        }
        //    }
        //}
        /// <summary>
        /// Get memebers which has changed
        /// </summary>
        /// <returns></returns>
        public List<Member> GetMembersToBeSent()
        {
            using (var hrLink = new HRLinkDBContext())
            {
                try
                {
                    int phpAgency = Common.Constants.Agency.Php;

                    var hrMemember = hrLink.Members.Include("MemberContact").Include("MemberInteractionMethods").Include("MemberAgencies").
                        Where(x => x.FTStatus == Common.Constants.FTStatus.ToBeSent && 
                                   //new record with Active Status only
                                   (x.FTCandidateId == null && x.MemberAgencies.Any(a => a.AgencyID == phpAgency && a.EnabledStatus) || 
                                    //existing record with any Status
                                    x.FTCandidateId != null)).
                        OrderBy(x => x.MemberID);

                    return hrMemember.ToList();
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Logger.Error(ex);
                }
            }
            return new List<Member>();
        }       
    }
}