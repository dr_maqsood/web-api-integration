﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using Programmed.HRLink.Integration.Database.Abstract;
using Programmed.HRLink.Integration.Model.Database;

namespace Programmed.HRLink.Integration.Database.Services
{
    public class ShiftService : IShiftService
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ShiftService));
        /// <summary>
        /// Get unpaid shifts
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        public List<LockedShift> GetUnpaidShifts(int? locationId)
        {
            using (var hrLink = new HRLinkDBContext())
            {

                var locationIdParam = new SqlParameter
                {
                    ParameterName = "LocationID",
                    Value = locationId
                };
                var timesheet = hrLink.Database.SqlQuery<LockedShift>("exec php_rsLockedShifts @LocationID ", locationIdParam).ToList();
                return timesheet;
            }
        }
        /// <summary>
        /// Get locked and not exported shifts
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        public List<Shift> GetLockedShifts(int locationId)
        {
            using (var hrLink = new HRLinkDBContext())
            {
                var shifts = hrLink.Shifts.Where(x => string.IsNullOrEmpty(x.PinkslipID) && (x.FTExported == null || x.FTExported == false)).ToList();
                return shifts;
            }
        }

        /// <summary>
        /// Update Shift Job Id
        /// </summary>
        /// <param name="shiftId"></param>
        /// <param name="jobId"></param>
        /// <returns></returns>
        public bool UpdateJobId(int shiftId, long jobId)
        {
            try
            {
                using (var hrLink = new HRLinkDBContext())
                {
                    var shift = hrLink.Shifts.FirstOrDefault(x => x.ShiftID == shiftId);
                    if (shift != null)
                    {
                        shift.FTJobId = jobId;
                        hrLink.SaveChanges();
                    }
                    else
                    {
                        Logger.Error($"ShiftId: {shiftId} nout found in HRLink");
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }
    }
}