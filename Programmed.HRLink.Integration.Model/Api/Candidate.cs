﻿using System;
using System.Collections.Generic;

namespace Programmed.HRLink.Integration.Model.Api
{
    #region Candidate Structure

    public class Candidate
    {
        public long id { get; set; }
        public Office office { get; set; }
        public long ownerUserId { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string surname { get; set; }
        public Salutation salutation { get; set; }
        public object preferredOrPreviousName { get; set; }
        public Status status { get; set; }
        public InactiveReason inactiveReason { get; set; }
        public double grade { get; set; }
        public Type type { get; set; }
        public int skillGroupId { get; set; }
        public int positionId { get; set; }
        public ContactDetails contactDetails { get; set; }
        public CandidatePreferredContactMethod candidatePreferredContactMethod { get; set; }
        public MainAddress mainAddress { get; set; }
        public MailingAddress mailingAddress { get; set; }
        public string alternateNumberOne { get; set; }
        public object alternateNumberTwo { get; set; }
        public object migrationId { get; set; }
        public DiaryType diaryType { get; set; }
        public object availableDate { get; set; }
        public object preferredWorkHours { get; set; }
        public string noticePeriodRequired { get; set; }
        public int noticeWeeks { get; set; }
        public object notes { get; set; }
        public bool? carAvailable { get; set; }
        public bool? driversLicence { get; set; }
        public int residencyStatusId { get; set; }
        public object visaNumber { get; set; }
        public object visaType { get; set; }
        public object expiryDate { get; set; }
        public object contractVisaDetails { get; set; }
        public object placeOfBirth { get; set; }
        public object nationality { get; set; }
        public object immigrationDate { get; set; }
        public OptInDetails optInDetails { get; set; }
        public bool? isPrivacyStatementAccepted { get; set; }
        public object privacyStatementAcceptedDate { get; set; }
        public object privacyStatementAcceptedTime { get; set; }
        public object privacyStatementAcceptedDateUtc { get; set; }
        public object privacyStatementAcceptedTimeUtc { get; set; }
        public object interviewDate { get; set; }
        public object interviewNotes { get; set; }
        public bool? verified { get; set; }
        public int? verifiedByUserId { get; set; }
        public object registrationDate { get; set; }
        public object registrationTime { get; set; }
        public object registrationDateUtc { get; set; }
        public object registrationTimeUtc { get; set; }
        public DateTime? dob { get; set; }
        public Gender gender { get; set; }
        public MaritalStatus maritalStatus { get; set; }
        public Source source { get; set; }
        public decimal salary { get; set; }
        public object salaryPackage { get; set; }
        public decimal rate { get; set; }
        public object incomeComment { get; set; }
        public object socialSecurityNumber { get; set; }
        public object emergencyName { get; set; }
        public object relationshipTocandidate { get; set; }
        public object emergencyPhone { get; set; }
        public int speech { get; set; }
        public int appearance { get; set; }
        public int writing { get; set; }
        public int attitude { get; set; }
        public int understanding { get; set; }
        public object profile { get; set; }
        public int height { get; set; }
        public int weight { get; set; }
        public object specialNotes { get; set; }
        public Industry industry { get; set; }
        public int portalUserId { get; set; }
        public object mailCodes { get; set; }
        public IList<object> candidateSkill { get; set; }
        public IList<object> candidateWorkReference { get; set; }
        public CandidateAvailabilityPreferenceOne candidateAvailabilityPreferenceOne { get; set; }
        public CandidateAvailabilityPreferenceTwo candidateAvailabilityPreferenceTwo { get; set; }
        public object willingToTravel { get; set; }
        public bool isExpiryDateInRange { get; set; }
        public AuditDetails auditDetails { get; set; }
    }

    public class Candidates
    {
        public IList<Candidate> data { get; set; }
    }
    public class CandidateOne
    {
        public Candidate data { get; set; }
    }
    public class State
        {
            public int? id { get; set; }
            public string name { get; set; }
        }

    public class Country
    {
        public int? id { get; set; }
        public string name { get; set; }
        public State state { get; set; }
    }

    public class Office
    {
        public int id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public int brandId { get; set; }
        public string brandName { get; set; }
        public int regionId { get; set; }
        public string region { get; set; }
        public Country country { get; set; }
        public object accountSegment { get; set; }
        public object importCode { get; set; }
        public object exportCode { get; set; }
        public bool status { get; set; }
    }

    public class Salutation
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class Status
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class InactiveReason
    {
        public object id { get; set; }
        public object name { get; set; }
        public bool status { get; set; }
    }

    public class Type
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class ContactDetails
    {
        public string emailOne { get; set; }
        public string emailTwo { get; set; }
        public string fax { get; set; }
        public string webSite { get; set; }
        public string phoneBusinessHours { get; set; }
        public string phoneAfterHours { get; set; }
        public string mobilePhone { get; set; }
        public string facebookURL { get; set; }
        public string linkedinURL { get; set; }
        public string twitterURL { get; set; }
    }

    public class MainAddress
    {
        public string addressLineOne { get; set; }
        public string addressLineTwo { get; set; }
        public string suburb { get; set; }
        public string city { get; set; }

        public State state { get; set; }
        public Country country { get; set; }
        public string postCode { get; set; }
    }

    public class CandidatePreferredContactMethod
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }



    public class MailingAddress
    {
        public string addressLineOne { get; set; }
        public string addressLineTwo { get; set; }
        public string suburb { get; set; }
        public string city { get; set; }
        public State state { get; set; }
        public Country country { get; set; }
        public string postCode { get; set; }
    }

    public class DiaryType
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class OptInDetails
    {
        public bool? optInStatus { get; set; }
        public bool? optInEmail { get; set; }
        public bool? optInSms { get; set; }
        public bool? optInMail { get; set; }
    }

    public class Gender
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class MaritalStatus
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class Source
    {
        public int? id { get; set; }
        public object name { get; set; }
        public bool status { get; set; }
    }

    public class Industry
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class CandidateAvailabilityPreferenceOne
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class CandidateAvailabilityPreferenceTwo
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class AuditDetails
    {
        public int createdByUserId { get; set; }
        public string createdDate { get; set; }
        public string createdDateUtc { get; set; }
        public int? lastUpdatedByUserId { get; set; }
        public string lastUpdatedDate { get; set; }
        public string lastUpdatedDateUtc { get; set; }
    }

       
    #endregion
}