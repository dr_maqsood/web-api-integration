﻿//using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;
//using Newtonsoft.Json;

namespace Programmed.HRLink.Integration.Model.Api
{
    public class CandidateAddEdit
    {
        public int? officeId { get; set; }
        public long ownerUserId { get; set; }
        public string firstName { get; set; }
        //[JsonIgnore]
        //public string middleName { get; set; }
        public string surname { get; set; }
        public int? salutationId { get; set; }
        //[JsonIgnore]
        //public string preferredOrPreviousName { get; set; }
        public int statusId { get; set; }
        public int inactiveReasonId { get; set; }
        //[JsonIgnore]
        //public double grade { get; set; }
        public int typeId { get; set; }
        public int skillGroupId { get; set; }
        //[JsonIgnore]
        //public int positionId { get; set; }
        public ContactDetails contactDetails { get; set; }
        //[JsonIgnore]
        //public int candidatePreferredContactMethodId { get; set; }
        public MainAddress mainAddress { get; set; }
        public MainAddress mailingAddress { get; set; }
        public string alternateNumberOne { get; set; }
        public int genderId { get; set; }
        //[JsonIgnore]
        public int sourceId { get; set; }
        //[JsonIgnore]
        [JsonConverter(typeof(DateFormatConverter), "yyyy-MM-dd")]
        public DateTime dob { get; set; }
        //[JsonIgnore]
        //public string alternateNumberTwo { get; set; }
        //[JsonIgnore]
        //public string migrationId { get; set; }
        //[JsonIgnore]
        //public int diaryTypeId { get; set; }
        //[JsonIgnore]
        //public string availableDate { get; set; }
        //[JsonIgnore]
        //public string preferredWorkHours { get; set; }
        //[JsonIgnore]
        //public bool noticePeriodRequired { get; set; }
        //[JsonIgnore]
        //public int noticeWeeks { get; set; }
        //[JsonIgnore]
        //public string notes { get; set; }
        //[JsonIgnore]
        //public bool carAvailable { get; set; }
        //[JsonIgnore]
        //public bool driversLicence { get; set; }
        //[JsonIgnore]
        //public int? residencyStatusId { get; set; }
        //[JsonIgnore]
        //public string visaNumber { get; set; }
        //[JsonIgnore]
        //public string visaType { get; set; }
        //[JsonIgnore]
        //public string expiryDate { get; set; }
        //[JsonIgnore]
        //public string contractVisaDetails { get; set; }
        //[JsonIgnore]
        //public string placeOfBirth { get; set; }
        //[JsonIgnore]
        //public string nationality { get; set; }
        //[JsonIgnore]
        //public string immigrationDate { get; set; }
        //[JsonIgnore]
        //public OptInDetails optInDetails { get; set; }
        //[JsonIgnore]
        //public bool isPrivacyStatementAccepted { get; set; }
        //[JsonIgnore]
        //public string privacyStatementAcceptedDate { get; set; }
        //[JsonIgnore]
        //public string privacyStatementAcceptedTime { get; set; }
        //[JsonIgnore]
        //public string interviewDate { get; set; }
        //[JsonIgnore]
        //public string interviewNotes { get; set; }
        //[JsonIgnore]
        //public bool verified { get; set; }
        //[JsonIgnore]
        //public int verifiedByUserId { get; set; }
        //[JsonIgnore]
        //public string registrationDate { get; set; }
        //[JsonIgnore]
        //public string registrationTime { get; set; }
        //[JsonIgnore]
        //public string dob { get; set; }
        //[JsonIgnore]
        //public int maritalStatusId { get; set; }        
        //[JsonIgnore]
        //public double salary { get; set; }
        //[JsonIgnore]
        //public string salaryPackage { get; set; }
        //[JsonIgnore]
        //public double rate { get; set; }
        //[JsonIgnore]
        //public string incomeComment { get; set; }
        //[JsonIgnore]
        //public string socialSecurityNumber { get; set; }
        //[JsonIgnore]
        //public string emergencyName { get; set; }
        //[JsonIgnore]
        //public string relationshipTocandidate { get; set; }
        //[JsonIgnore]
        //public string emergencyPhone { get; set; }
        //[JsonIgnore]
        //public int speech { get; set; }
        //[JsonIgnore]
        //public int appearance { get; set; }
        //[JsonIgnore]
        //public int writing { get; set; }
        //[JsonIgnore]
        //public int attitude { get; set; }
        //[JsonIgnore]
        //public int understanding { get; set; }
        //[JsonIgnore]
        //public string profile { get; set; }
        //[JsonIgnore]
        //public int height { get; set; }
        //[JsonIgnore]
        //public int weight { get; set; }
        //[JsonIgnore]
        //public string specialNotes { get; set; }
        //[JsonIgnore]
        //public int industryId { get; set; }
        //[JsonIgnore]
        //public IList<int> candidateSkill { get; set; }
        //[JsonIgnore]
        //public IList<int> candidateWorkReference { get; set; }
        //[JsonIgnore]
        //public int candidateAvailabilityPreferenceOneId { get; set; }
        //[JsonIgnore]
        //public int candidateAvailabilityPreferenceTwoId { get; set; }
        //[JsonIgnore]
        //public IList<int> willingToTravel { get; set; }
        //[JsonIgnore]
        //public IList<int> mailCodeId { get; set; }

        public class MainAddress
        {
            public string addressLineOne { get; set; }
            public string addressLineTwo { get; set; }
            public string suburb { get; set; }
            public string city { get; set; }
            public int stateId { get; set; }
            public int countryId { get; set; }
            public string postCode { get; set; }
        }

        //public class MailingAddress
        //{
        //    public string addressLineOne { get; set; }
        //    public string addressLineTwo { get; set; }
        //    public string suburb { get; set; }
        //    public string city { get; set; }
        //    public int stateId { get; set; }
        //    public int countryId { get; set; }
        //    public string postCode { get; set; }
        //}

        //public class OptInDetails
        //{
        //    public bool optInStatus { get; set; }
        //    public bool optInEmail { get; set; }
        //    public bool optInSms { get; set; }
        //    public bool optInMail { get; set; }
        //}
    }
}