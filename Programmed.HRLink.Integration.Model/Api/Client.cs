﻿using System.Collections.Generic;

namespace Programmed.FT360.API.Model.Client
{
    public class Client
    {
        public long id { get; set; }
        public Office office { get; set; }
        public long? parentId { get; set; }
        public string name { get; set; }
        public TypeId typeId { get; set; }
        public Industry industry { get; set; }
        public string departmentName { get; set; }
        public Status status { get; set; }
        public InactiveReason inactiveReason { get; set; }
        public int ownerUserId { get; set; }
        public MainAddress mainAddress { get; set; }
        public MailingAddress mailingAddress { get; set; }
        public Source source { get; set; }
        public double grade { get; set; }
        public string migrationId { get; set; }
        public string inspectionDate { get; set; }
        public string profile { get; set; }
        public string registrationDate { get; set; }
        public string deregistrationDate { get; set; }
        public PayrollTaxState payrollTaxState { get; set; }
        public string alternateNumber { get; set; }
        public bool? enforceProjectCodeEntry { get; set; }
        public bool? enforceProjectCodeSelection { get; set; }
        public bool? tradeRefChecked { get; set; }
        public bool? creditChecked { get; set; }
        public bool? termsConditions { get; set; }
        public string healthSafetyDetails { get; set; }
        public int? totalEmployees { get; set; }
        public int? totalWhiteCollarEmployees { get; set; }
        public int? totalBlueCollarEmployees { get; set; }
        public double annualSales { get; set; }
        public double capitalization { get; set; }
        public string specialnotes { get; set; }
        public string corporateIdentityNumber { get; set; }
        public WeekendDay weekendDay { get; set; }
        public TimesheetFrequency timesheetFrequency { get; set; }
        public int? bimonthlyPeriod1StartDay { get; set; }
        public int? bimonthlyPeriod2StartDay { get; set; }
        public string cycleStartDate { get; set; }
        public bool? timesheetSplit { get; set; }
        public bool? timesheetCodeRequired { get; set; }
        public CodeDuplicateCheck codeDuplicateCheck { get; set; }
        public WorkCoverState workCoverState { get; set; }
        public WorkCoverCode workCoverCode { get; set; }
        public string payrollTaxExempt { get; set; }
        public string discription { get; set; }
        public string clothingDressRequirements { get; set; }
        public string travelInstructions { get; set; }
        public string mealAmenities { get; set; }
        public ContactDetails contactDetails { get; set; }
        public AuditDetails auditDetails { get; set; }
    }
    public class State
    {
        public int? id { get; set; }
        public string name { get; set; }
    }

    public class Country
    {
        public int? id { get; set; }
        public string name { get; set; }
        public State state { get; set; }
    }

    public class Office
    {
        public int id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public int brandId { get; set; }
        public string brandName { get; set; }
        public int regionId { get; set; }
        public string region { get; set; }
        public Country country { get; set; }
        public string accountSegment { get; set; }
        public string importCode { get; set; }
        public string exportCode { get; set; }
        public string status { get; set; }
    }

    public class TypeId
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class Industry
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class Status
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class InactiveReason
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class MainAddress
    {
        public string addressLineOne { get; set; }
        public string addressLineTwo { get; set; }
        public string suburb { get; set; }
        public string city { get; set; }
        public State state { get; set; }
        public Country country { get; set; }
        public string postCode { get; set; }
    }

    public class MailingAddress
    {
        public string addressLineOne { get; set; }
        public string addressLineTwo { get; set; }
        public string suburb { get; set; }
        public string city { get; set; }
        public State state { get; set; }
        public Country country { get; set; }
        public string postCode { get; set; }
    }

    public class Source
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class PayrollTaxState
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class WeekendDay
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class TimesheetFrequency
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class CodeDuplicateCheck
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class WorkCoverState
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class WorkCoverCode
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class ContactDetails
    {
        public string emailOne { get; set; }
        public string emailTwo { get; set; }
        public string fax { get; set; }
        public string webSite { get; set; }
        public string mobilePhone { get; set; }
        public string phoneBusinessHours { get; set; }
        public string phoneAfterHours { get; set; }
        public string facebookURL { get; set; }
        public string linkedinURL { get; set; }
        public string twitterURL { get; set; }
    }

    public class AuditDetails
    {
        public int createdByUserId { get; set; }
        public string createdDate { get; set; }
        public string createdDateUtc { get; set; }
        public int lastUpdatedByUserId { get; set; }
        public string lastUpdatedDate { get; set; }
        public string lastUpdatedDateUtc { get; set; }
    }    

    public class Clients
    {
        public IList<Client> data { get; set; }
    }

    public class ClientOne
    {
        public Client client { get; set; }
    }
}