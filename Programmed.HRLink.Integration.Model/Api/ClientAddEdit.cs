﻿namespace Programmed.FT360.API.Model.Client
{
    public class ClientAddEdit
    {
        public int officeId { get; set; }
        public long? parentId { get; set; }
        public string name { get; set; }
        //public int? typeId { get; set; }
        //public int? industryId { get; set; }
        //public string departmentName { get; set; }
        public int statusId { get; set; }
        public int? inactiveReasonId { get; set; }
        public long ownerUserId { get; set; }
        public MainAddress mainAddress { get; set; }
        public MailingAddress mailingAddress { get; set; }
        //public int? sourceId { get; set; }
        //public double grade { get; set; }
        //public string migrationId { get; set; }
        //public string inspectionDate { get; set; }
        //public string profile { get; set; }
        //public string registrationDate { get; set; }
        //public string deregistrationDate { get; set; }
        public int? payrollTaxStateId { get; set; }
        public string alternateNumber { get; set; }
        //public bool enforceProjectCodeEntry { get; set; }
        //public bool enforceProjectCodeSelection { get; set; }
        public bool tradeRefChecked { get; set; }
        //public bool creditChecked { get; set; }
        public bool termsConditions { get; set; }
        //public string healthSafetyDetails { get; set; }
        //public int totalEmployees { get; set; }
        //public int totalWhiteCollarEmployees { get; set; }
        //public int totalBlueCollarEmployees { get; set; }
        //public double annualSales { get; set; }
        //public double capitalization { get; set; }
        //public string specialnotes { get; set; }
        //public string corporateIdentityNumber { get; set; }
        //public int? weekendDayId { get; set; }
        //public int? timesheetFrequencyId { get; set; }
        //public int bimonthlyPeriod1StartDay { get; set; }
        //public int bimonthlyPeriod2StartDay { get; set; }
        //public string cycleStartDate { get; set; }
        //public bool timesheetSplit { get; set; }
        //public bool timesheetCodeRequired { get; set; }
        //public int? codeDuplicateCheckId { get; set; }
        public int? workCoverStateId { get; set; }
        public int? workCoverCodeId { get; set; }
        //public string payrollTaxExempt { get; set; }
        //public string discription { get; set; }
        //public string clothingDressRequirements { get; set; }
        //public string travelInstructions { get; set; }
        //public string mealAmenities { get; set; }
        //public ContactDetails contactDetails { get; set; }

        public class MainAddress
        {
            public string addressLineOne { get; set; }
            public string addressLineTwo { get; set; }
            public string suburb { get; set; }
            public string city { get; set; }
            public int stateId { get; set; }
            public int countryId { get; set; }
            public string postCode { get; set; }
        }
        public class MailingAddress
        {
            public string addressLineOne { get; set; }
            public string addressLineTwo { get; set; }
            public string suburb { get; set; }
            public string city { get; set; }
            public int stateId { get; set; }
            public int countryId { get; set; }
            public string postCode { get; set; }
        }

        //public class ContactDetails
        //{
        //    public string emailOne { get; set; }
        //    public string emailTwo { get; set; }
        //    public string fax { get; set; }
        //    public string webSite { get; set; }
        //    public string mobilePhone { get; set; }
        //    public string phoneBusinessHours { get; set; }
        //    public string phoneAfterHours { get; set; }
        //    public string facebookURL { get; set; }
        //    public string linkedinURL { get; set; }
        //    public string twitterURL { get; set; }
        //}
    }
}
