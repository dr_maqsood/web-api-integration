﻿using System.Collections.Generic;

namespace Programmed.FT360.API.Model.ClientContact
{
    public class ClientContact
    {
        public long id { get; set; }
        public string firstName { get; set; }
        public string preferredFirstName { get; set; }
        public string surname { get; set; }
        public int clientId { get; set; }
        public Status status { get; set; }
        public InactiveReason inactiveReason { get; set; }
        public Salutation salutation { get; set; }
        public string contactTitle { get; set; }
        public int ownerUserId { get; set; }
        public ContactType contactType { get; set; }
        public PreferredContactMethod preferredContactMethod { get; set; }
        public AuthorisedPositionType authorisedPositionType { get; set; }
        public string migrationId { get; set; }
        public string alternateNumber { get; set; }
        public ContactDetails contactDetails { get; set; }
        public int? candidateId { get; set; }
        public MainAddress mainAddress { get; set; }
        public string dobDay { get; set; }
        public string dobMonth { get; set; }
        public OptInDetails optInDetails { get; set; }
        public MailCodes mailCodes { get; set; }
        public int? authorizedSkillsGroupId { get; set; }
        public bool acceptReferrals { get; set; }
        public AuditDetails auditDetails { get; set; }
    }
    public class Status
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class InactiveReason
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class Salutation
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class ContactType
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class PreferredContactMethod
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class AuthorisedPositionType
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class ContactDetails
    {
        public string emailOne { get; set; }
        public string emailTwo { get; set; }
        public string fax { get; set; }
        public string webSite { get; set; }
        public string mobilePhone { get; set; }
        public string phoneAfterHours { get; set; }
        public string facebookURL { get; set; }
        public string linkedinURL { get; set; }
        public string twitterURL { get; set; }
    }

    public class State
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class Country
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class MainAddress
    {
        public string addressLineOne { get; set; }
        public string addressLineTwo { get; set; }
        public string suburb { get; set; }
        public string city { get; set; }
        public State state { get; set; }
        public Country country { get; set; }
        public string postCode { get; set; }
    }

    public class OptInDetails
    {
        public bool optInStatus { get; set; }
        public bool optInEmail { get; set; }
        public bool optInSms { get; set; }
        public bool optInMail { get; set; }
    }

    public class MailCodes
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class AuditDetails
    {
        public int? createdByUserId { get; set; }
        public string createdDate { get; set; }
        public string createdDateUtc { get; set; }
        public int? lastUpdatedByUserId { get; set; }
        public string lastUpdatedDate { get; set; }
        public string lastUpdatedDateUtc { get; set; }
    }

    public class ClientContacts
    {
        public IList<ClientContact> data { get; set; }
    }
    public class ClientContactOne
    {
        public ClientContact data { get; set; }
    }
}
