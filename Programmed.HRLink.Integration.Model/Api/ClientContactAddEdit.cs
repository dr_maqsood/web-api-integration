﻿namespace Programmed.HRLink.Integration.Model.ClientContact
{
    public class ClientContactAddEdit
    {
        public string firstName { get; set; }
        public string preferredFirstName { get; set; }
        public string surname { get; set; }
        public int clientId { get; set; }
        public int statusId { get; set; }
        public int? inactiveReasonId { get; set; }
        public string salutation { get; set; }
        public string contactTitle { get; set; }
        public int ownerUserId { get; set; }
        public int? contactTypeId { get; set; }
        public int? preferredContactMethodId { get; set; }
        public int? authorisedPositionTypeId { get; set; }
        public string migrationId { get; set; }
        public string alternateNumber { get; set; }
        public ContactDetails contactDetails { get; set; }
        public int candidateId { get; set; }
        public MainAddress mainAddress { get; set; }
        public string dobDay { get; set; }
        public string dobMonth { get; set; }
        public OptInDetails optInDetails { get; set; }
        public int mailCodesId { get; set; }
        public int? authorizedSkillsGroupId { get; set; }
        public bool acceptReferrals { get; set; }
    }

    public class ContactDetails
    {
        public string emailOne { get; set; }
        public string emailTwo { get; set; }
        public string fax { get; set; }
        public string webSite { get; set; }
        public string mobilePhone { get; set; }
        public string phoneAfterHours { get; set; }
        public string facebookURL { get; set; }
        public string linkedinURL { get; set; }
        public string twitterURL { get; set; }
    }

    public class MainAddress
    {
        public string addressLineOne { get; set; }
        public string addressLineTwo { get; set; }
        public string suburb { get; set; }
        public string city { get; set; }
        public int stateId { get; set; }
        public int countryId { get; set; }
        public string postCode { get; set; }
    }

    public class OptInDetails
    {
        public bool optInStatus { get; set; }
        public bool optInEmail { get; set; }
        public bool optInSms { get; set; }
        public bool optInMail { get; set; }
    }
}
