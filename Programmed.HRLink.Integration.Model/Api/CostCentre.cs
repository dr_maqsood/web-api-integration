﻿using System.Collections.Generic;

namespace Programmed.HRLink.Integration.Model.Api.CostCentre
{
    public class CostCentre
    {
        public long id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public TypeId typeId { get; set; }
        public Status status { get; set; }
        public InactiveReason inactiveReason { get; set; }
        public long ownerUserId { get; set; }
        public Office office { get; set; }
        public long clientid { get; set; }
        public string registrationDate { get; set; }
        public string registrationTime { get; set; }
        public string registrationDateUtc { get; set; }
        public string registrationTimeUtc { get; set; }
        public string deregistrationDate { get; set; }
        public string deregistrationTime { get; set; }
        public string deregistrationDateUtc { get; set; }
        public string deregistrationTimeUtc { get; set; }
        public string migrationId { get; set; }
        public string alternateNumber { get; set; }
        public bool? tradeReferenceChecked { get; set; }
        public bool? creditChecked { get; set; }
        public bool? termsConditions { get; set; }
        public string corporateIdentityNumber { get; set; }
        public MainAddress mainAddress { get; set; }
        public MainAddress mailingAddress { get; set; }
        public string profile { get; set; }
        public AuditDetails auditDetails { get; set; }
    }

    public class CostCentres
    {
        public IList<CostCentre> data { get; set; }
    }
    public class CostCentreOne
    {
        public CostCentre data { get; set; }
    }
    public class TypeId
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class Status
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class InactiveReason
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class State
    {
        public int? id { get; set; }
        public string name { get; set; }
    }

    public class Country
    {
        public int? id { get; set; }
        public string name { get; set; }
        public State state { get; set; }
    }

    public class Office
    {
        public int id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public int brandId { get; set; }
        public string brandName { get; set; }
        public int regionId { get; set; }
        public string region { get; set; }
        public Country country { get; set; }
        public string accountSegment { get; set; }
        public string importCode { get; set; }
        public string exportCode { get; set; }
        public string status { get; set; }
    }

    public class MainAddress
    {
        public string addressLineOne { get; set; }
        public string addressLineTwo { get; set; }
        public string suburb { get; set; }
        public string city { get; set; }
        public State state { get; set; }
        public Country country { get; set; }
        public string postCode { get; set; }
    }

    public class AuditDetails
    {
        public int? createdByUserId { get; set; }
        public string createdDate { get; set; }
        public string createdDateUtc { get; set; }
        public int? lastUpdatedByUserId { get; set; }
        public string lastUpdatedDate { get; set; }
        public string lastUpdatedDateUtc { get; set; }
    }
}
