﻿namespace Programmed.HRLink.Integration.Model.Api
{
    public class CostCentreAddEdit
    {
        public string name { get; set; }
        public string code { get; set; }
        //public int? typeId { get; set; }
        public int statusId { get; set; }
        public int? inactiveReasonId { get; set; }
        public long ownerUserId { get; set; }
        public int officeId { get; set; }
        public long clientid { get; set; }
        //public string registrationDate { get; set; }
        //public string deregistrationDate { get; set; }
        //public string migrationId { get; set; }
        public string alternateNumber { get; set; }
        //public bool tradeReferenceChecked { get; set; }
        //public bool creditChecked { get; set; }
        //public bool termsConditions { get; set; }
        //public string corporateIdentityNumber { get; set; }
        public MainAddress mainAddress { get; set; }
        public MainAddress mailingAddress { get; set; }
        //public int? clientContactId { get; set; }
        //public string profile { get; set; }

        public class MainAddress
        {
            public string addressLineOne { get; set; }
            public string addressLineTwo { get; set; }
            public string suburb { get; set; }
            public string city { get; set; }
            public int stateId { get; set; }
            public int countryId { get; set; }
            public string postCode { get; set; }
        }
    }    
}
