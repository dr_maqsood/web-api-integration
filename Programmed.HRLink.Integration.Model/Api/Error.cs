﻿using System.Collections.Generic;

namespace Programmed.HRLink.Integration.Model.Api
{
    public class ErrorDetail
    {
        public string statusCode { get; set; }
        public string errorCode { get; set; }
        public List<string> errorUserMessage { get; set; }
        public string errorDeveloperMessage { get; set; }
        public string docsUrl { get; set; }
    }

    public class Error
    {
        public ErrorDetail error { get; set; }
    }
}
