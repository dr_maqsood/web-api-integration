﻿using System.Collections.Generic;

namespace Programmed.FT360.API.Model.ParentCompany
{
    public class ParentCompany
    {
        public long id { get; set; }
        public string name { get; set; }
        public Type type { get; set; }
        public Status status { get; set; }
        public string migrationId { get; set; }
        public Industry industry { get; set; }
        public int ownerUserId { get; set; }
        public MainAddress mainAddress { get; set; }
        public MailingAddress mailingAddress { get; set; }
        public string profile { get; set; }
        public string notes { get; set; }
        public Office office { get; set; }
        public string corporateIdentityNumberLabel { get; set; }
        public string corporateIdentityNumber { get; set; }
        public InactiveReason inactiveReason { get; set; }
        public ContactDetails contactDetails { get; set; }
        public AuditDetails auditDetails { get; set; }
    }
    public class Type
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class Status
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class Industry
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class State
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class Country
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class MainAddress
    {
        public string addressLineOne { get; set; }
        public string addressLineTwo { get; set; }
        public string suburb { get; set; }
        public string city { get; set; }
        public State state { get; set; }
        public Country country { get; set; }
        public string postCode { get; set; }
    }

    public class Office
    {
        public int id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public int brandId { get; set; }
        public string brandName { get; set; }
        public int regionId { get; set; }
        public string region { get; set; }
        public Country country { get; set; }
        public string accountSegment { get; set; }
        public string importCode { get; set; }
        public string exportCode { get; set; }
        public string status { get; set; }
    }

    public class InactiveReason
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class ContactDetails
    {
        public string emailOne { get; set; }
        public string emailTwo { get; set; }
        public string fax { get; set; }
        public string webSite { get; set; }
        public string mobilePhone { get; set; }
        public string phoneBusinessHours { get; set; }
        public string phoneAfterHours { get; set; }
        public string facebookURL { get; set; }
        public string linkedinURL { get; set; }
        public string twitterURL { get; set; }
    }

    public class AuditDetails
    {
        public int createdByUserId { get; set; }
        public string createdDate { get; set; }
        public string createdDateUtc { get; set; }
        public int lastUpdatedByUserId { get; set; }
        public string lastUpdatedDate { get; set; }
        public string lastUpdatedDateUtc { get; set; }
    }

    
    public class ParentCompanies
    {
        public IList<ParentCompany> data { get; set; }
    }
    public class ParentCompanyOne
    {
        public ParentCompany data { get; set; }
    }
}
