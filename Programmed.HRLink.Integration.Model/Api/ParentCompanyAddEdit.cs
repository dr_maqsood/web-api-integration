﻿namespace Programmed.FT360.API.Model.ParentCompany
{
    public class ParentCompanyAddEdit
    {
        //public int id { get; set; }
        public string name { get; set; }
        //public int? typeId { get; set; }
        public int statusId { get; set; }
        //public string migrationId { get; set; }
        //public int? industryId { get; set; }
        public long ownerUserId { get; set; }
        public MainAddress mainAddress { get; set; }
        public MailingAddress mailingAddress { get; set; }
        //public string profile { get; set; }
        //public string notes { get; set; }
        public int officeId { get; set; }
        //public int? corporateIdentityNumber { get; set; }
        public string alternateNumber { get; set; }
        public int? inactiveReasonId { get; set; }
        //public ContactDetails contactDetails { get; set; }

    }
    //public class MainAddress
    //{
    //    public string addressLineOne { get; set; }
    //    public string addressLineTwo { get; set; }
    //    public string suburb { get; set; }
    //    public string city { get; set; }
    //    public int stateId { get; set; }
    //    public int countryId { get; set; }
    //    public string postCode { get; set; }
    //}

    //public class State
    //{
    //    public int? id { get; set; }
    //    public string name { get; set; }
    //    public bool status { get; set; }
    //}

    //public class Country
    //{
    //    public int? id { get; set; }
    //    public string name { get; set; }
    //    public bool status { get; set; }
    //}

    public class MailingAddress
    {
        public string addressLineOne { get; set; }
        public string addressLineTwo { get; set; }
        public string suburb { get; set; }
        public string city { get; set; }
        public State state { get; set; }
        public Country country { get; set; }
        public string postCode { get; set; }
    }
    //public class ContactDetails
    //{
    //    public string emailOne { get; set; }
    //    public string emailTwo { get; set; }
    //    public string fax { get; set; }
    //    public string webSite { get; set; }
    //    public string mobilePhone { get; set; }
    //    public string phoneBusinessHours { get; set; }
    //    public string phoneAfterHours { get; set; }
    //    public string facebookURL { get; set; }
    //    public string linkedinURL { get; set; }
    //    public string twitterURL { get; set; }
    //}
}