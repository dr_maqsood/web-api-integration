﻿using System.Collections.Generic;

namespace Programmed.FT360.API.Model.TempJob
{
    public class TempJob
    {
        public int id { get; set; }
        public Office office { get; set; }
        public int clientId { get; set; }
        public int candidateId { get; set; }
        public int filledByUserId { get; set; }
        public string filledByDate { get; set; }
        public string filledByDateUtc { get; set; }
        public string filledByTime { get; set; }
        public string filledByTimeUtc { get; set; }
        public string cancelDate { get; set; }
        public string cancelDateUtc { get; set; }
        public string cancelTime { get; set; }
        public string cancelTimeUtc { get; set; }
        public string orderDate { get; set; }
        public string orderDateUtc { get; set; }
        public string orderTime { get; set; }
        public string orderTimeUtc { get; set; }
        public string department { get; set; }
        public int costCentreId { get; set; }
        public Status status { get; set; }
        public StatusReason statusReason { get; set; }
        public bool submitToTimeSheet { get; set; }
        public bool extPossibility { get; set; }
        public ServiceType serviceType { get; set; }
        public Type type { get; set; }
        public int? jobFamilyId { get; set; }
        public string alternateNumber { get; set; }
        public string migrationId { get; set; }
        public Source source { get; set; }
        public NeedReason needReason { get; set; }
        public bool exclusive { get; set; }
        public string exclusiveDate { get; set; }
        public string purchaseOrderNo { get; set; }
        public string purchaseOrderGroup { get; set; }
        public int? orderedByContactId { get; set; }
        public int? siteContactId { get; set; }
        public int? reportToContactId { get; set; }
        public int creatorUserId { get; set; }
        public double probabilityFill { get; set; }
        public int skillGroupId { get; set; }
        public int positionId { get; set; }
        public string clientPosition { get; set; }
        public WorkType workType { get; set; }
        public string location { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public int ownerUserId { get; set; }
        public string description { get; set; }
        public JobRateFrequency jobRateFrequency { get; set; }
        public OtherAgency otherAgency { get; set; }
        public WorkCoverState workCoverState { get; set; }
        public WorkCoverCode workCoverCode { get; set; }
        public bool? isPayrollTaxExempt { get; set; }
        public IList<TaxExemption> taxExemptions { get; set; }
        public IList<BillingFeeExemption> billingFeeExemptions { get; set; }
        public PayrollTaxState payrollTaxState { get; set; }
        public string payeeTaxType { get; set; }
        public TimesheetFrequencyDetails timesheetFrequencyDetails { get; set; }
        public bool splitTimeSheetatEndofCalendarMonth { get; set; }
        public bool timeSheetCodeRequired { get; set; }
        public string lastTimesheetEndDate { get; set; }
        public EstimatedRate estimatedRate { get; set; }
        public Workplace workplace { get; set; }
        public string advertisementDescription { get; set; }
        public AuditDetails auditDetails { get; set; }
        public AccCodeEntity accCodeEntity { get; set; }
    }
    public class State
    {
        public int? id { get; set; }
        public string name { get; set; }
    }

    public class Country
    {
        public int? id { get; set; }
        public string name { get; set; }
        public State state { get; set; }
    }

    public class Office
    {
        public int id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public int brandId { get; set; }
        public string brandName { get; set; }
        public int regionId { get; set; }
        public string region { get; set; }
        public Country country { get; set; }
    }

    public class Status
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class StatusReason
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class ServiceType
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class Type
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class Source
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class NeedReason
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class WorkType
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class JobRateFrequency
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class OtherAgency
    {
        public string name { get; set; }
        public string candidateNo { get; set; }
        public string candidate { get; set; }
        public string jobNumber { get; set; }
        public string clientNumber { get; set; }
        public string positionTitle { get; set; }
    }

    public class WorkCoverState
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class WorkCoverCode
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class TaxExemption
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class BillingFeeExemption
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class PayrollTaxState
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class TimeSheetFrequency
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class WeekEndDay
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }
    }

    public class TimesheetFrequencyDetails
    {
        public TimeSheetFrequency timeSheetFrequency { get; set; }
        public WeekEndDay weekEndDay { get; set; }
        public int? period1StartDay { get; set; }
        public int? period2StartDay { get; set; }
        public string cycleStartDate { get; set; }
    }

    public class EstimatedRate
    {
        public decimal payRate { get; set; }
        public decimal billRate { get; set; }
    }

    public class Workplace
    {
        public string description { get; set; }
        public string clothingDressRequirement { get; set; }
        public string travelInstructions { get; set; }
        public string mealAmenities { get; set; }
        public string inspectionDate { get; set; }
        public string occupationalHealthSafetyDetails { get; set; }
    }

    public class AuditDetails
    {
        public int createdByUserId { get; set; }
        public string createdDate { get; set; }
        public string createdDateUtc { get; set; }
        public int? lastUpdatedByUserId { get; set; }
        public string lastUpdatedDate { get; set; }
        public string lastUpdatedDateUtc { get; set; }
    }

    public class AccCodeEntity
    {
        public int accCodeId { get; set; }
        public string code { get; set; }
        public bool active { get; set; }
        public string industry { get; set; }
    }

    public class TempJobs
    {
        public IList<TempJob> data { get; set; }
    }

    public class TempJobOne
    {
        public TempJob data { get; set; }
    }
}
