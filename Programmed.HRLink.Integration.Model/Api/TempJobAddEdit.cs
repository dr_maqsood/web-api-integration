﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Programmed.FT360.API.Model.TempJob;

namespace Programmed.HRLink.Integration.Model.Api
{
    public class TempJobAddEdit
    {
        public int officeId { get; set; }
        public long? clientId { get; set; }
        public long? candidateId { get; set; }
        public int filledByUserId { get; set; }
        public string department { get; set; }
        public long? costCentreId { get; set; }
        public int statusId { get; set; }
        public int statusReasonId { get; set; }
        public bool submitToTimeSheet { get; set; }
        public string cancelDate { get; set; }
        public string cancelTime { get; set; }
        public bool extPossibility { get; set; }
        public int jobOrderServiceTypeId { get; set; }
        public int? typeId { get; set; }
        public int? jobFamilyId { get; set; }
        public string alternateNumber { get; set; }
        public string migrationId { get; set; }
        public int sourceId { get; set; }
        public int? needReasonId { get; set; }
        public bool exclusive { get; set; }
        [JsonConverter(typeof(DateFormatConverter), "yyyy-MM-dd")]
        public DateTime exclusiveDate { get; set; }
        public string purchaseOrderNo { get; set; }
        public string purchaseOrderGroup { get; set; }
        public int? orderedByContactId { get; set; }
        public int? siteContactId { get; set; }
        public int? reportToContactId { get; set; }
        public int? creatorUserId { get; set; }
        [JsonConverter(typeof(DateFormatConverter), "yyyy-MM-dd")]
        public DateTime orderDate { get; set; }
        public string orderTime { get; set; }
        public double probabilityFill { get; set; }
        public int skillGroupId { get; set; }
        public int positionId { get; set; }
        public string clientPosition { get; set; }
        public int workTypeId { get; set; }
        public string location { get; set; }
        [JsonConverter(typeof(DateFormatConverter), "yyyy-MM-dd")]
        public DateTime startDate { get; set; }
        [JsonConverter(typeof(DateFormatConverter), "yyyy-MM-dd")]
        public DateTime endDate { get; set; }
        public int ownerUserId { get; set; }
        public string description { get; set; }
        public int? jobRateFrequencyId { get; set; }
        public OtherAgency otherAgency { get; set; }
        public int? workCoverStateId { get; set; }
        public int? workCoverCodeId { get; set; }
        public bool isPayrollTaxExempt { get; set; }
        public IList<int> taxExemptionsId { get; set; }
        public IList<int> billingFeeExemptionsId { get; set; }
        public int payrollTaxStateId { get; set; }
        public TimesheetFrequencyDetails timesheetFrequencyDetails { get; set; }
        public bool splitTimeSheetatEndofCalendarMonth { get; set; }
        public bool timeSheetCodeRequired { get; set; }
        public Workplace workplace { get; set; }
        public string advertisementDescription { get; set; }

        public class OtherAgency
        {
            public string name { get; set; }
            public string candidateNo { get; set; }
            public string candidate { get; set; }
            public string jobNumber { get; set; }
            public string clientNumber { get; set; }
            public string positionTitle { get; set; }
        }

        public class TimesheetFrequencyDetails
        {
            public int timeSheetFrequencyId { get; set; }
            public int weekEndDayId { get; set; }
            public int period1StartDay { get; set; }
            public int period2StartDay { get; set; }
            public string cycleStartDate { get; set; }
        }
    }
    public class DateFormatConverter : IsoDateTimeConverter
    {
        public DateFormatConverter(string format)
        {
            DateTimeFormat = format;
        }
    }
}