﻿namespace Programmed.HRLink.Integration.Model.Api
{
    public class UpdateResponse
    {
        public long Id { get; set; }
        public string ErrorMessage { get; set; }
    }
}
