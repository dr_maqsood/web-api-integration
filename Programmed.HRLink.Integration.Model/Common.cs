﻿using System;

namespace Programmed.HRLink.Integration
{
    public class Common
    {     
        public static class Constants
        {
            public static class Country
            {
                public const int Australia = 16;
            }
            public static class CandidateStatus
            {
                public const int Active = 758;
                public const int Inactive = 759;
            }
            public static class CandidateType
            {
                public const int Quarantine = 5;
            }
            public static class MemberInteractionMethod
            {
                public const int Email = 3;
                public const int Mobile = 1;
            }
            public static class FTStatus
            {
                public const Int16 ToBeSent = 10;
                public const Int16 Sent = 20;
                public const Int16 Error = 30;
            }
            public static class FTSkillGroup
            {
                public const int Common = 41;
            }
            public static class Agency
            {
                public const int Php = 4;
            }
            public static class FTParentCompanyStatus
            {
                public const int Active = 198;
                public const int Inactive = 199;
            }

            public static class ClientStatus
            {
                public const int Active = 198;
                public const int Inactive = 199;
            }

            public static class CostCentreStatus
            {
                public const int Active = 198;
                public const int Inactive = 199;
            }
            public static class InactiveReason
            {
                public const int CandidateInactiveReasonId = 16;
                public const int ParentCompanyInactiveReasonId = 27;
                public const int ClientInactiveReasonId = 23;
                public const int CostCentreInactiveReasonId = 29;
            }
            public static class GreatPlainsNetwork
            {
                public const int CentralNetworkId = 8010;
            }
        }
        /// <summary>
        /// Get hr link state code
        /// </summary>
        /// <param name="stateId"></param>
        /// <returns></returns>
        public static int GetHRLinkStateCode(int? stateId)
        {
            if (!stateId.HasValue)
                return 3000;

            //800     NT
            //2000    NSW
            //2600    ACT
            //3000    VIC
            //4000    QLD
            //5000    SA
            //6000    WA
            //7000    TAS

            switch (stateId)
            {
                case 1:
                    return 2000;
                case 2:
                    return 3000;
                case 3:
                    return 4000;
                case 4:
                    return 5000;
                case 5:
                    return 6000;
                case 6:
                    return 7000;
                case 7:
                    return 2600;
                case 8:
                    return 800;
            }

            return 3000;
        }
        /// <summary>
        /// Get fast track state id
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public static int GetFTStateId(string state)
        {
            //800     NT
            //2000    NSW
            //2600    ACT
            //3000    VIC
            //4000    QLD
            //5000    SA
            //6000    WA
            //7000    TAS
            switch (state)
            {
                case "NSW":
                    return 1;
                case "VIC":
                    return 2;
                case "QLD":
                    return 3;
                case "SA":
                    return 4;
                case "WA":
                    return 5;
                case "TAS":
                    return 6;
                case "ACT":
                    return 7;
                case "NT":
                    return 8;            
            }
            return 0;
        }
        /// <summary>
        /// Get fast track salutation id
        /// </summary>
        /// <param name="salutation"></param>
        /// <returns></returns>
        public static int GetSalutationId(string salutation)
        {            
            switch(salutation)
            {
                case "Miss":
                    return 157;
                case "Mr":
                    return 158;
                case "Mrs":
                    return 159;
                case "Ms":
                    return 160;
                case "Dr":
                    return 161;
                default:
                    return 157;
            }            
        }
        /// <summary>
        /// Get member status
        /// </summary>
        /// <param name="ftStatusId"></param>
        /// <returns></returns>
        public static bool GeMemberStatus(int ftStatusId)
        {
            switch(ftStatusId)
            {
                case Constants.CandidateStatus.Active: 
                    return true;
                case Constants.CandidateStatus.Inactive://Inactive
                    return false;
            }
            return false;
        }
        /// <summary>
        /// Is fast track candidate qurantine
        /// </summary>
        /// <param name="ftTypeId"></param>
        /// <param name="ftStatusId"></param>
        /// <returns></returns>
        public static bool IsFTCandidateQuarantine(int? ftTypeId, int ftStatusId)
        {
            return ftStatusId == Constants.CandidateStatus.Inactive && ftTypeId == Constants.CandidateType.Quarantine;
        }
        /// <summary>
        /// Get fast track candidate status
        /// </summary>
        /// <param name="memberStatus"></param>
        /// <returns></returns>
        public static int GetCandidateStatus(bool? memberStatus)
        {
            switch (memberStatus)
            {
                case true: 
                    return Constants.CandidateStatus.Active;//Active
                case false://Inactive
                    return Constants.CandidateStatus.Inactive;
            }
            return Constants.CandidateStatus.Inactive;
        }
        /// <summary>
        /// Is Candidate Quarantine Type
        /// </summary>
        /// <param name="memberStatus"></param>
        /// <param name="quarantineStatus"></param>
        /// <returns></returns>
        public static int? GetCandidateType(bool? memberStatus, bool? quarantineStatus)
        {
            if (memberStatus == false && quarantineStatus == true)
                return Constants.CandidateType.Quarantine;
            else
                return null;            
        }

        public static bool GeHRLinkClientStatus(int ftStatusId)
        {
            switch (ftStatusId)
            {
                case Constants.ClientStatus.Active:
                    return true;
                case Constants.ClientStatus.Inactive://Inactive
                    return false;
            }
            return false;
        }
        /// <summary>
        /// Get fast track client status
        /// </summary>
        /// <param name="clientStatus"></param>
        /// <returns></returns>
        public static int GetFTClientStatus(bool? clientStatus)
        {
            switch (clientStatus)
            {
                case true:
                    return Constants.ClientStatus.Active;//Active
                case false://Inactive
                    return Constants.ClientStatus.Inactive;
            }
            return Constants.ClientStatus.Inactive;
        }
        /// <summary>
        /// Get fast track const centre status
        /// </summary>
        /// <param name="enabledStatus"></param>
        /// <returns></returns>
        public static int GetCostCentreStatus(bool? enabledStatus)
        {
            switch (enabledStatus)
            {
                case true:
                    return Constants.CostCentreStatus.Active;//Active
                case false://Inactive
                    return Constants.CostCentreStatus.Inactive;
            }
            return Constants.ClientStatus.Inactive;
        }
        /// <summary>
        /// Get fast track gender id
        /// </summary>
        /// <param name="memberSex"></param>
        /// <returns></returns>
        public static int GetFTGenderId(string memberSex)
        {
            switch (memberSex)
            {
                case "M":
                    return 163;
                case "F":
                    return 164;
                case "U":
                    return 162;
            }
            return 162;
        }
        /// <summary>
        /// Get fast track state Id
        /// </summary>
        /// <param name="locationId"></param>        
        /// <returns></returns>
        public static int GetFTStateId(short locationId)
        {            
            switch (locationId)
            {
                case 2600://ACT
                    return 1;
                case 2000://NSW                                       
                    return 2;
                case 800://NT
                    return 3;
                case 4000://QLD
                    return 4;
                case 5000://SA
                    return 5;
                case 7000://TAS
                    return 6;
                case 3000://VIC
                    return 7;                
                case 6000://WA
                    return 8;
                default://VIC
                    return 7;
            }
        }
        /// <summary>
        /// Get fast track work cover code Id
        /// </summary>
        /// <param name="locationId"></param>        
        /// <returns></returns>
        public static int GetFTWorkCoverCodeId(short locationId)
        {
            switch (locationId)
            {
                case 2600://ACT
                    return 24;
                case 2000://NSW                                       
                    return 20;
                case 800://NT
                    return 25;
                case 4000://QLD
                    return 18;
                case 5000://SA
                    return 21;
                case 7000://TAS
                    return 23;
                case 3000://VIC
                    return 19;
                case 6000://WA
                    return 22;
                default://VIC
                    return 19;
            }
        }
    }
}