﻿namespace Programmed.HRLink.Integration.Database.Model
{
    using Integration.Model.Database;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Client")]
    public partial class Client
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Client()
        {
            ClientCampus = new HashSet<ClientCampus>();
            ClientCostCentres = new HashSet<ClientCostCentre>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public short ClientId { get; set; }

        [Required]
        [StringLength(40)]
        public string Name { get; set; }

        public int GreatPlainsNetworkID { get; set; }

        public short LocationId { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Timestamp { get; set; }

        public byte? ClientType { get; set; }

        public int ClientNetworkID { get; set; }

        public byte? CriminalRecordCheckStatus { get; set; }

        [StringLength(20)]
        public string LastUpdatedBy { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? LastUpdatedDate { get; set; }

        public byte PrimaryAgencyID { get; set; }

        public bool AccreditedFacilityStatus { get; set; }

        public bool NoLiftPolicyStatus { get; set; }

        [StringLength(40)]
        public string CEO { get; set; }

        [StringLength(40)]
        public string DON { get; set; }

        public bool AcceptMidwifeStatus { get; set; }

        public bool AnnualCRCStatus { get; set; }

        public bool? ManualHandlingStatus { get; set; }

        public bool WorkingWithChildrenCheck { get; set; }

        public int? CRCExpiryPeriod { get; set; }

        public int? ClientRatingID { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? RequireOrientation { get; set; }

        public bool? EnabledStatus { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? StatusChangedDate { get; set; }

        public long? FTClientId { get; set; }
        public long? FTOwnerId { get; set; }
        public int? FTOfficeId { get; set; }
        public short? FTStatus { get; set; }
        public string FTErrorDescription { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ClientAgency> ClientAgencies { get; set; }

        public virtual ClientFinance ClientFinance { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ClientCampus> ClientCampus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ClientCostCentre> ClientCostCentres { get; set; }
        //public virtual ClientNetwork ClientNetwork { get; set; }
    }
}