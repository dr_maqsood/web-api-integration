﻿namespace Programmed.HRLink.Integration.Database.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ClientAgency")]
    public partial class ClientAgency
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClientAgencyID { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short ClientID { get; set; }

        [Key]
        [Column(Order = 1)]
        public byte AgencyID { get; set; }

        public int GreatPlainsClientID { get; set; }

        public bool EnabledStatus { get; set; }

        [StringLength(20)]
        public string LastUpdatedBy { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? LastUpdatedDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Timestamp { get; set; }

        public bool ServiceAgreementStatus { get; set; }

        public bool ProcessPayrollStatus { get; set; }

        [StringLength(10)]
        public string VendorNumber { get; set; }

        [StringLength(100)]
        public string InvoiceEmailAddress { get; set; }

        public bool? DoNotEmailInvoices { get; set; }

        public virtual Client Client { get; set; }
    }
}
