﻿namespace Programmed.HRLink.Integration.Model.Database
{
    using Integration.Database.Model;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class ClientCampus
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ClientCampus()
        {
            ClientCampusWards = new HashSet<ClientCampusWard>();
        }

        [Key]
        public short CampusId { get; set; }

        public short ClientId { get; set; }

        [Required]
        [StringLength(40)]
        public string Name { get; set; }

        [StringLength(30)]
        public string Street1 { get; set; }

        [StringLength(30)]
        public string Abbrev1 { get; set; }

        [StringLength(30)]
        public string Street2 { get; set; }

        [StringLength(30)]
        public string Abbrev2 { get; set; }

        [StringLength(30)]
        public string Suburb { get; set; }

        [StringLength(6)]
        public string State { get; set; }

        [StringLength(10)]
        public string MapRef { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Timestamp { get; set; }

        [StringLength(4)]
        public string Postcode { get; set; }

        [StringLength(20)]
        public string LastUpdatedBy { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? LastUpdatedDate { get; set; }

        public byte? MapType { get; set; }

        public short BedNumbers { get; set; }

        public short ShiftsPerWeek { get; set; }

        public short? OfficeID { get; set; }

        public bool EnabledStatus { get; set; }

        public byte VisitFrequency { get; set; }

        public bool RequestOrderEMail { get; set; }

        public byte OHSRating { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? OHSDueDate { get; set; }

        [StringLength(10)]
        public string WICCode { get; set; }

        public byte NewOHSRating { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? WRADueDate { get; set; }

        public int? FilterId { get; set; }

        public bool AllowWardOHS { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? RequireOrientation { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? HHICReviewDate { get; set; }

        public virtual Client Client { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ClientCampusWard> ClientCampusWards { get; set; }
    }
}
