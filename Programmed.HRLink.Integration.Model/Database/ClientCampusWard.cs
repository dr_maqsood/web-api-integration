﻿namespace Programmed.HRLink.Integration.Model.Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ClientCampusWard")]
    public partial class ClientCampusWard
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ClientCampusWard()
        {
            WardCostCentreDates = new HashSet<WardCostCentreDate>();
        }

        [Key]
        public int WardId { get; set; }

        public short CampusId { get; set; }

        [Required]
        [StringLength(40)]
        public string Name { get; set; }

        public byte WorkAreaId { get; set; }

        [StringLength(30)]
        public string Description { get; set; }

        [StringLength(30)]
        public string ChargeNurse { get; set; }

        [StringLength(30)]
        public string PhoneNbr { get; set; }

        [StringLength(7000)]
        public string Comments { get; set; }

        public bool AcceptNewRecruitStatus { get; set; }

        public bool EnabledStatus { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Timestamp { get; set; }

        public bool CallAboutStaffStatus { get; set; }

        public bool CallDirectToWardStatus { get; set; }

        [StringLength(20)]
        public string LastUpdatedBy { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? LastUpdatedDate { get; set; }

        [StringLength(30)]
        public string FaxNbr { get; set; }

        public short BedNumbers { get; set; }

        [StringLength(1)]
        public string Sex { get; set; }

        public bool? WCCRequired { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? RequireOrientation { get; set; }

        [StringLength(30)]
        public string Street1 { get; set; }

        [StringLength(30)]
        public string Abbrev1 { get; set; }

        [StringLength(30)]
        public string Street2 { get; set; }

        [StringLength(30)]
        public string Abbrev2 { get; set; }

        [StringLength(30)]
        public string Suburb { get; set; }

        [StringLength(4)]
        public string Postcode { get; set; }

        [StringLength(6)]
        public string State { get; set; }

        public byte? MapType { get; set; }

        [StringLength(10)]
        public string MapRef { get; set; }

        public byte OHSRating { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? OHSDueDate { get; set; }

        [StringLength(10)]
        public string WICCode { get; set; }

        public byte NewOHSRating { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? WRADueDate { get; set; }

        public bool? HasVehicle { get; set; }

        [StringLength(7)]
        public string VehicleRegistration { get; set; }

        [Column(TypeName = "date")]
        public DateTime? VehicleRegistrationExpiry { get; set; }

        public byte? VehicleInsuranceTypeID { get; set; }

        [Column(TypeName = "date")]
        public DateTime? VehicleInsuranceExpiry { get; set; }

        [Column(TypeName = "date")]
        public DateTime? VehicleInspection { get; set; }

        public virtual ClientCampus ClientCampus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WardCostCentreDate> WardCostCentreDates { get; set; }
    }
}
