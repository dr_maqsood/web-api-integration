﻿namespace Programmed.HRLink.Integration.Model.Database
{
    using Integration.Database.Model;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ClientCostCentre")]
    public partial class ClientCostCentre
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ClientCostCentre()
        {
            WardCostCentreDates = new HashSet<WardCostCentreDate>();
        }

        public short ClientId { get; set; }

        [Key]
        public int CostCentreId { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [StringLength(40)]
        public string Description { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Timestamp { get; set; }

        [StringLength(20)]
        public string LastUpdatedBy { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? LastUpdatedDate { get; set; }

        public virtual Client Client { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WardCostCentreDate> WardCostCentreDates { get; set; }
    }
}
