﻿namespace Programmed.HRLink.Integration.Database.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ClientFinance")]
    public partial class ClientFinance
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short ClientID { get; set; }

        [StringLength(30)]
        public string AccountsContactName { get; set; }

        [StringLength(20)]
        public string AccountsContactPosition { get; set; }

        public int InvoiceSplitType { get; set; }

        [Required]
        [StringLength(30)]
        public string PostalStreet1 { get; set; }

        [StringLength(30)]
        public string PostalStreet2 { get; set; }

        [Required]
        [StringLength(30)]
        public string PostalSuburb { get; set; }

        [Required]
        [StringLength(10)]
        public string PostalState { get; set; }

        [StringLength(6)]
        public string PostalPostcode { get; set; }

        [StringLength(30)]
        public string PhoneNbr { get; set; }

        [StringLength(30)]
        public string FaxNbr { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Timestamp { get; set; }

        public byte? PayrollTaxStatus { get; set; }

        public byte? PayrollTaxCollectionStatus { get; set; }

        public bool GSTStatus { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal TravelAllowancePerShift { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal TravelAllowancePerKilometre { get; set; }

        [StringLength(37)]
        public string AddressBarcode { get; set; }

        public bool AuthorisesShiftsWithICT { get; set; }

        [StringLength(10)]
        public string InternalCompanyCode { get; set; }

        public bool PrintTimesheetStatus { get; set; }

        [StringLength(100)]
        public string LegalEntity { get; set; }

        public bool BaycorpMonitor { get; set; }

        [StringLength(14)]
        public string ABN { get; set; }

        public bool ConsolidatedInvoice { get; set; }

        [StringLength(100)]
        public string InvoiceEmailAddress { get; set; }

        public bool? DoNotEmailInvoices { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal? TravelAllowancePerKilometreClient { get; set; }

        public virtual Client Client { get; set; }
    }
}
