﻿namespace Programmed.HRLink.Integration.Database.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ClientNetwork")]
    public partial class ClientNetwork
    {        
        public int ClientNetworkID { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Timestamp { get; set; }

        public short LocationID { get; set; }

        public int? FTOfficeId { get; set; }

        public long? FTParentId { get; set; }

        public long? FTOwnerId { get; set; }

        public int? FTStatusId { get; set; }

        public short? FTStatus { get; set; }
        public string FTErrorDescription { get; set; }
    }
}
