﻿namespace Programmed.HRLink.Integration.Model.Database
{
    public class FT360Timesheet
    {
        public int? Candidate_ID { get; set; }
        public long? Client_No { get; set; }
        public int? Cost_Centre_Code { get; set; }
        public long? Job_ID { get; set; }
        public int Job_Alt_No { get; set; }
        public string Item_Date { get; set; }
        public string Pay_Code_Name { get; set; }
        public short Quantity { get; set; }
        public string Attendance_Item_Name { get; set; }
        public string Start_Time_1 { get; set; }
        public string End_Time_1 { get; set; }
        public string Break_Start_1 { get; set; }
        public string Break_End_1 { get; set; }
        public string Start_Time_2 { get; set; }
        public string End_Time_2 { get; set; }
    }
}