﻿using System;

namespace Programmed.HRLink.Integration.Model.Database
{
    public class LockedShift
    {
        public int ShiftID { get; set; }
        public long? FTClientId { get; set; }
        public long? FTCandidateId { get; set; }
        public long? FTCostCentreId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public string PayClassName { get; set; }
        public int InvoiceSplitType { get; set; }
    }
}