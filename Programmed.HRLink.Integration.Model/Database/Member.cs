namespace Programmed.HRLink.Integration.Database.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Member")]
    public partial class Member
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Member()
        {
            MemberAgencies = new HashSet<MemberAgency>();
            MemberInteractionMethods = new HashSet<MemberInteractionMethod>();
        }

        public int MemberID { get; set; }

        [Required]
        [StringLength(20)]
        public string Dear { get; set; }

        [Required]
        [StringLength(6)]
        public string Title { get; set; }

        [Required]
        [StringLength(30)]
        public string GivenName { get; set; }

        [Required]
        [StringLength(30)]
        public string Surname { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime BirthDate { get; set; }

        [Required]
        [StringLength(1)]
        public string Sex { get; set; }

        public int LocationID { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Timestamp { get; set; }

        [StringLength(30)]
        public string MaidenName { get; set; }

        [StringLength(30)]
        public string PriorGivenName { get; set; }

        public bool? RequestCRC { get; set; }

        public bool? SecondTierMember { get; set; }

        public long? FTCandidateId { get; set; }

        public short? FTStatus { get; set; }

        public long? FTOwnerId { get; set; }

        public int? FTSkillGroupId { get; set; }

        public int? FTOfficeId { get; set; }

        public string FTErrorDescription { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MemberAgency> MemberAgencies { get; set; }

        public virtual MemberContact MemberContact { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MemberInteractionMethod> MemberInteractionMethods { get; set; }
        public int? FTSourceId { get; set; }
    }
}