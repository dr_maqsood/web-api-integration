namespace Programmed.HRLink.Integration.Database.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("MemberAgency")]
    public partial class MemberAgency
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MemberID { get; set; }

        [Key]
        [Column(Order = 1)]
        public byte AgencyID { get; set; }

        public bool EnabledStatus { get; set; }

        public bool QuarantineStatus { get; set; }

        public bool MailingListStatus { get; set; }

        public int? AdvertisingType { get; set; }

        [StringLength(20)]
        public string AgencyRepresentative { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? ReviewDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Timestamp { get; set; }

        public byte PrefillStatus { get; set; }

        public int? WhatPromptedYou { get; set; }

        [StringLength(50)]
        public string WhatPromptedYouOther { get; set; }

        public bool DoNotUseForElectronicContact { get; set; }

        public int? ExitReasonId { get; set; }

        public virtual Member Member { get; set; }
    }
}
