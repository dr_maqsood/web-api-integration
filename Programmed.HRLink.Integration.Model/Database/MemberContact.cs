namespace Programmed.HRLink.Integration.Database.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("MemberContact")]
    public partial class MemberContact
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MemberID { get; set; }

        [Required]
        [StringLength(30)]
        public string Street1 { get; set; }

        [StringLength(30)]
        public string Abbrev1 { get; set; }

        [StringLength(30)]
        public string Street2 { get; set; }

        [StringLength(30)]
        public string Abbrev2 { get; set; }

        [Required]
        [StringLength(30)]
        public string Suburb { get; set; }

        [Required]
        [StringLength(10)]
        public string State { get; set; }

        [Required]
        [StringLength(6)]
        public string PostalPostcode { get; set; }

        [StringLength(30)]
        public string KinName { get; set; }

        [StringLength(20)]
        public string KinRelationship { get; set; }

        [StringLength(30)]
        public string KinStreet1 { get; set; }

        [StringLength(30)]
        public string KinStreet2 { get; set; }

        [StringLength(30)]
        public string KinSuburb { get; set; }

        [StringLength(6)]
        public string KinState { get; set; }

        [StringLength(10)]
        public string KinPostcode { get; set; }

        [StringLength(20)]
        public string KinCountry { get; set; }

        [StringLength(15)]
        public string KinPhoneNbr1 { get; set; }

        [StringLength(15)]
        public string KinPhoneNbr2 { get; set; }

        [StringLength(30)]
        public string PagerNbr { get; set; }

        [Required]
        [StringLength(30)]
        public string PostalStreet1 { get; set; }

        [StringLength(30)]
        public string PostalStreet2 { get; set; }

        [Required]
        [StringLength(30)]
        public string PostalSuburb { get; set; }

        [StringLength(6)]
        public string PostalState { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? PagerDateIssue { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime ModifyDate { get; set; }

        [StringLength(20)]
        public string UserName { get; set; }

        [StringLength(20)]
        public string Street1No { get; set; }

        [StringLength(20)]
        public string Street2No { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Timestamp { get; set; }

        [StringLength(9)]
        public string MapRef { get; set; }

        [StringLength(37)]
        public string AddressBarcode { get; set; }

        [StringLength(30)]
        public string PostalCountry { get; set; }

        public bool? DoNotEmailPayslips { get; set; }

        public bool? SendSMSShiftReminder { get; set; }

        [StringLength(6)]
        public string Postcode { get; set; }

        public virtual Member Member { get; set; }
    }
}
