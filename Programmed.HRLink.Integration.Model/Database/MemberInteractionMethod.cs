namespace Programmed.HRLink.Integration.Database.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("MemberInteractionMethod")]
    public partial class MemberInteractionMethod
    {
        public int MemberInteractionMethodID { get; set; }

        public int MemberID { get; set; }

        public byte InteractionMethodID { get; set; }

        [StringLength(30)]
        public string Description { get; set; }

        [StringLength(100)]
        public string SendTo { get; set; }

        public byte ContactOrder { get; set; }

        public virtual Member Member { get; set; }
    }
}
