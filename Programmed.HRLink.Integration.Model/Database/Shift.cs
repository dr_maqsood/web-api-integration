﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Programmed.HRLink.Integration.Model.Database
{
    [Table("Shift")]
    public partial class Shift
    {
        public int ShiftID { get; set; }

        public short ClientID { get; set; }

        public int? MemberID { get; set; }

        public byte AgencyID { get; set; }

        public int? InvoiceID { get; set; }

        public int? PayslipID { get; set; }

        public short LocationID { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime FinishDate { get; set; }

        public byte? OutcomeType { get; set; }

        public short? CampusID { get; set; }

        public int? WardID { get; set; }

        [StringLength(20)]
        public string ConfirmedWith { get; set; }

        public short? SourceTerminalID { get; set; }

        [StringLength(6)]
        public string PayRequestedClassName { get; set; }

        public bool PaidMealBreakStatus { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PayHrsPaid1 { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal PayRate1 { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal PayAmount1 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PayHrsPaid2 { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal PayRate2 { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal PayAmount2 { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal PayShiftAllowAmount { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal PayUniformAllowAmount { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal PayGrossAmount { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal PayAdminFeeAmount { get; set; }

        public bool CheckTimesStatus { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal InvAmount1 { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal InvAmount2 { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal InvShiftAllowAmount { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal InvRate1 { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal InvRate2 { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal InvLineTotalAmount { get; set; }

        [StringLength(6)]
        public string PayClassName { get; set; }

        [StringLength(6)]
        public string InvClassName { get; set; }

        [StringLength(20)]
        public string OrderedBy { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? ConfirmedDate { get; set; }

        public short TradeID { get; set; }

        [StringLength(6)]
        public string WillPayClassName { get; set; }

        [StringLength(6)]
        public string PayMemberClassName { get; set; }

        public byte ReasonType { get; set; }

        public bool PayCertificateStatus { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal MarkupPercent { get; set; }

        public bool AuthorisedStatus { get; set; }

        public byte? WorkAreaID { get; set; }

        public byte? SlotRule { get; set; }

        public short? QualID { get; set; }

        [StringLength(50)]
        public string MemberRequested { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Timestamp { get; set; }

        [StringLength(600)]
        public string Comments { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal PayOnCallAmount { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? InsertDate { get; set; }

        public bool OnCallStatus { get; set; }

        public bool PayAllowanceStatus { get; set; }

        [StringLength(20)]
        public string PinkslipID { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? PinkslipIDDate { get; set; }

        [Required]
        [MaxLength(3)]
        public byte[] Allocated { get; set; }

        [StringLength(1)]
        public string Sex { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? PaidWE { get; set; }

        public int? PayrollTaxInvoiceID { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal? DeemedWagesAmount { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal? PayRollTaxAmount { get; set; }

        public byte? MealBreakTime { get; set; }

        public bool InChargeStatus { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal GSTAmount { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal InvUniformAllowAmount { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal PayCertAllowAmount { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal InvCertAllowAmount { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal PayParkingAllowAmount { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal PayTravelAllowanceAmount { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal InvTravelAllowanceAmount { get; set; }

        public short Kilometres { get; set; }

        public int? TimesheetID { get; set; }

        [StringLength(600)]
        public string PublicComments { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal? InvChargeFee { get; set; }

        public short? KilometresClient { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal? PayTravelClientAllowanceAmount { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal? InvTravelClientAllowanceAmount { get; set; }

        public long? FTJobId { get; set; }
        public bool? FTExported { get; set; }
    }
}
