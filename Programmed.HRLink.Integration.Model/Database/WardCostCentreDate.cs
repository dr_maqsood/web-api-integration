﻿namespace Programmed.HRLink.Integration.Model.Database
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("WardCostCentreDate")]
    public partial class WardCostCentreDate
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int WardId { get; set; }

        public int CostCentreId { get; set; }

        [Key]
        [Column(Order = 1, TypeName = "smalldatetime")]
        public DateTime EffectiveStartDate { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime EffectiveFinishDate { get; set; }

        [StringLength(20)]
        public string LastUpdatedBy { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? LastUpdatedDate { get; set; }

        public long? FTCostCentreId { get; set; }

        public int? FTOfficeId { get; set; }

        public long? FTOwnerId { get; set; }

        public short? FTStatus { get; set; }

        public string FTErrorDescription { get; set; }

        public virtual ClientCampusWard ClientCampusWard { get; set; }

        public virtual ClientCostCentre ClientCostCentre { get; set; }
    }
}
