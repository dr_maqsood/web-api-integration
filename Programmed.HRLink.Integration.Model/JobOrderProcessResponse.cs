﻿namespace Programmed.HRLink.Integration.Model
{
    public class JobOrderProcessResponse
    {
        public long JobId { get; set; }
        public int ShiftId { get; set; }
        public string ErrorMessage { get; set; }
    }
}
