﻿using System;
using Programmed.FT360.API.Endpoints.Candidate;
using Programmed.HRLink.Integration.Model.Api;
using Xunit;

namespace Programmed.HRLink.Integration.Test
{    
    public class ApiTest
    {
        public CandidateApiService candidateApiService;
        public ApiTest()
        {
            candidateApiService = new CandidateApiService("phpuat", "maqsood.api", "Fast1234", "https://phpapiuat.programmed.com.au/api/v120/", "login", 24, "candidates");
        }
        [Fact]
        public async void CandidateExists()
       {
            var candidates = await candidateApiService.GetCandidates();
            Assert.True(candidates.data != null);
            Assert.True(candidates.data.Count > 0);
        }

        [Fact]
        public async void CandidateByIdExists()
        {
            var candidates = await candidateApiService.GetCandidate(100000001);
            Assert.True(candidates.data != null);
            Assert.True(candidates.data.id > 0);
        }
        [Fact]
        public async void CandidateByIdNotExists()
        {
            var candidates = await candidateApiService.GetCandidate(0000000);
            Assert.True(candidates.data != null);
            Assert.True(candidates.data.id == 0);
        }

        [Fact]
        public async void CreateCandidateInFastTRack()
        {
            #region Create Fast Track Candidate Object
            var candAddEdit = new CandidateAddEdit
            {
                alternateNumberOne = "111",
                salutationId = Common.GetSalutationId("Mr"),
                firstName = "Test" + new Random().Next(),
                surname = "Surnname",
                skillGroupId = 1,
                officeId = 1,
                contactDetails = new ContactDetails
                {
                    emailOne = "email@email.com",
                    mobilePhone = "0412345",
                },
                mainAddress = new CandidateAddEdit.MainAddress
                {
                    addressLineOne = "Street 1",
                    addressLineTwo = "Street 2",
                    postCode = "3000",
                    stateId = Common.GetFTStateId("VIC"),
                    countryId = Common.Constants.Country.Australia
                },
                mailingAddress = new CandidateAddEdit.MainAddress
                {
                    addressLineOne = "Postal Street 1",
                    addressLineTwo = "Postal Street 2",
                    postCode = "3000",
                    stateId = Common.GetFTStateId("Vic"),
                    countryId = Common.Constants.Country.Australia
                },
                ownerUserId = 900000001,
                statusId = Common.GetCandidateStatus(true),
            };
            var updateResponse = await candidateApiService.UpdateCandidate(null, candAddEdit);
            Assert.True(updateResponse.Id > 0);
            #endregion
        }
    }
}
