﻿using Programmed.HRLink.Integration.Database.Services;
using Xunit;

namespace Programmed.HRLink.Integration.Test
{
    public class DatabaseTest
    {
        [Fact]
        public void MembersExist()
        {
            MemberService memberService = new MemberService();
            var members = memberService.GetMembersToBeSent();
            Assert.True(members != null);
        }
    }
}
