﻿using HrLinkShared;
using Xunit;

namespace Programmed.HRLink.Integration.Test
{
    public class HRLinkShareVB6Test
    {
        [Fact]
        public void TestShared()
        {
            var df = new DataFacade();
            string response = df.ProcessFT360JobOrders("3000");
            Assert.NotNull(response);
        }
    }
}
