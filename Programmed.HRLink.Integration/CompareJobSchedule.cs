﻿using System;
using Quartz;
using Quartz.Impl;

namespace Programmed.HRLink.FT360Integration
{
    /// <summary>
    /// Simple Qaurtz job scheduler to trigger compare data regularly.
    /// </summary>
    public class CompareJobScheduler : IDisposable
    {
        private readonly IScheduler _scheduler = StdSchedulerFactory.GetDefaultScheduler();

        public void Start<TJobType>() where TJobType : IJob
        {
            var trigger = TriggerBuilder.Create()
                                        .WithIdentity("CompareJobScheduler")
                                        .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(Configuration.CompareTimeInHour, 5))
                                        .Build();

            var job = JobBuilder.Create<TJobType>()
                                .Build();

            _scheduler.ScheduleJob(job, trigger);
            _scheduler.Start();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_scheduler != null && !_scheduler.IsShutdown)
                {
                    _scheduler.Shutdown();
                }
            }
        }
    }
}
