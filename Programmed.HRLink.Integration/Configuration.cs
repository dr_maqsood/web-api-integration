﻿using System;
using System.Text;

namespace Programmed.HRLink.FT360Integration
{
    internal static class Configuration
    {
        public static string FTUserName { get; set; }
        public static string FTPassword { get; set; }        
        public static string FTConsumerUserName { get; set; }
        public static int FTAuthenticationTokenExpiry { get; set; }
        public static int SyncTimeInterval { get; set; }
        public static int CompareTimeInHour { get; set; }
        public static string FTLoginEndpoint { get; set; }
        public static string FTBaseUrl { get; set; }
        public static string FTCandidateEndpoint { get; set; }
        public static string FTParentCompanyEndpoint { get; set; }
        public static string FTClientEndpoint { get; set; }
        public static long FTDefaultOwnerUserId { get; set; }
        public static int FTDefaultSkillGroupId { get; set; }
        public static int FTDefaultOfficeId { get; set; }
        public static int FTDefaultSourceId { get; set; }
        public static string FTCostCentreEndpoint { get; set; }
        public static string FTTempJobEndPoint { get; set; }
        public static string FTJobTemplateEndPoint { get; set; }
        public static string FTApplyJobTemplateEndPoint { get; set; }
        public static string SMTP { get; set; }
        public static string CompareReportFromEmail { get; set; }
        public static string CompareReportToEmail { get; set; }
        /// <summary>
        /// Configuration constructor
        /// </summary>
        static Configuration()
        {
            FTUserName = GetConfigValueOrDefault("FTUserName", "");
            FTPassword = GetConfigValueOrDefault("FTPassword", "");            
            FTConsumerUserName = GetConfigValueOrDefault("FTConsumerUserName", "");
            FTAuthenticationTokenExpiry = GetConfigValueOrDefault("FTAuthenticationTokenExpiry", 24);
            SyncTimeInterval = GetConfigValueOrDefault("SyncTimeInterval", 180);
            CompareTimeInHour = GetConfigValueOrDefault("CompareTimeInHour", 23);
            FTLoginEndpoint = GetConfigValueOrDefault("FTLoginEndpoint", "login");
            FTBaseUrl = GetConfigValueOrDefault("FTBaseUrl", "");
            FTCandidateEndpoint = GetConfigValueOrDefault("FTCandidateEndpoint", "candidates");
            FTParentCompanyEndpoint = GetConfigValueOrDefault("FTParentCompanyEndpoint", "parentcompanies");
            FTClientEndpoint = GetConfigValueOrDefault("FTClientEndpoint", "clients");
            FTDefaultOwnerUserId = GetConfigValueOrDefault("FTDefaultOwnerUserId", 900000015);
            FTDefaultSkillGroupId = GetConfigValueOrDefault("FTDefaultSkillGroupId", 41);
            FTDefaultOfficeId = GetConfigValueOrDefault("FTDefaultOfficeId", 1);
            FTDefaultSourceId = GetConfigValueOrDefault("FTDefaultSourceId", 35);
            FTCostCentreEndpoint = GetConfigValueOrDefault("FTCostCentreEndpoint", "costcentres");
            FTTempJobEndPoint = GetConfigValueOrDefault("FTTempJobEndPoint", "tempjobs");
            FTJobTemplateEndPoint = GetConfigValueOrDefault("FTJobTemplateEndPoint", "jobtemplate/templatemethod");
            FTApplyJobTemplateEndPoint = GetConfigValueOrDefault("FTApplyJobTemplateEndPoint", "/tempJobOrder/");
            SMTP = GetConfigValueOrDefault("SMTP", "mail.iwf.com.au");
            CompareReportFromEmail = GetConfigValueOrDefault("CompareReportFromEmail", "");
            CompareReportToEmail = GetConfigValueOrDefault("CompareReportToEmail", "");
        }
        /// <summary>
        /// Get all the settings in string
        /// </summary>
        /// <returns></returns>
        internal new static string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("The following configuration values will be used:");
            sb.AppendFormat("\tFTUserName: {0} minutes\r\n", FTUserName);
            sb.AppendFormat("\tFTPassword: {0}\r\n", FTPassword);            
            sb.AppendFormat("\tFTConsumerUserName: {0}\r\n", FTConsumerUserName);
            sb.AppendFormat("\tFTAuthenticationTokenExpiry: {0}\r\n", FTAuthenticationTokenExpiry);
            sb.AppendFormat("\tSyncTimeInterval: {0}\r\n", SyncTimeInterval);
            sb.AppendFormat("\tFTLoginUrl: {0}\r\n", FTLoginEndpoint);
            sb.AppendFormat("\tFTCandidateUrl: {0}\r\n", FTCandidateEndpoint);
            sb.AppendFormat("\tFTParentCompanyUrl: {0}\r\n", FTParentCompanyEndpoint);
            sb.AppendFormat("\tFTClientUrl: {0}", FTClientEndpoint);

            return sb.ToString();
        }
        /// <summary>
        /// Get config values
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private static T GetConfigValueOrDefault<T>(string key, T defaultValue)
        {           
            var strConfigValue = System.Configuration.ConfigurationManager.AppSettings[key];

            if (!string.IsNullOrEmpty(strConfigValue))
            {
                return (T)Convert.ChangeType(strConfigValue, typeof(T));
            }

            return defaultValue;
        }
    }
}