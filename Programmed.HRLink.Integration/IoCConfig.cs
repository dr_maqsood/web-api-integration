﻿using Autofac;

namespace Programmed.HRLink.FT360Integration
{
    public static class IoCConfig
    {
        public static IContainer Configure()
        {
            // Setup container
            var builder = new ContainerBuilder();

            //builder.RegisterType<DatabaseContext>()
            //       .As<IDatabaseContext>()
            //       .InstancePerLifetimeScope();

            builder.RegisterType<SyncJobScheduler>()
                   .SingleInstance();

            builder.RegisterType<CompareJobScheduler>()
                .SingleInstance();

            return builder.Build();
        }
    }
}