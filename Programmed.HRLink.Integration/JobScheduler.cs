﻿using System;
using Quartz;
using Quartz.Impl;

namespace Programmed.HRLink.FT360Integration
{
    /// <summary>
    /// Simple Qaurtz job scheduler to trigger sync regularly.
    /// </summary>
    public class SyncJobScheduler : IDisposable
    {
        private readonly IScheduler _scheduler = StdSchedulerFactory.GetDefaultScheduler();

        public void Start<TJobType>() where TJobType : IJob
        {
            var trigger = TriggerBuilder.Create()
                                        .StartNow()
                                        .WithSimpleSchedule(x => x
                                            .WithIntervalInMinutes(Configuration.SyncTimeInterval)
                                            .RepeatForever())
                                        .Build();

            var job = JobBuilder.Create<TJobType>()
                                .Build();

            _scheduler.ScheduleJob(job, trigger);
            _scheduler.Start();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_scheduler != null && !_scheduler.IsShutdown)
                {
                    _scheduler.Shutdown();
                }
            }
        }
    }
}
