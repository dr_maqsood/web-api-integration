﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Common.Logging;
using log4net.Config;
using Nito.AsyncEx;
using Programmed.FT360.API.Endpoints.Candidate;
using Programmed.FT360.API.Endpoints.Client;
using Programmed.FT360.API.Endpoints.CostCentre;
using Programmed.FT360.API.Endpoints.ParentCompany;
using Programmed.FT360.API.Endpoints.TempJob;
using Programmed.FT360.API.Model.Client;
using Programmed.FT360.API.Model.ParentCompany;
using Programmed.FT360.API.Model.TempJob;
using Programmed.HRLink.FT360Integration.JobOrderProcess;
using Programmed.HRLink.Integration.Database.Services;
using Programmed.HRLink.Integration.Model.Api;
using Programmed.HRLink.Integration.Model.Database;
using Quartz;
using Topshelf;
using ContactDetails = Programmed.HRLink.Integration.Model.Api.ContactDetails;

namespace Programmed.HRLink.FT360Integration
{
    internal class Program
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(Program));
        private static readonly IContainer Container = IoCConfig.Configure();
        private static CandidateApiService _candidateApiService;
        private static ParentCompanyApiService _parentCompanyApiService;
        private static ClientApiService _clientApiService;
        private static CostCentreApiService _costCentreApiService;
        private static TempJobApiService _tempJobApiService;
        private static JobTemplateApiService _jobTemplateApiService;
        private static ApplyJobTemplateApiService _applyJobTemplateApiService;
        static void Main(string[] args)
        {
            XmlConfigurator.Configure();
            
            AppDomain.CurrentDomain.UnhandledException += (o, e) =>
            {
                Logger.Fatal("Unhandled exception occured.", (Exception)e.ExceptionObject);
                Container?.Dispose();
                // Close application and return error code
                Environment.Exit(2);
            };

            HostFactory.Run(configurator =>
            {
                configurator.StartAutomatically();


                // Set recovery policy
                configurator.EnableServiceRecovery(rc =>
                {
                    rc.RestartService(5); // restart the service after 5 minutes
                    rc.RestartService(5);
                    rc.RestartService(5);
                    rc.SetResetPeriod(1); // set the reset interval to one day
                });

                configurator.Service<SyncService>(service =>
                {
                    // Run processing
                    service.ConstructUsing(f => new SyncService());

                    service.WhenStarted(x =>
                    {
                        Logger.Info("Service started.");
                    });

                    service.WhenStopped(x =>
                    {
                        Logger.Info("Service stopped.");
                        // Cleanup
                        Container.Dispose();
                    });
                });

                configurator.SetDisplayName("Programmed HRLink FT360 Integration Service");
                configurator.SetDescription("This service integrates HRLink with Fast Track 360");
                configurator.SetServiceName("Programmed.HRLink.FT360Integration");
                configurator.UseLog4Net();

                _candidateApiService = new CandidateApiService(Configuration.FTConsumerUserName, Configuration.FTUserName, Configuration.FTPassword, Configuration.FTBaseUrl, Configuration.FTLoginEndpoint, Configuration.FTAuthenticationTokenExpiry, Configuration.FTCandidateEndpoint);
                _clientApiService = new ClientApiService(Configuration.FTConsumerUserName, Configuration.FTUserName, Configuration.FTPassword, Configuration.FTBaseUrl, Configuration.FTLoginEndpoint, Configuration.FTAuthenticationTokenExpiry, Configuration.FTClientEndpoint);
                _parentCompanyApiService = new ParentCompanyApiService(Configuration.FTConsumerUserName, Configuration.FTUserName, Configuration.FTPassword, Configuration.FTBaseUrl, Configuration.FTLoginEndpoint, Configuration.FTAuthenticationTokenExpiry, Configuration.FTParentCompanyEndpoint);
                _costCentreApiService = new CostCentreApiService(Configuration.FTConsumerUserName, Configuration.FTUserName, Configuration.FTPassword, Configuration.FTBaseUrl, Configuration.FTLoginEndpoint, Configuration.FTAuthenticationTokenExpiry, Configuration.FTCostCentreEndpoint);
                _tempJobApiService = new TempJobApiService(Configuration.FTConsumerUserName, Configuration.FTUserName, Configuration.FTPassword, Configuration.FTBaseUrl, Configuration.FTLoginEndpoint, Configuration.FTAuthenticationTokenExpiry, Configuration.FTTempJobEndPoint);
                _jobTemplateApiService = new JobTemplateApiService(Configuration.FTConsumerUserName, Configuration.FTUserName, Configuration.FTPassword, Configuration.FTBaseUrl, Configuration.FTLoginEndpoint, Configuration.FTAuthenticationTokenExpiry, Configuration.FTJobTemplateEndPoint);
                _applyJobTemplateApiService = new ApplyJobTemplateApiService(Configuration.FTConsumerUserName, Configuration.FTUserName, Configuration.FTPassword, Configuration.FTBaseUrl, Configuration.FTLoginEndpoint, Configuration.FTAuthenticationTokenExpiry, Configuration.FTApplyJobTemplateEndPoint);
            });
        }

        static async Task RunAsync()
        {
            var startTime = DateTime.Now;

            //Logger.Info("Started integration run.");
            //Logger.Info(Configuration.ToString());

            //Logger.Info("Processing HRLink...");

            try
            {                
                using (var cts = new CancellationTokenSource())
                {
                    // Setup max execution latency                
                    cts.CancelAfter(TimeSpan.FromMinutes(Configuration.SyncTimeInterval));

                    var token = cts.Token;
                    int i = 1;
                    var tasks = new Task[i];
                    while (i-- > 0)
                    {
                        tasks[i] = Task.Run(async () => await ProcessAsync(token), token);
                    }

                    await Task.WhenAll(tasks);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Fatal error occured inside processing loop.", ex);
            }

            //Logger.Info("Finished RunAsync processing.");
            //Logger.InfoFormat("Execution time: {0:g}", DateTime.Now - startTime);
        }

        static async Task RunCompareAsync()
        {
            var startTime = DateTime.Now;

            //Logger.Info("Started compare run.");
            //Logger.Info(Configuration.ToString());

            //Logger.Info("Comparing HRLink...");

            try
            {
                using (var cts = new CancellationTokenSource())
                {
                    // Setup max execution latency                
                    cts.CancelAfter(TimeSpan.FromMinutes(Configuration.CompareTimeInHour));

                    var token = cts.Token;
                    int i = 1;
                    var tasks = new Task[i];
                    while (i-- > 0)
                    {
                        tasks[i] = Task.Run(async () => await ProcessCompareAsync(token), token);
                    }

                    await Task.WhenAll(tasks);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Fatal error occured inside comparing loop.", ex);
            }

            //Logger.Info("Finished compare processing.");
            //Logger.InfoFormat("Execution time: {0:g}", DateTime.Now - startTime);
        }
        /// <summary>
        /// Compare data between HRLink and Fast Track
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private static async Task ProcessCompareAsync(CancellationToken token)
        {
            try
            {
                CompareService compareService = new CompareService();
             
                //Logger.Info("ProcessCompareAsync...");
                token.ThrowIfCancellationRequested();
                int counter = 0;
                var candiateList = await _candidateApiService.GetCandidates();
                foreach (var candidate in candiateList.data)
                {
                    Console.WriteLine($"{++counter}. CandidateId: {candidate.id} FirstName: {candidate.firstName} Surname:{candidate.surname} Email:{candidate.contactDetails.emailOne} Mobile: {candidate.contactDetails.mobilePhone} InactiveReasonId: {candidate.inactiveReason?.id}");
                }

                counter = 0;
                MemberService memService = new MemberService();
                var members = memService.GetLinkedActiveMembers();
                foreach (var member in members)
                {
                    Console.WriteLine($"HRLink {++counter}. MemberId: {member.MemberID} FirstName: {member.GivenName} Surname:{member.Surname}");
                }

                
                var differences = compareService.CompareMembers(candiateList.data, members);
                if (differences.Count > 0)
                {
                    //Logger.Error(String.Join(Environment.NewLine, differences));
                    compareService.SendEmail(Configuration.SMTP, Configuration.CompareReportFromEmail, Configuration.CompareReportToEmail, "Member data difference between HRLink & Fast Track 360", String.Join(Environment.NewLine, differences));
                }
                counter = 0;
                var parentCompanyList = await _parentCompanyApiService.GetParentCompanies();
                foreach (var parentCompany in parentCompanyList.data)
                {
                    Console.WriteLine($"{++counter}. ParentCompanyId: {parentCompany.id} Name: {parentCompany.name}");
                }

                counter = 0;
                ClientNetworkService clientNetworkService = new ClientNetworkService();
                var clientNetworks = clientNetworkService.GetLinkedClientNetworks();
                foreach (var clientNetwork in clientNetworks)
                {
                    Console.WriteLine($"HRLink {++counter}. ClientNetworkId: {clientNetwork.ClientNetworkID} Name: {clientNetwork.Name}");
                }

                differences = compareService.CompareClientNetworks(parentCompanyList.data, clientNetworks);
                if (differences.Count > 0)
                {
                    //Logger.Error(String.Join(Environment.NewLine, differences));
                    compareService.SendEmail(Configuration.SMTP, Configuration.CompareReportFromEmail, Configuration.CompareReportToEmail, "Client Network data difference between HRLink & Fast Track 360", String.Join(Environment.NewLine, differences));
                }

                counter = 0;
                var clientList = await _clientApiService.GetClients();
                foreach (var client in clientList.data)
                {
                    Console.WriteLine($"{++counter}. ClientId: {client.id} Name: {client.name}");
                }

                counter = 0;
                ClientService clientService = new ClientService();
                var clients = clientService.GetLinkedActiveClients();
                foreach (var client in clients)
                {
                    Console.WriteLine($"HRLink {++counter}. ClientId: {client.ClientId} Name: {client.Name}");
                }

                differences = compareService.CompareClients(clientList.data, clients);
                if (differences.Count > 0)
                {
                    //Logger.Error(String.Join(Environment.NewLine, differences));
                    compareService.SendEmail(Configuration.SMTP, Configuration.CompareReportFromEmail, Configuration.CompareReportToEmail, "Client data difference between HRLink & Fast Track 360", String.Join(Environment.NewLine, differences));
                }
                counter = 0;
                var costCentreList = await _costCentreApiService.GetCostCentres();
                foreach (var costCentre in costCentreList.data)
                {
                    Console.WriteLine($"{++counter}. CostCentreId: {costCentre.id} Name: {costCentre.name}");
                }

                counter = 0;
                CostCentreService costCentreService = new CostCentreService();
                var costCentres = costCentreService.GetLinkedActiveCostCentres();
                foreach (var costCentre in costCentres)
                {
                    Console.WriteLine($"HRLink {++counter}. CostCenreId: {costCentre.CostCentreId} Name: {costCentre.ClientCostCentre.Name}");
                }

                differences = compareService.CompareCostCenters(costCentreList.data, costCentres);
                if (differences.Count > 0)
                {
                    //Logger.Error(String.Join(Environment.NewLine, differences));
                    compareService.SendEmail(Configuration.SMTP, Configuration.CompareReportFromEmail, Configuration.CompareReportToEmail, "Cost Centre data difference between HRLink & Fast Track 360", String.Join(Environment.NewLine, differences));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Logger.Error(ex);
            }
            //Logger.Info("ProcessCompareAsync cycle completed");
        }
        /// <summary>
        /// Synch data between HRLink and Fast Track
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private static async Task ProcessAsync(CancellationToken token)
        {
            try
            {
                //Logger.Info("Processing ProcessAsync...");
                token.ThrowIfCancellationRequested();
                //await ProcessCompareAsync(token);
                
                await SyncCandidates();
                await SyncClientNetworks();
                await SyncClients();
                await SyncCostCentres();

                #region Temp Jobs Updates from HRLink to FasTrack

                //var jobOrder = new JobOrder();
                //var updateResponses = await jobOrder.ProcessJobOrders(3000, _tempJobApiService, _jobTemplateApiService, _applyJobTemplateApiService);
                //foreach (var updateResponse in updateResponses)
                //{
                //    if (updateResponse.JobId == 0)
                //        Console.WriteLine($"ShiftId: {updateResponse.ShiftId} JobOrderId: {updateResponse.JobId} Error: {updateResponse.ErrorMessage}");
                //}


                #endregion
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Logger.Error(ex);
            }
            //Logger.Info("ProcessAsync cycle completed");
            //using (var scope = Container.BeginLifetimeScope())
            //{

            //}
            Console.WriteLine($"{DateTime.Now:HH:mm:ss} Completed cycle!!!\r\n");
        }
        /// <summary>
        /// Update Candidates in Fast Track and Members in HRLink
        /// </summary>
        /// <returns></returns>
        private static async Task SyncCandidates()
        {
            try
            {
                int counter = 0;

                #region Candidates Updates From FastTrack to HRLink
                Console.WriteLine($"\r\n{DateTime.Now:HH:mm:ss} Getting all candidates from FastTrack\r\n");
                var candiateList = await _candidateApiService.GetCandidates();
                if (candiateList.data != null)
                {
                    foreach (var candidate in candiateList.data)
                    {                        
                        Console.WriteLine($"{++counter}. CandidateId: {candidate.id} FirstName: {candidate.firstName} Surname:{candidate.surname} Email:{candidate.contactDetails.emailOne} Mobile: {candidate.contactDetails.mobilePhone} InactiveReasonId: {candidate.inactiveReason?.id}");
                    }

                    Console.WriteLine($"{DateTime.Now:HH:mm:ss} Update candidates to HRLink");
                    MemberService memberService = new MemberService();
                    memberService.UpdateMembers(candiateList);
                    Console.WriteLine($"{DateTime.Now:HH:mm:ss} Done!!!\r\n");

                    #endregion

                #region Candidates Updates from HRLink to FastTrack

                    Console.WriteLine($"{DateTime.Now:HH:mm:ss} Candidate Updates from HRLink to FastTrack");
                    var members = memberService.GetMembersToBeSent();
                    foreach (var member in members)
                    {
                        #region Create Fast Track Candidate Object

                        var candAddEdit = new CandidateAddEdit
                        {
                            alternateNumberOne = member.MemberID.ToString(),
                            salutationId = Integration.Common.GetSalutationId(member.Title),
                            firstName = member.GivenName.Trim().Split(' ').FirstOrDefault(), //get first word from given name
                            surname = member.Surname,
                            genderId = Integration.Common.GetFTGenderId(member.Sex),
                            skillGroupId = member.FTSkillGroupId ?? Configuration.FTDefaultSkillGroupId,
                            officeId = member.FTOfficeId ?? Configuration.FTDefaultOfficeId,
                            sourceId = member.FTSourceId?? Configuration.FTDefaultSourceId,
                            dob = member.BirthDate,
                            contactDetails = new ContactDetails
                            {
                                emailOne = member.MemberInteractionMethods.FirstOrDefault(x => x.InteractionMethodID == Integration.Common.Constants.MemberInteractionMethod.Email)?.SendTo,
                                mobilePhone = member.MemberInteractionMethods.FirstOrDefault(x => x.InteractionMethodID == Integration.Common.Constants.MemberInteractionMethod.Mobile && x.SendTo.StartsWith("(04"))?.SendTo,
                                phoneAfterHours = member.MemberInteractionMethods.FirstOrDefault(x => x.InteractionMethodID == Integration.Common.Constants.MemberInteractionMethod.Mobile && !x.SendTo.StartsWith("(04"))?.SendTo,
                            },
                            mainAddress = new CandidateAddEdit.MainAddress
                            {
                                addressLineOne =$"{member.MemberContact?.Street1No} {member.MemberContact?.Street1} {member.MemberContact?.Abbrev1}".Trim(),
                                addressLineTwo = $"{member.MemberContact?.Street2No} {member.MemberContact?.Street2} {member.MemberContact?.Abbrev2}".Trim(),
                                postCode = member.MemberContact?.Postcode,
                                suburb = member.MemberContact?.Suburb,
                                stateId = Integration.Common.GetFTStateId(member.MemberContact?.State.Trim()),
                                countryId = Integration.Common.Constants.Country.Australia
                            },
                            mailingAddress = new CandidateAddEdit.MainAddress
                            {
                                addressLineOne = member.MemberContact?.PostalStreet1,
                                addressLineTwo = member.MemberContact?.PostalStreet2,
                                postCode = member.MemberContact?.PostalPostcode,
                                suburb = member.MemberContact?.PostalSuburb,
                                stateId = Integration.Common.GetFTStateId(member.MemberContact?.PostalState.Trim()),
                                countryId = Integration.Common.Constants.Country.Australia
                            },
                            ownerUserId = member.FTOwnerId ?? Configuration.FTDefaultOwnerUserId,
                            statusId = Integration.Common.GetCandidateStatus(member.MemberAgencies.FirstOrDefault(x => x.AgencyID == Integration.Common.Constants.Agency.Php)?.EnabledStatus),
                        };

                        //if(member.LocationID ==)
                        #endregion

                        if (candAddEdit.statusId == Integration.Common.Constants.CandidateStatus.Inactive)
                        {
                            candAddEdit.inactiveReasonId = Integration.Common.Constants.InactiveReason.CandidateInactiveReasonId; //No Longer Trading

                            //set candidate type to qurantine if member is qurarantine
                            var memberQuarantine = member.MemberAgencies.FirstOrDefault(x => x.AgencyID == Integration.Common.Constants.Agency.Php)?.QuarantineStatus;
                            if (memberQuarantine.HasValue && memberQuarantine.Value)
                                candAddEdit.typeId = Integration.Common.Constants.CandidateType.Quarantine;
                        }

                        var updateResponse = await _candidateApiService.UpdateCandidate(member.FTCandidateId, candAddEdit);

                        if (updateResponse.Id > 0 && updateResponse.Id != member.FTCandidateId) //transaction was sucessfull so update FTCandidateId in HRLink
                        {
                            member.FTCandidateId = updateResponse.Id;
                            //memberService.UpdateMemberCandidateId(member);
                        }

                        #region Update FT Status
                        //update transaction status in HRLink
                        member.FTStatus = updateResponse.Id > 0 ? Integration.Common.Constants.FTStatus.Sent : Integration.Common.Constants.FTStatus.Error;
                        member.FTErrorDescription = updateResponse.ErrorMessage;
                        memberService.UpdateMemberFTStatus(member);

                        #endregion
                    }

                    Console.WriteLine($"{DateTime.Now:HH:mm:ss} Done!!!\r\n");
                }

                #endregion
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Logger.Error(ex);
            }
        }
        /// <summary>
        /// Update Client Networks in Fast Track
        /// </summary>
        /// <returns></returns>
        private static async Task SyncClientNetworks()
        {
            try
            {
                #region Parent Companies Updates from HRLink to FastTrack

                ClientNetworkService clientNetworkService = new ClientNetworkService();
                Console.WriteLine($"{DateTime.Now:HH:mm:ss} Parent Companies Updates from HRLink to FastTrack");
                var clientNetworks = clientNetworkService.GetClientNetworksToBeSent();
                foreach (var clientNetwork in clientNetworks)
                {
                    ParentCompanyAddEdit parentCompanyAddEdit = new ParentCompanyAddEdit()
                    {
                        alternateNumber = clientNetwork.ClientNetworkID.ToString(),
                        name = clientNetwork.Name,
                        officeId = clientNetwork.FTOfficeId ?? Configuration.FTDefaultOfficeId,
                        ownerUserId = clientNetwork.FTOwnerId ?? Configuration.FTDefaultOwnerUserId,
                        statusId = clientNetwork.FTStatusId ?? Integration.Common.Constants.FTParentCompanyStatus.Active,
                        mainAddress = new FT360.API.Model.ParentCompany.MainAddress(),
                        mailingAddress = new FT360.API.Model.ParentCompany.MailingAddress(),
                    };
                    if (parentCompanyAddEdit.statusId == Integration.Common.Constants.FTParentCompanyStatus.Inactive)
                        parentCompanyAddEdit.inactiveReasonId = Integration.Common.Constants.InactiveReason.ParentCompanyInactiveReasonId;//No Longer Trading           

                    var updateResponse = await _parentCompanyApiService.UpdateParentCompany(clientNetwork.FTParentId, parentCompanyAddEdit);

                    if (updateResponse.Id > 0 && updateResponse.Id != clientNetwork.FTParentId)//transaction was sucessfull so update FTParentId in HRLink
                    {
                        clientNetwork.FTParentId = updateResponse.Id;
                        //clientNetworkService.UpdateClietnNetworkFTParentId(clientNetwork);
                    }

                    #region Update FT Status
                    //update transaction status in HRLink
                    clientNetwork.FTStatus = updateResponse.Id > 0 ? Integration.Common.Constants.FTStatus.Sent : Integration.Common.Constants.FTStatus.Error;
                    clientNetwork.FTErrorDescription = updateResponse.ErrorMessage;
                    clientNetworkService.UpdateClientNetworkFTStatus(clientNetwork);
                    #endregion
                }
                Console.WriteLine($"{DateTime.Now:HH:mm:ss} Done!!!\r\n");
                #endregion
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Logger.Error(ex);
            }
        }
        /// <summary>
        /// Update Clients in Fast Track
        /// </summary>
        /// <returns></returns>
        private static async Task SyncClients()
        {
            try
            {
                #region Clients Updates From HRLink to FastTrack 
                Console.WriteLine($"{DateTime.Now:HH:mm:ss} Clients Updates From HRLink to FastTrack");
                ClientNetworkService clientNetworkService = new ClientNetworkService();
                ClientService clientService = new ClientService();
                var hrClients = clientService.GetClientsToBeSent();
                foreach (var hrClient in hrClients)
                {
                    ClientAddEdit clientAddEdit = new ClientAddEdit()
                    {
                        name = hrClient.Name,
                        alternateNumber = hrClient.ClientId.ToString(),
                        ownerUserId = hrClient.FTOwnerId ?? Configuration.FTDefaultOwnerUserId,
                        officeId = hrClient.FTOfficeId ?? Configuration.FTDefaultOfficeId,
                        statusId = Integration.Common.GetFTClientStatus(hrClient.EnabledStatus),
                        tradeRefChecked = true,
                        termsConditions = true,
                        //corporateIdentityNumber = hrClient.ClientFinance?.ABN,
                        workCoverCodeId = Integration.Common.GetFTWorkCoverCodeId(hrClient.LocationId),
                        workCoverStateId = Integration.Common.GetFTStateId(hrClient.LocationId),
                        payrollTaxStateId = Integration.Common.GetFTStateId(hrClient.LocationId),
                        mailingAddress = new ClientAddEdit.MailingAddress
                        {
                            addressLineOne = hrClient.ClientFinance?.PostalStreet1,
                            addressLineTwo = hrClient.ClientFinance?.PostalStreet2,
                            suburb = hrClient.ClientFinance?.PostalSuburb,
                            postCode = hrClient.ClientFinance?.PostalPostcode,
                            stateId = Integration.Common.GetFTStateId(hrClient.ClientFinance?.PostalState),
                            countryId = Integration.Common.Constants.Country.Australia
                        },
                        mainAddress = new ClientAddEdit.MainAddress
                        {
                            addressLineOne = hrClient.ClientFinance?.PostalStreet1,
                            addressLineTwo = hrClient.ClientFinance?.PostalStreet2,
                            suburb = hrClient.ClientFinance?.PostalSuburb,
                            postCode = hrClient.ClientFinance?.PostalPostcode,
                            stateId = Integration.Common.GetFTStateId(hrClient.ClientFinance?.PostalState),
                            countryId = Integration.Common.Constants.Country.Australia
                        },
                        parentId = clientNetworkService.GetClientNetworkParentId(hrClient.ClientNetworkID)
                    };
                    if (clientAddEdit.statusId == Integration.Common.Constants.ClientStatus.Inactive)
                        clientAddEdit.inactiveReasonId = Integration.Common.Constants.InactiveReason.ClientInactiveReasonId;
                    var updateResponse = await _clientApiService.UpdateClient(hrClient.FTClientId, clientAddEdit);

                    if (updateResponse.Id > 0 && updateResponse.Id != hrClient.FTClientId)//transaction was sucessfull so update FTClientId in HRLink
                    {
                        hrClient.FTClientId = updateResponse.Id;
                        //clientService.UpdateFTClientId(hrClient);
                    }

                    #region Update FT Status
                    //update transaction status in HRLink
                    hrClient.FTStatus = updateResponse.Id > 0 ? Integration.Common.Constants.FTStatus.Sent : Integration.Common.Constants.FTStatus.Error;
                    hrClient.FTErrorDescription = updateResponse.ErrorMessage;
                    clientService.UpdateClientFTStatus(hrClient);
                    #endregion
                }
                Console.WriteLine($"{DateTime.Now:HH:mm:ss} Done!!!\r\n");
                #endregion
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Logger.Error(ex);
                throw;
            }
        }
        /// <summary>
        /// Update Cost Centres in Fast Track
        /// </summary>
        /// <returns></returns>
        private static async Task SyncCostCentres()
        {
            try
            {
                #region Cost Centre Updates from HRLink to FastTrack
                Console.WriteLine($"{DateTime.Now:HH:mm:ss} Cost Centre Updates from HRLink to FastTrack");
                var costCentreService = new CostCentreService();
                var costCentreList = costCentreService.GetCostCentresToBeSent();
                foreach (var costCentre in costCentreList)
                {
                    #region Create CostCentre Fast Track Object
                    var costCentreAddEdit = new CostCentreAddEdit
                    {
                        alternateNumber = costCentre.CostCentreId.ToString(),
                        name = costCentre.ClientCampusWard?.Name,//department name
                        code = costCentre.ClientCostCentre.Name,//cost centre name
                        officeId = costCentre.FTOfficeId ?? Configuration.FTDefaultOfficeId,
                        mainAddress = new CostCentreAddEdit.MainAddress
                        {
                            addressLineOne = costCentre.ClientCampusWard?.ClientCampus?.Street1,
                            addressLineTwo = costCentre.ClientCampusWard?.ClientCampus?.Street2,
                            postCode = costCentre.ClientCampusWard?.ClientCampus?.Postcode,
                            suburb = costCentre.ClientCampusWard?.ClientCampus?.Suburb,
                            stateId = Integration.Common.GetFTStateId(costCentre.ClientCampusWard?.ClientCampus?.State),
                            countryId = Integration.Common.Constants.Country.Australia
                        },
                        mailingAddress = new CostCentreAddEdit.MainAddress
                        {
                            addressLineOne = costCentre.ClientCostCentre.Client.ClientFinance?.PostalStreet1,
                            addressLineTwo = costCentre.ClientCostCentre.Client.ClientFinance?.PostalStreet2,
                            postCode = costCentre.ClientCostCentre.Client.ClientFinance?.PostalPostcode,
                            suburb = costCentre.ClientCostCentre.Client.ClientFinance?.PostalSuburb,
                            stateId = Integration.Common.GetFTStateId(costCentre.ClientCostCentre.Client.ClientFinance?.PostalState),
                            countryId = Integration.Common.Constants.Country.Australia
                        },
                        ownerUserId = costCentre.FTOwnerId ?? Configuration.FTDefaultOwnerUserId,
                        statusId = Integration.Common.GetCostCentreStatus(costCentre.ClientCampusWard?.EnabledStatus),
                        clientid = costCentre.ClientCostCentre.Client.FTClientId ?? 0
                    };
                    #endregion

                    if (costCentreAddEdit.statusId == Integration.Common.Constants.CostCentreStatus.Inactive)
                        costCentreAddEdit.inactiveReasonId = Integration.Common.Constants.InactiveReason.CostCentreInactiveReasonId;//No Longer Trading                    

                    UpdateResponse updateResponse = await _costCentreApiService.UpdateCostCentre(costCentre.FTCostCentreId, costCentreAddEdit);

                    if (updateResponse.Id > 0 && updateResponse.Id != costCentre.FTCostCentreId)//transaction was sucessfull so update FTCostCentreId in HRLink
                    {
                        costCentre.FTCostCentreId = updateResponse.Id;
                        //costCentreService.UpdateFTCostCentreId(costCentre);
                    }

                    #region Update FT Status
                    //update transaction status in HRLink
                    costCentre.FTStatus = updateResponse.Id > 0 ? Integration.Common.Constants.FTStatus.Sent : Integration.Common.Constants.FTStatus.Error;
                    costCentre.FTErrorDescription = updateResponse.ErrorMessage;
                    costCentreService.UpdateCostCentreFTStatus(costCentre);
                    #endregion
                }
                Console.WriteLine($"{DateTime.Now:HH:mm:ss} Done!!!\r\n");
                #endregion
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Logger.Error(ex);
            }
        }


        #region Job Related Tasks
        /// <summary>
        /// Sync job for scheduler.
        /// </summary>
        [DisallowConcurrentExecution]
        internal class HRLinkSyncJob : IJob
        {
            public void Execute(IJobExecutionContext context)
            {
                //Logger.Info("Started sync job.");

                AsyncContext.Run(async () =>
                {
                    try
                    {
                        await RunAsync();
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex);
                    }
                });

                //Logger.Info("Finished sync job.");
            }
        }
        /// <summary>
        /// Compare job for scheduler.
        /// </summary>
        [DisallowConcurrentExecution]
        internal class HRLinkCompareJob : IJob
        {
            public void Execute(IJobExecutionContext context)
            {
                Logger.Info("Started compare job.");

                AsyncContext.Run(async () =>
                {
                    try
                    {
                        await RunCompareAsync();
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex);
                    }
                });

                Logger.Info("Finished compare job.");
            }
        }

        /// <summary>
        /// Windows service wrapper over job scheduler.
        /// </summary>
        internal class SyncService
        {
            public SyncService()
            {
                // Creates scheduler and starts export jobs. Scheduler is disposed by container.
                var jobScheduler = Container.Resolve<SyncJobScheduler>();
                jobScheduler.Start<HRLinkSyncJob>();

                var compareJobScheduler = Container.Resolve<CompareJobScheduler>();
                compareJobScheduler.Start<HRLinkCompareJob>();
            }
        }
        #endregion
    }
}
