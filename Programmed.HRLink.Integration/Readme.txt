﻿1. Please use --o option to view supported parameters
2. To use as a service (see http://docs.topshelf-project.com/en/latest/overview/commandline.html):
	- To install service: Programmed.HRLink.FT360Integration.exe install [-username:[user] -password:[password]] [options(see 1)]
	- To start service: net start Programmed.HRLink.FT360Integration
	- To start service: net stop Programmed.HRLink.FT360Integration
	- To uninstall service Programmed.HRLink.FT360Integration.exe uninstall