﻿USE HRLink

ALTER TABLE [dbo].[Member] ADD
	[FTCandidateId] [bigint] NULL,
	[FTStatus] [smallint] NULL default(10),
	[FTOwnerId] [bigint] NULL,
	[FTSkillGroupId] [int] NULL,
	[FTOfficeId] [int] NULL,	
	FTErrorDescription varchar(MAX) NULL,
	FTSourceId int NULL
GO


ALTER TABLE dbo.ClientNetwork ADD
	FTOfficeId int NULL,
	FTParentId bigint NULL,
	FTOwnerId bigint NULL,
	FTStatusId int NULL,
	FTStatus smallint NULL default(10),	
	FTErrorDescription varchar(MAX) NULL
GO

ALTER TABLE dbo.Client ADD
	FTClientId bigint NULL,
	FTOwnerId bigint NULL,
	FTOfficeId int NULL,
	FTStatus smallint NULL default(10),	
	FTErrorDescription varchar(MAX) NULL
GO

--To change primary key data type, we need to drop the constraint first, change the data type and then add the constraint again
ALTER TABLE ClientFinance DROP CONSTRAINT PKClientFinance 
GO

ALTER TABLE ClientFinance
ALTER COLUMN ClientID smallint NOT NULL
GO

ALTER TABLE [dbo].[ClientFinance] ADD PRIMARY KEY CLUSTERED 
(
	[ClientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO

ALTER TABLE dbo.WardCostCentreDate ADD
	FTCostCentreId bigint NULL,
	FTOfficeId int NULL,
	FTOwnerId bigint NULL,
	FTStatus smallint NULL default(10),
	FTErrorDescription varchar(MAX) NULL
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This insert trigger will change the Member.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if new member is created by other than integration service
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Member_INSERT]
       ON [dbo].[Member]
AFTER INSERT
AS
BEGIN
	IF App_Name() <> 'Programmed.HRLink.FT360Integration' -- we want to change the status if update made by other than integration service
	BEGIN
       SET NOCOUNT ON;
 
       DECLARE @MemberId INT
 
       SELECT @MemberId = INSERTED.MemberID
       FROM INSERTED
 
	   UPDATE Member SET FTStatus=10, FTErrorDescription=NULL WHERE MemberID = @MemberId      
	END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This update trigger will change the Member.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if any integraton fields are changed by other than integration service
-- =============================================
CREATE TRIGGER [dbo].[Member_UPDATE]
       ON [dbo].[Member]
AFTER UPDATE
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @MemberId INT	   
 
		   SELECT @MemberId = INSERTED.MemberID FROM INSERTED

		   IF UPDATE(FTCandidateId) OR UPDATE(Dear) OR UPDATE(GivenName) OR UPDATE(Surname) OR UPDATE(Title) OR UPDATE(LocationID) OR UPDATE(BirthDate)
		   BEGIN
			   UPDATE Member SET FTStatus=10, FTErrorDescription=NULL WHERE MemberID = @MemberId      
		   END		   
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This insert trigger will change the Member.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if new MemeberInteractionMethod is created by other than than integration service
-- =============================================
CREATE TRIGGER [dbo].[MemberInteractionMethod_INSERT]
       ON [dbo].[MemberInteractionMethod]
AFTER INSERT
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @MemberId INT	   
 
		   SELECT @MemberId = INSERTED.MemberID FROM INSERTED		   
			
		   UPDATE Member SET FTStatus=10, FTErrorDescription=NULL WHERE MemberID = @MemberId
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This insert trigger will change the Member.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if new MemberAgency is created by other than than integration service
-- =============================================
CREATE TRIGGER [dbo].[MemberAgency_INSERT]
       ON [dbo].[MemberAgency]
AFTER INSERT
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @MemberId INT	   
 
		   SELECT @MemberId = INSERTED.MemberID FROM INSERTED		   
			
		   UPDATE Member SET FTStatus=10, FTErrorDescription=NULL WHERE MemberID = @MemberId
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This update trigger will change the Member.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if any integraton fields are changed by other than integration service
-- =============================================
CREATE TRIGGER [dbo].[MemberAgency_UPDATE]
       ON [dbo].[MemberAgency]
AFTER UPDATE
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @MemberId INT	   
 
		   SELECT @MemberId = INSERTED.MemberID FROM INSERTED

		   IF UPDATE(EnabledStatus) OR UPDATE(AgencyID) OR UPDATE(QuarantineStatus)
		   BEGIN
			   UPDATE Member SET FTStatus=10, FTErrorDescription=NULL WHERE MemberID = @MemberId      
		   END		   
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This insert trigger will change the Member.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if new MemberContact is created by other than than integration service
-- =============================================
CREATE TRIGGER [dbo].[MemberContact_INSERT]
       ON [dbo].[MemberContact]
AFTER INSERT
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @MemberId INT	   
 
		   SELECT @MemberId = INSERTED.MemberID FROM INSERTED		   
			
		   UPDATE Member SET FTStatus=10, FTErrorDescription=NULL WHERE MemberID = @MemberId
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This update trigger will change the Member.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if any integraton fields are changed by other than integration service
-- =============================================
CREATE TRIGGER [dbo].[MemberContact_UPDATE]
       ON [dbo].[MemberContact]
AFTER UPDATE
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @MemberId INT	   
 
		   SELECT @MemberId = INSERTED.MemberID FROM INSERTED

		   IF UPDATE(Street1) OR UPDATE(Street2) OR UPDATE(Suburb) OR UPDATE(State) OR UPDATE(Postcode) OR UPDATE(PostalSuburb) OR UPDATE (PostalPostcode) OR 
			  UPDATE(PostalStreet1) OR UPDATE(PostalStreet2) OR UPDATE(PostalState) OR UPDATE(PostalCountry)
		   BEGIN
			   UPDATE Member SET FTStatus=10, FTErrorDescription=NULL WHERE MemberID = @MemberId      
		   END		   
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This insert trigger will change the Client.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if new Client is created by other than than integration service
-- =============================================
CREATE TRIGGER [dbo].[Client_INSERT]
       ON [dbo].[Client]
AFTER INSERT
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @ClientId INT	   
 
		   SELECT @ClientId = INSERTED.ClientId FROM INSERTED		   
			
		   UPDATE Client SET FTStatus=10, FTErrorDescription=NULL WHERE ClientID = @ClientId

		   --Update to trigger Cost Centre address change
		   UPDATE WardCostCentreDate SET FTStatus=10, FTErrorDescription=NULL WHERE WardId IN (SELECT WardId FROM ClientCampusWard WHERE CampusId IN (SELECT CampusId FROM ClientCampus WHERE ClientId = @ClientId))
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This update trigger will change the Client.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if any integraton fields are changed by other than integration service
-- =============================================
CREATE TRIGGER [dbo].[Client_UPDATE]
       ON [dbo].[Client]
AFTER UPDATE
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @ClientId INT	   
 
		   SELECT @ClientId = INSERTED.ClientId FROM INSERTED
		   
		   IF UPDATE(FTClientId) OR UPDATE(ClientType) OR UPDATE(Name) OR UPDATE(EnabledStatus) OR UPDATE(ClientNetworkID) OR UPDATE(PrimaryAgencyID) OR UPDATE (LocationId)
		   BEGIN
			   UPDATE Client Set FTStatus=10, FTErrorDescription=NULL WHERE ClientId = @ClientId

			   --Update to trigger Cost Centre address change
		       UPDATE WardCostCentreDate SET FTStatus=10, FTErrorDescription=NULL WHERE WardId IN (SELECT WardId FROM ClientCampusWard WHERE CampusId IN (SELECT CampusId FROM ClientCampus WHERE ClientId = @ClientId))
		   END		   
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This insert trigger will change the Client.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if new ClientAgency is created by other than than integration service
-- =============================================
CREATE TRIGGER [dbo].[ClientAgency_INSERT]
       ON [dbo].[ClientAgency]
AFTER INSERT
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @ClientId INT	   
 
		   SELECT @ClientId = INSERTED.ClientId FROM INSERTED		   
			
		   UPDATE Client SET FTStatus=10, FTErrorDescription=NULL WHERE ClientID = @ClientId
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This update trigger will change the Client.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if any integraton fields are changed by other than integration service
-- =============================================
CREATE TRIGGER [dbo].[ClientAgency_UPDATE]
       ON [dbo].[ClientAgency]
AFTER UPDATE
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @ClientId INT	   
 
		   SELECT @ClientId = INSERTED.ClientId FROM INSERTED
		   
		   IF UPDATE(AgencyID) OR UPDATE(EnabledStatus)
		   BEGIN
			   UPDATE Client Set FTStatus=10, FTErrorDescription=NULL WHERE ClientId = @ClientId      
		   END		   
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This insert trigger will change the Client.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if new ClientFinance is created by other than than integration service
-- =============================================
CREATE TRIGGER [dbo].[ClientFinance_INSERT]
       ON [dbo].[ClientFinance]
AFTER INSERT
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @ClientId INT	   
 
		   SELECT @ClientId = INSERTED.ClientId FROM INSERTED		   
			
		   UPDATE Client SET FTStatus=10, FTErrorDescription=NULL WHERE ClientID = @ClientId

		   --Update to trigger Cost Centre address change
		   UPDATE WardCostCentreDate SET FTStatus=10, FTErrorDescription=NULL WHERE WardId IN (SELECT WardId FROM ClientCampusWard WHERE CampusId IN (SELECT CampusId FROM ClientCampus WHERE ClientId = @ClientId))
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This update trigger will change the Client.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if any integraton fields are changed by other than integration service
-- =============================================
CREATE TRIGGER [dbo].[ClientFinance_UPDATE]
       ON [dbo].[ClientFinance]
AFTER UPDATE
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @ClientId INT	   
 
		   SELECT @ClientId = INSERTED.ClientId FROM INSERTED
		   
		   IF UPDATE(PostalSuburb) OR UPDATE(PostalPostcode) OR UPDATE(PostalStreet1) OR UPDATE(PostalStreet2) OR UPDATE(PostalState)
		   BEGIN
			   UPDATE Client Set FTStatus=10, FTErrorDescription=NULL WHERE ClientId = @ClientId      

			   --Update to trigger Cost Centre address change
		       UPDATE WardCostCentreDate SET FTStatus=10, FTErrorDescription=NULL WHERE WardId IN (SELECT WardId FROM ClientCampusWard WHERE CampusId IN (SELECT CampusId FROM ClientCampus WHERE ClientId = @ClientId))
		   END		   
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This insert trigger will change the Client.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if new ClientNetwork is created by other than than integration service
-- =============================================
CREATE TRIGGER [dbo].[ClientNetwork_INSERT]
       ON [dbo].[ClientNetwork]
AFTER INSERT
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @ClientNetworkID INT	   
 
		   SELECT @ClientNetworkID = INSERTED.ClientNetworkID FROM INSERTED		   
			
		   UPDATE Client SET FTStatus=10, FTErrorDescription=NULL WHERE ClientNetworkID = @ClientNetworkID
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This update trigger will change the Client.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if any integraton fields are changed by other than integration service
-- =============================================
CREATE TRIGGER [dbo].[ClientNetwork_UPDATE]
       ON [dbo].[ClientNetwork]
AFTER UPDATE
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @ClientNetworkId INT	   
 
		   SELECT @ClientNetworkId = INSERTED.ClientNetworkId FROM INSERTED
		   
		   IF UPDATE(Name) OR UPDATE(LocationId) OR UPDATE(FTStatusId)
		   BEGIN
			   UPDATE Client Set FTStatus=10, FTErrorDescription=NULL WHERE ClientNetworkId = @ClientNetworkId     
		   END		   
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This insert trigger will change the WardCostCentreDate.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if new WardCostCentreDate is created by other than than integration service
-- =============================================
CREATE TRIGGER [dbo].[WardCostCentreDate_INSERT]
       ON [dbo].[WardCostCentreDate]
AFTER INSERT
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @WardId INT	   
		   DECLARE @EffectiveStartDate SMALLDATETIME
 
		   SELECT @WardId = INSERTED.WardId, @EffectiveStartDate=INSERTED.EffectiveStartDate FROM INSERTED
			
		   UPDATE WardCostCentreDate SET FTStatus=10, FTErrorDescription=NULL WHERE WardId = @WardId AND EffectiveStartDate = @EffectiveStartDate
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This update trigger will change the WardCostCentreDate.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if any integraton fields are changed by other than integration service
-- =============================================
CREATE TRIGGER [dbo].[WardCostCentreDate_UPDATE]
       ON [dbo].[WardCostCentreDate]
AFTER UPDATE
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @WardId INT	   
		   DECLARE @EffectiveStartDate SMALLDATETIME
 
		   SELECT @WardId = INSERTED.WardId, @EffectiveStartDate=INSERTED.EffectiveStartDate FROM INSERTED
		   
		   IF UPDATE(WardId) OR UPDATE(EffectiveStartDate)
		   BEGIN
			   UPDATE WardCostCentreDate SET FTStatus=10, FTErrorDescription=NULL WHERE WardId = @WardId AND EffectiveStartDate = @EffectiveStartDate
		   END		   
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This insert trigger will change the WardCostCentreDate.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if new ClientCostCentre is created by other than than integration service
-- =============================================
CREATE TRIGGER [dbo].[ClientCostCentre_INSERT]
       ON [dbo].[ClientCostCentre]
AFTER INSERT
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @CostCentreId INT	   		   
 
		   SELECT @CostCentreId = INSERTED.CostCentreId FROM INSERTED
			
		   UPDATE WardCostCentreDate SET FTStatus=10, FTErrorDescription=NULL WHERE CostCentreId = @CostCentreId
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This update trigger will change the WardCostCentreDate.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if any integraton fields are changed by other than integration service
-- =============================================
CREATE TRIGGER [dbo].[ClientCostCentre_UPDATE]
       ON [dbo].[ClientCostCentre]
AFTER UPDATE
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		  SET NOCOUNT ON;	   
		  DECLARE @CostCentreId INT	   		   
 
		  SELECT @CostCentreId = INSERTED.CostCentreId FROM INSERTED
		   
		  IF UPDATE(Name) OR UPDATE(Description)
		  BEGIN
			  UPDATE WardCostCentreDate SET FTStatus=10, FTErrorDescription=NULL WHERE CostCentreId = @CostCentreId
		  END		   
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This insert trigger will change the WardCostCentreDate.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if new ClientCampusWard is created by other than than integration service
-- =============================================
CREATE TRIGGER [dbo].[ClientCampusWard_INSERT]
       ON [dbo].[ClientCampusWard]
AFTER INSERT
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		   SET NOCOUNT ON;	   
		   DECLARE @WardId INT	   		   
 
		   SELECT @WardId = INSERTED.WardId FROM INSERTED
			
		   UPDATE WardCostCentreDate SET FTStatus=10, FTErrorDescription=NULL WHERE WardId = @WardId
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 19/03/2018
-- Description:	This update trigger will change the WardCostCentreDate.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if any integraton fields are changed by other than integration service
-- =============================================
CREATE TRIGGER [dbo].[ClientCampusWard_UPDATE]
       ON [dbo].[ClientCampusWard]
AFTER UPDATE
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		  SET NOCOUNT ON;
		   
		   DECLARE @WardId INT	   		   
 
		   SELECT @WardId = INSERTED.WardId FROM INSERTED
			
		   IF UPDATE(Name) OR UPDATE(EnabledStatus)
		   BEGIN
			  UPDATE WardCostCentreDate SET FTStatus=10, FTErrorDescription=NULL WHERE WardId = @WardId
		   END
	   END
END
GO


ALTER TABLE dbo.Shift ADD
	FTJobId bigint NULL,	
	FTExported bit NULL
GO


-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 04/04/2018
-- Description:	This update trigger will change the WardCostCentreDate.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if any integraton fields are changed by other than integration service
-- =============================================
CREATE TRIGGER [dbo].[ClientCampus_UPDATE]
       ON [dbo].[ClientCampus]
AFTER UPDATE
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		  SET NOCOUNT ON;		   
		   
		   DECLARE @CampusId INT
 
		   SELECT @CampusId = INSERTED.CampusId FROM INSERTED		   
			
		   IF UPDATE(Street1) OR UPDATE(Street2) OR UPDATE(Postcode) OR UPDATE(State)
		   BEGIN
			  UPDATE WardCostCentreDate SET FTStatus=10, FTErrorDescription=NULL WHERE WardId IN (SELECT WardId FROM ClientCampusWard WHERE CampusId = @CampusId)
		   END
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 04/04/2018
-- Description:	This insert trigger will change the WardCostCentreDate.FTStatus to 'ToBeSent' i.e. 10 and FTErrorDescription to NULL 
--              if any integraton fields are changed by other than integration service
-- =============================================
CREATE TRIGGER [dbo].[ClientCampus_INSERT]
       ON [dbo].[ClientCampus]
AFTER INSERT
AS
BEGIN
	   IF App_Name() <> 'Programmed.HRLink.FT360Integration'-- we want to change the status if update made by other than integration service
	   BEGIN 
		  SET NOCOUNT ON;		   
		   
		   DECLARE @CampusId INT
 
		   SELECT @CampusId = INSERTED.CampusId FROM INSERTED
		   
		    UPDATE WardCostCentreDate SET FTStatus=10, FTErrorDescription=NULL WHERE WardId IN (SELECT WardId FROM ClientCampusWard WHERE CampusId = @CampusId)
		   
	   END
END
GO

-- =============================================
-- Author:		Maqsood Ahmad (mahmad)
-- Create date: 04/04/2018
-- Description:	This sp will get locked and not exported shifts
-- =============================================
CREATE PROCEDURE [dbo].[php_rsLockedShifts]
	@LocationID INT	
AS
BEGIN
	SELECT TOP 100
		s.ShiftID		
		, c.FTClientId
		, m.FTCandidateId
		, wccd.FTCostCentreId		
		, s.StartDate
		, s.FinishDate
		, s.PayClassName	
		, cf.InvoiceSplitType		
	FROM dbo.Shift s
	LEFT OUTER JOIN WardCostCentreDate wccd ON s.WardID = wccd.WardID 
						AND CONVERT(VARCHAR, s.StartDate, 106) BETWEEN wccd.EffectiveStartDate AND wccd.EffectiveFinishDate
	INNER JOIN Client as c on s.ClientID = c.ClientID
	INNER JOIN Member m on (s.MemberId = m.MemberId)

	WHERE s.LocationID = @LocationID
	AND s.PinkSlipID > ''
	AND s.StartDate >= '1 Jan 2000'
	AND (s.FTExported IS NULL OR s.FTExported = 0)
END
GO

--Set office codes
ALTER TABLE dbo.Office ADD
	FTOfficeId int NULL
GO
UPDATE Office SET FTOfficeId=36 WHERE LocationID=2600
UPDATE Office SET FTOfficeId=3 WHERE LocationID=3000
UPDATE Office SET FTOfficeId=7 WHERE LocationID=4000
UPDATE Office SET FTOfficeId=11 WHERE LocationID=5000
UPDATE Office SET FTOfficeId=9 WHERE LocationID=6000
UPDATE Office SET FTOfficeId=32 WHERE LocationID=800
UPDATE Office SET FTOfficeId=27 WHERE LocationID=2000 AND (Name='Newcastle' OR Name='NSW Rural' OR Name='Rural')
UPDATE Office SET FTOfficeId=19 WHERE LocationID=2000 AND Name='Parramatta' 
UPDATE Office SET FTOfficeId=23 WHERE LocationID=2000 AND (Name='Sydney City North' OR Name='Sydney City South')